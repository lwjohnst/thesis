Regarding clinical applications, I think you do need to address this – every piece of biomedical research is eventually intended to have some clinical application, even if it is well down the road (in other words, even the most fundamental discovery research is intended to build to a clinical application).  You can comment on (a) what these potential clinical applications were a priori; (b) what you learned from your research and how the findings might eventually have clinical application, after additional research.  This last point will help you to think about future directions.

Regarding R code, stick it in an appendix. 

Regarding abbreviations – do one up to show him, and we can figure out if it can be included.  I think you should call or email SGS and ask someone.  Same with the contributions – since some previous theses have it, there is precedent.
