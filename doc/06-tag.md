
# Specific clusters of fatty acids within the serum triacylglycerol fraction associate with the pathogenesis of type 2 diabetes 

\chaptermark{TAGFA on the pathogenesis of T2DM}

\begin{chapabstract}

\textbf{Objective}:
Although elevated serum triacylglyceride (TAG) is a well-described risk factor for
type 2 diabetes (T2DM), few data are available regarding the role of the specific fatty
acid (FA) composition within serum TAG (TAGFA) in the pathogenesis of T2DM. Our aim, therefore, was to
examine longitudinal associations of TAGFA with insulin sensitivity (IS) and
beta-cell function.

\textbf{Research Design and Methods}:
We used longitudinal data (3 visits over 6 years) from the Prospective
Metabolism and Islet Cell Evaluation (PROMISE) cohort of adults (n=477) who were at-risk
for diabetes at baseline. Glucose and insulin from an OGTT were used to calculate the outcome variables
Matsuda index (ISI), HOMA2-\%S, Insulinogenic Index over HOMA-IR (IGI/IR),
and Insulin Secretion-Sensitivity Index-2 (ISSI-2). Gas chromatography
quantified TAGFA composition. Generalized estimating equations (GEE) adjusted for
confounders and partial least squares (PLS) were used for the analysis.

\textbf{Results}: 
The metabolic outcome variables declined by 14\% to 27\% over the 6-years. In the adjusted
GEE models, four TAGFA (14:0, 16:0, 14:1n-7, 16:1n-7 as mol\%) had
strong negative associations with IS while others (e.g. 18:1n-7, 18:1n-9, 20:2n-6, 
20:5n-3) had strong positive associations. Few associations were seen for 
beta-cell function, except for 16:0 (negative) and 18:1n-7 (positive). PLS
analysis indicated four TAGFA (14:0, 16:0, 14:1n-7, 16:1n-7) that are markers of de novo lipogenesis (DNL)
clustered together and strongly predicted
lower IS. These four TAGFA also correlated highly (r>0.4) with clinically
measured TAG.

\textbf{Conclusions}:
We found that higher proportions of a cluster of four DNL TAGFA 
strongly predicted lower IS as well as hypertriglyceridemia.

\end{chapabstract}

## Background

Hypertriglyceridemia is a well described metabolic disorder resulting in negative health outcomes [@Chehade2013a].
It is a risk factor for cardiovascular disease [@DAgostino2004a; @Verges2015a]
and is associated with other metabolic disorders such as non-alcoholic
fatty liver disease [@Kawano2013a], the metabolic syndrome [@Alberti2009], and
abdominal obesity [@Lemieux2000a].
Circulating triacylglceride (TAG) concentration is commonly measured during
routine clinical assessment using enzymatic methods.
However, clinically measured TAG is limited as it
represents the full fatty acid spectrum within the TAG fraction as a summary
measure. There is increasing appreciation for the importance of specific fatty 
acid composition profiles in different plasma fractions on various health
outcomes [@Forouhi2014a; @Ma2015a; @Johnston2016a], however there are relatively
few studies that have explored the impact of the fatty acid composition in the TAG fraction [@Rhee2011a; @Lankinen2015a].

The interaction between TAG and insulin sensitivity is complex and involves 
components of a feedback system [@Verges2015a]. Greater resistance to insulin in
both the liver and muscle may result in greater production of TAG and secretion
of lipoproteins that transport TAG [@Yu2012a]. Likewise, greater TAG may
contribute to metabolic dysfunction and lipotoxicity in various tissues,
affecting insulin sensitivity, and thus continue the cycle [@Verges2015a]. Given the
complexity and temporal nature of the relationship, long term studies with
multiple data collection time points are paramount to better understanding the underlying
biology and subsequent risk.

While several studies have documented prospective associations of 
hypertriglyceridemia with incident type 2 diabetes 
[@Chien2008a; @DAgostino2004a; @Schulze2009a], only a limited number of longitudinal
studies [@Rhee2011a; @Lankinen2015a] have examined the relationship between
TAG and its composition with the pathophysiological factors underlying diabetes, 
particularly beta-cell function. Our objective was to examine the 
longitudinal role of the specific composition of the serum TAG fraction on 
OGTT-derived measures of insulin sensitivity and beta-cell function compared to 
clinically measured TAG in a Canadian population at risk for diabetes.

## Subjects and Methods











<!-- Use an external source via DOI for reference to these methods? -->

Recruitment for the baseline visit of the Prospective Metabolism and Islet Cell Evaluation (PROMISE) 
cohort took place between 2004-2006 in London and Toronto, 
Canada. Individuals were selected to participate if they met the eligibility 
criteria of having one or more risk factors for type 2 diabetes mellitus, 
including obesity, hypertension, family history of diabetes, and/or a history of
gestational diabetes or birth of a macrosomic infant. A total of 736 individuals
attended the baseline visit. Subsequent examinations occurred every three years,
with data from three examination visits available for the present analysis
(2004-2006, 2007-2009, and 2010-2013). The current study
used data on participants who did not have diabetes at baseline, who
returned for one or more of the follow-up examinations, and who had samples
available for fatty acid measurements (n=477; see the CONSORT diagram in 
Supplemental Figure 6.1). Metabolic characterization, anthropometric
measurements, and questionnaires on lifestyle and sociodemographics were
administered at each examination visit. Research ethics approval was obtained from
Mount Sinai Hospital and the University of Western Ontario, and all participants
provided written informed consent. Data collection methods were standardized
across the 2 centres and research nurses were centrally trained.

### Metabolic characterization

After 8-12 hours of overnight fasting, participants completed a 75g oral glucose tolerance test (OGTT) at 
each examination visit, with blood samples taken at fasting, 30 min, and 2 hr 
post-glucose load. Samples were subsequently processed and frozen at -70°C.
Alanine aminotransferase (ALT) was measured using standard laboratory 
procedures. Cholesterol, HDL, and clinically-measured TAG 
were measured using Roche Modular's enzymatic colorimetric tests (Mississauga, 
ON). Both insulin and glucose were
measured from OGTT blood samples at fasting, 30 minute,
and 2 hour time points. Specific insulin was measured with the Elecsys 1010 
(Roche Diagnostics, Basel, Switzerland) immunoassay analyzer and 
electrochemiluminescence immunoassay, which shows 0.05% cross-reactivity to 
intact human pro-insulin and the Des 31,32 circulating split form (Linco Res. 
Inc) and has a coefficient of variation (CV) of 9.3%. Glucose was determined 
using an enzymatic hexokinase method (Roche Modular, Roche Diagnostics) with a 
detection range of 0.11 to 41.6 mmol/L and an inter-assay CV of <1.1% and an
intra-assay CV of < 1.9%.  All assays were performed at the Banting and Best
Diabetes Centre Core Lab at Mt Sinai Hospital. Impaired fasting glucose (IFG),
impaired glucose tolerance (IGT), and diabetes were categorized using the 2006
WHO criteria [@WHO2006].

TAGFA composition was quantified using stored fasting serum samples from the 
baseline visit, which had been frozen at -70°C for 4-6 years and had not been 
exposed to any freeze-thaw cycles. Serum fatty acids have been documented to be 
stable at these temperatures for up to 10 years [@Matthan2010a]. A known amount 
of triheptadecanoin (17:0; Nu-Chek Prep, Inc Elysian, MN, USA) was added as an internal
standard prior to extracting total lipids according to the method of Folch 
[@Folch1957a]. Each serum lipid fraction (non-esterified fatty acids (NEFA), 
cholesteryl ester, phospholipid, and TAG) was isolated using thin layer 
chromatography. Fatty acid methyl esters were separated and quantified using a 
Varian-430 gas chromatograph (Varian, Lake Forest, CA, USA) equipped with a 
Varian Factor Four capillary column and a flame ionization detector. Fatty acid 
concentrations (nmol/ml) were calculated by proportional comparison of gas
chromatography peak areas to that of the internal standards [@Nishi2014a]. There
were 22 fatty acids measured in the TAGFA fraction. 
Findings
for other lipid fractions in this cohort are reported separately (see ref
@Johnston2016a for the analysis of the phospholipid and cholesteryl ester
fractions). <!--{{put these analyses in discussion?}} -->
<!--and {{next citation}} for the analysis of the NEFA fraction).-->

### Anthropometrics and sociodemographics

Height, weight, and waist circumference (WC) were measured at all clinic 
examinations using standard procedures.  WC was measured at the natural waist, 
defined as the narrowest part of the torso between the umbilicus and the xiphoid
process. BMI was calculated by dividing weight (kg) by height (m) squared.
Questionnaires administered at each examination determined sociodemographics.
A version of the Modifiable Activity Questionnaire (MAQ) [@Kriska1990]
determined estimated physical activity. The MAQ collects information on leisure
and occupational activity, including intensity, frequency, and duration, over
the past year. Each reported activity from the MAQ was weighted by its
metabolic intensity allowing for the estimation of MET-hours per week [@Kriska1990].

### Variable calculation and statistical analysis

Insulin sensitivity and beta-cell function indices were computed using the OGTT 
glucose and insulin data. Insulin sensitivity was assessed using the Insulin
Sensitivity Index (ISI) [@Matsuda1999] and HOMA2-%S [@Levy1998a] using the HOMA2
Calculator. HOMA largely reflects hepatic insulin sensitivity, while ISI
reflects whole-body insulin sensitivity [@AbdulGhani2007]. Beta-cell function
was assessed using the Insulinogenic Index [@Wareham1995] over HOMA-IR [@Matthews1985] (IGI/IR)
and the Insulin Secretion-Sensitivity Index-2 (ISSI-2) [@Retnakaran2009]. IGI/IR
is a measure of the early phase of insulin secretion while ISSI-2 is analogous to
the disposition index (but is calculated using OGTT values). Each index has been validated
against gold standard measures [@Matthews1985; @Hermans1999a; @Matsuda1999; @Retnakaran2009].

The primary outcome variables for this analysis were HOMA2-%S, ISI, IGI/IR, and
ISSI-2, which were log-transformed for the statistical modeling. The primary
predictor variables for this analysis were 22 individual TAGFA included as either mole percent
(mol%) of the total fraction or as a concentration (nmol/mL). Clinically-measured
TAG was also included as a primary predictor to allow us to test the hypothesis
that specific TAGFA better predicted outcomes compared to clinical TAG. 
Pearson correlation coefficients were computed to assess the relationships of
individual TAGFA with other continuous variables. Correlations were also
computed and hierarchical clustering analysis was conducted for TAGFA against each other.

Generalized estimating equation (GEE) models [@Zeger1986a] were used in the 
primary analysis to determine the longitudinal associations between the outcome 
variables and the predictor variables. The predictor variables and continuous 
covariates were scaled (mean-centered and standardized). Given the longitudinal 
design, an auto-regressive of order 1 (AR1) working correlation matrix was 
specified in the GEE model. Covariates to adjust for were selected based on the
previous literature, from directed acyclic graph [@Greenland1999a]
recommendations, and from quasi-likelihood information criteria (QIC). The final
GEE model (M6) was adjusted for time, waist circumference, baseline age, ethnicity,
sex, ALT, MET, and total NEFA. The TAGFA, total NEFA, sex, ethnicity, and
baseline age were classified as *time-independent* (held constant) as they were
measured only at the baseline visit or do not change throughout the study, while
the outcome variables and remaining covariates were set as *time-dependent*.
After transformations, the GEE estimates are interpreted as an expected percent 
difference in the outcome variable for every standard deviation (SD) increase in
the predictor variable given the covariates are held constant (including time). 
We also tested for an interaction with sex, ethnicity, or time by the predictor 
term for each outcome variable. See the Supplemental Methods for an expanded 
explanation of the GEE modeling analysis.

While GEE accounts for the longitudinal design of the data, this approach is 
limited in that it cannot analyze the inherent multivariate nature of the 
composition of the TAGFA fraction. Therefore, to confirm the GEE results in a multivariate 
environment (i.e. all TAGFA analyzed collectively), partial least squares regression (PLS) was
used to identify the patterns of TAGFA composition against insulin sensitivity
and beta-cell function as outcome variables. For a detailed explanation of PLS
see the Supplemental Methods. Briefly, PLS is a technique that extracts latent
structures (clusters) underlying a set of predictor variables conditional on a
response variable(s) (i.e. the outcome variables). How accurately the clusters
within the TAGFA composition predict metabolic function is determined by using
cross-validation on the PLS models.

All analyses were performed using R 3.4.0 [@Rbase], along with the R
packages geepack 1.2.1 for GEE [@Hoejsgaard2006a] and pls
2.6.0 for PLS. The R code and extra analyses for
this manuscript is available at https://doi.org/10.6084/m9.figshare.5143438. Results were considered
statistically significant at p<0.05, after adjusting for multiple testing using
the Benjamini-Hochberg False Discovery Rate [@Benjamini1995a]. STROBE was used
as a guideline for reporting [@Vandenbroucke2007b].

## Results

### Basic characteristics of the PROMISE cohort

Table 6.1 shows basic characteristics of the PROMISE cohort. The mean 
follow-up time was 5.6 (1.0) years, where 88.7%
of participants attended all three visits. There were
349 (73.2%) females and 336 (70.4%) who had European-ancestry, with a mean
age in years of 50.1 (9.8) and a mean BMI
of 31.1 (6.4) kg/m^2^. As expected from the study's eligibility
criteria, the majority of participants, n=308 (64.8%), had a family history of
diabetes. Between the baseline visit and the 6-year visit in this sample,
insulin sensitivity and beta-cell function measures had a significant median decline of
between 14% to 27% (p<0.001 from GEE; n=367-470). There 
were 96 (20%) and 42 (9%) incident cases of pre-diabetes (IFG and IGT) and
diabetes, respectively, over the 6-years; these observations were excluded from GEE and PLS
analyses due to how the outcomes were measured (OGTT was not done on those with diabetes).

\newpage\newpage

| Measure         |      Baseline       |        3-yr         |        6-yr         |
|:----------------|:-------------------:|:-------------------:|:-------------------:|
| HOMA2-%S        |  88.8 (54.2-136.7)  |  76.8 (49.1-121.8)  |  73.7 (49.5-110.1)  |
| ISI             |   13.6 (8.7-21.8)   |   11.6 (6.9-19.1)   |   11.6 (7.5-17.5)   |
| IGI/IR          |   7.1 (4.2-10.6)    |    5.6 (3.6-9.8)    |    5.6 (3.5-9.0)    |
| ISSI-2          | 727.5 (570.0-922.5) | 613.4 (493.9-836.7) | 622.5 (472.5-810.3) |
| ALT (U/L)       |     29.6 (16.0)     |     28.4 (19.5)     |     25.9 (16.9)     |
| TAG (mmol/L)    |      1.5 (0.8)      |      1.4 (0.8)      |      1.4 (0.7)      |
| Chol (mmol/L)   |      5.2 (0.9)      |      5.1 (1.0)      |      5.1 (0.9)      |
| HDL (mmol/L)    |      1.4 (0.4)      |      1.3 (0.4)      |      1.4 (0.4)      |
| TAGFA (nmol/mL) |   3137.5 (1686.6)   |                     |                     |
| NEFA (nmol/mL)  |    383.1 (116.3)    |                     |                     |
| MET             |     45.2 (59.7)     |     48.5 (60.5)     |     44.1 (57.1)     |
| Age (yrs)       |     50.1 (9.8)      |     53.2 (9.7)      |     56.5 (9.6)      |
| BMI (kg/m^2^)   |     31.1 (6.4)      |     31.4 (6.5)      |     31.1 (6.6)      |
| WC (cm)         |     98.5 (15.5)     |     99.3 (15.7)     |    100.4 (15.7)     |
| Ethnicity       |                     |                     |                     |
| - European      |      336 (70%)      |                     |                     |
| - Latino/a      |      58 (12%)       |                     |                     |
| - Other         |      51 (11%)       |                     |                     |
| - South Asian   |       32 (7%)       |                     |                     |
| Sex             |                     |                     |                     |
| - Female        |      349 (73%)      |                     |                     |
| - Male          |      128 (27%)      |                     |                     |

Table: Table 6.1: Basic characteristics of PROMISE participants at each of the 3 clinic visits.


Figure 6.1 shows the composition of each FA in the TAG fraction (see 
Supplemental Table 6.2 for a tabular presentation of the values). 
Three TAGFA contributed 82.4% to the total 
TAG concentration: 18:1n-9 (37.8%); 16:0 (26.6%); and, 18:2n-6 (18.0%).
Figure 6.2 shows a heatmap of the correlation of individual TAGFA as
concentrations with the outcome variables and several basic characteristics.
As expected, nearly all TAGFA had very strong positive correlations (r=
0.33 to 0.92) with clinically-measured TAG and moderate positive 
correlations with WC (r=0.31 to 0.36). There were also moderate
negative correlations with HDL (r=-0.53 to -0.32). For the outcome variables,
the correlations for the insulin sensitivity measures were generally higher 
(HOMA2-%S: r=-0.48 to -0.32, ISI: r=-0.48 to -0.33) than 
for the beta-cell function measures (all r<0.30). For correlations
of individual TAGFA using mol% with the basic participant characteristics, as shown in 
Figure 6.3, differences in correlations between fatty acids were
most evident for 14:0, 14:1n-7, 16:0, and 16:1n-7 that had a moderate positive
correlation with clinical TAG (r=-0.5 to -0.31) while all other fatty
acids had a negative association (r=0.42 to 0.52). In particular,
those fatty acids with the negative associations with clinical TAG were all the
very long chain polyunsaturated fatty acids (e.g. 20:4n-6, 20:5n-3).
As seen in Figure 6.4, four fatty acids (14:0,
16:0, 14:1n-7, and 16:1n-7) clustered together, each highly positively correlated
with each other and negatively correlated with all other fatty acids.


\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/fig_tagfa-1.pdf}
\caption[Figure 6.1: Distribution of TAGFA in serum.]{Figure 6.1: Distribution of the composition of triacylglycerol fatty acids in the baseline visit of PROMISE participants (2004-2006). Boxplots represent the median and interquartile range of the fatty acid values.}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/fig_heatmap-1.pdf}
\caption[Figure 6.2: Correlation heatmap of TAGFA (nmol/mL) with basic characteristics.]{Figure 6.2: Pearson correlation heatmap of triacylglycerol fatty acids (nmol/mL) with continuous basic and metabolic characteristics of PROMISE participants from the baseline visit (2004-2006). Darker orange represents a positive correlation; darker blue represents a negative correlation.}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/fig_heatmap_mol-1.pdf}
\caption[Figure 6.3: Correlation heatmap of TAGFA (mol\%) with basic characteristics.]{Figure 6.3: Pearson correlation heatmap of triacylglycerol fatty acids (mol\%) with continuous basic and metabolic characteristics of PROMISE participants from the baseline visit (2004-2006). Darker orange represents a positive correlation; darker blue represents a negative correlation.}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/fig_heatmap_tagfa-1.pdf}
\caption[Figure 6.4: Correlation heatmap of each individual TAGFA.]{Figure 6.4: Pearson correlation heatmap of the triacylglycerol fatty acids in the PROMISE participants from the baseline visit (2004-2006). The correlations of fatty acids grouped using heirarchical cluster analysis; fatty acids along the x and y axis are ordered according to this analysis. Darker orange represents a positive correlation; darker blue represents a negative correlation.}
\end{figure}


### Generalized estimating equation models

Results from the unadjusted GEE model are shown in Figure 6.5 and 
for the adjusted GEE model in Figure 6.6. The majority of
associations with beta-cell function measures were attenuated after full model
adjustment, while nearly all associations with insulin sensitivity remained
significant for both mol% and nmol/mL results. Subsequent analysis revealed that
the attenuation with beta-cell function was due primarily to adjustment for
waist.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/fig_gee_unadj-1.pdf}
\caption[Figure 6.5: Time-adjusted GEE models of TAGFA with outcomes.]{Figure 6.5: Time-adjusted GEE models of the association of the triacylglycerol fatty acids (mol\% and nmol/mL) and total clinically-measured TAG with insulin sensitivity and beta-cell function outcomes using the 6 year longitudinal data from the PROMISE cohort. X-axis values represent a percent difference in the outcome per SD increase in the fatty acid. P-values were adjusted for the BH false discovery rate, with the largest dot representing a significant (p<0.05) association. BaseTAG is clinically measured TAG at the baseline visit.}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/fig_gee_adj-1.pdf}
\caption[Figure 6.6: Fully-adjusted GEE models of TAGFA with outcomes.]{Figure 6.6: Fully-adjusted GEE models of the association of the triacylglycerol fatty acids (mol\% and nmol/mL) and total clinically-measured TAG with insulin sensitivity and beta-cell function outcomes using the 6 year longitudinal data from the PROMISE cohort. Variables controlled for were follow-up time, waist circumference, baseline age, ethnicity, sex, ALT, physical activity, and total NEFA. X-axis values represent a percent difference in the outcome per SD increase in the fatty acid. P-values were adjusted for the BH false discovery rate, with the largest dot representing a significant (p<0.05) association. BaseTAG is clinically measured TAG at the baseline visit.}
\end{figure}


In analyses using concentration values, nearly all TAGFA had a strong
negative association on HOMA2-%S and ISI (estimates of percent different ranging from
-13.7 to -3.6 and -14.7 to -4.4, respectively), and a
few had strong negative associations with IGI/IR and ISSI-2 (estimates ranging 
from -7.4 to -7.3 and -4.1 to -3.4, respectively).
In analyses using TAGFA mol% values, four TAGFA (14:0, 16:0, 14:1n-7, and 16:1n-7) had negative
associations with HOMA2-%S and ISI (between -11.6 to -5.7 and 
-12.3 to -5.8, respectively, lower insulin sensitivity for every SD
increase in the TAGFA), while several more TAGFA had positive associations with
HOMA2-%S and ISI (20:0, 18:1n-9, 20:1n-9, 22:1n-9, 18:2n-6, 20:2n-6, 20:4n-6,
and 22:5n-3) predicting between 4.3 to 14.2 and 
5.8 to 15.2%, respectively, higher insulin sensitivity for every
SD increase in the TAGFA. One TAGFA, 20:2n-6, had a very strong positive
association with the insulin sensitivity measures, with a 14.2
to 15.2% higher insulin sensitivity for every SD increase. 
Both clinically-measured TAG and total TAGFA concentration had very strong
negative associations with all outcome variables. 

While there were a few
significant interactions by time in unadjusted models, after inclusion of
covariates in the model, these interactions were attenuated (data not
shown). There were no significant interactions by sex or ethnicity for any of
the TAGFA (data not shown). Results of the sensitivity analyses identifying
waist circumference as the covariate that attenuated the beta-cell function
associations from the unadjusted model are shown in Supplemental Figure 6.4. 
A tabular presentation of the GEE results is shown in Supplemental Table 6.3
for unadjusted models and Supplemental Table 6.4 for adjusted models.

### Clustering of TAGFA by metabolic measures

The PLS analysis corroborated the findings from the GEE models. 
The PLS results conditioned on insulin sensitivity as the outcome showed a
clustering of the fatty acids 14:0, 14:1n-7, 16:0, and 16:1n-7 as mol%
(Figure 6.7). These TAGFA loaded strongly and negatively on HOMA2-%S and ISI
in the first component, suggesting this cluster of TAGFA tracks
together with lower insulin sensitivity. The TAGFA 20:2n-6, 20:5n-3, 22:5n-3,
and 22:6n-3 loaded positively on both insulin sensitivity measures. No other
TAGFA loaded strongly. In the second component, 18:1n-9 and 18:1n-7 loaded postively 
but not strongly while 20:5n-3 and 22:6n-3 loaded strongly and negatively with both
HOMA2-%S and ISI; however, this component only explained <10% of the variance. 
The PLS model for insulin sensitivity had good predictive
ability, with a high correlation between the predicted outcome values against
the observed values (HOMA2-%S: r=0.46, p<0.001; ISI: r=0.39, p<0.001).

\begin{figure}
\centering
\includegraphics[height=0.8\textheight]{manuscript_files/figure-docx/fig_pls-1.pdf}
\caption[Figure 6.7: PLS component loadings of TAGFA with the outcomes.]{\small Figure 6.7: Partial least squares (PLS) models showing the clustering of triacylglycerol fatty acids on insulin sensitivity and beta-cell function measures. The percent explained variance of each component is shown in brackets on each axis. The solid line represents an explained variance of 100\% while the dashed line represents an explained variance of 50\%. Fatty acids between these lines represent variables that strongly explain the underlying structure of the data. See the Supplemental Methods for a description of PLS analysis and an explanation of interpreting this plot.}
\end{figure}


The beta-cell function PLS results showed a similar clustering of fatty acids,
however there was a lower correlation (though significant at 
p<0.001) between the predicted values and the observed values 
(r=0.25 to 0.24), suggesting that TAGFA composition
poorly predicts beta-cell function.
Given the low predictability, only the insulin sensitivity measures are
presented. We used the extracted PLS scores as the predictor variable in the GEE
models and found negative associations of the first component on all outcome
variables, with the strongest association being with the insulin sensitivity
variables (beta=10.2, all p<0.001; using PLS
scores constrained by ISI).

## Discussion

In the present study, we found that in a Canadian cohort at risk for diabetes,
several specific TAGFA and groups of TAGFA were strongly associated
with insulin sensitivity and moderately associated with beta-cell function. In 
particular, the TAGFA myristic acid (14:0), 7-tetradecenoic acid 
(14:1n-7), palmitic acid (16:0), and palmitoleic acid (16:1n-7) all strongly and
negatively predicted lower insulin sensitivity. While most TAGFA were not
associated with beta-cell function, two fatty acids, palmitic acid (16:0) and
*cis*-vaccenic acid (18:1n-7), were associated negatively and positively,
respectively, with measures of beta-cell function. Using PLS, we also found that four
TAGFA (14:0, 14:1n-7, 16:0, 16:1n-7) clustered together, and that this cluster
strongly predicted lower insulin sensitivity. These four fatty acids are
involved in the *de novo* lipogenesis (DNL) of refined and simple carbohydrates
[@Hodson2008a; @Kawano2013a]. Our results suggest that higher activity of DNL
(potentially through higher intakes of simple carbohydrates) may increase the
risk for diabetes, primarily through worsening insulin sensitivity.

To our knowledge, no longitudinal study to date has examined the role of the
composition of the TAGFA fraction on detailed OGTT-derived metabolic measures 
Two large prospective studies have been published that similarly examined TAGFA 
composition and diabetes outcomes. Rhee *et al* presented a nested case-control
analysis (n=189 cases and n=189 controls) within the Framingham offspring cohort
[@Rhee2011a], which found that subjects with a TAGFA composition characterized
by a lower carbon chain and fewer double bonds (e.g. 14:0, 16:0) had a higher
risk for diabetes after 12-years while those with a profile characterized by
higher carbon chain and more double bond TAGFA had a lower risk for diabetes.
A similar pattern of TAGFA was also associated with HOMA-IR cross-sectionally
at the baseline visit. In addition, Lankinen *et al* reported on
a prospective cohort of males in Finland [@Lankinen2015a], for which 
TAGFA data were available for 831 participants after 6-years of follow-up. In
their cohort, OGTT data was only available at the 6-year visit.
They cross-sectionally found that most saturated fatty acids had negative 
associations with insulin sensitivity and beta-cell function while
linoleic acid (18:2n-6), docosapentaenoic acid (22:5n-3), eicosapentaenoic acid
(20:5n-3), and arachidonic acid (20:4n-6) had positive associations with insulin
sensitivity. The magnitude of the associations were larger in the insulin
sensitivity results compared to the beta-cell function results, similar to what
we observed. Our study extends these findings by using multiple measurements
of metabolic function and as well as multivariate statistical approaches that
allowed us to identify clusters of TAGFA. In another study of a much
smaller (n=16) mostly female group [@Kotronen2009a], the authors report a
positive correlation between total esterified (of which TAG make up the
majority) 16:0, 16:1n-7, and 18:1n-9 with HOMA-IR, findings which were largely 
similar to the present analysis.

Previous research has shown that carbohydrate intake increases DNL 
[@Hodson2008a; @Kawano2013a; @Harding2015a; @Hudgins2000a; @Parks1999a]. In
particular, DNL from refined or simple carbohydrate sources increases the 14 to
16 chain fatty acids as well as the 18 chain TAGFA. Several studies have shown a
link between higher estimated DNL and an increased risk for metabolic 
dysfunction [@Ma2015a; @Zong2013a; @Lankinen2015a; @Kroger2011a]. Our study 
extends these findings by showing that TAGFA with 14 to 16 carbons clustered
together and this pattern strongly predicted lower insulin sensitivity. While
these fatty acids also had a significant association with beta-cell function,
the magnitude of associations were more modest compared to those for insulin
sensitivity. 

The link between higher DNL and increases in specific fatty acids has been
examined in several studies. Previous studies that have examined DNL have used
markers of estimated DNL, such as the ratio between 18:2n-6 to 16:0 or 16:1n-7
to 16:0 [@Hodson2008a; @Lankinen2015a; @Kroger2011a]. However, there are 
limitations to using these ratios as the fatty acids used in their calculation
can also be obtained from the diet in addition to being created through DNL
[@Hodson2008a]. A feeding trial (n=24) was conducted to identify 
the fatty acids that most accurately reflected DNL as potential biomarkers
[@Lee2015a]. The study found that palmitoleic acid (16:1n-7), directly measured
DNL using isotopes, and liver fat were all highly correlated with each other
(r>0.50), suggesting that 16:1n-7 may be a good biomarker for hepatic DNL. In
another small (n=14) feeding trial, meal type (high fat vs low fat) was tested
to determine its effect on DNL and TAGFA composition [@Wilke2009a]. The authors
reported that 14:0, 16:0, 16:1, and 18:2 were higher in the low fat (high
carbohydrate) group. These fatty acids are similar to the fatty acids we found
that clustered together using the PLS analysis, implicating these fatty acids as
indicative of a higher carbohydrate diet. A higher carbohydrate diet, 
particularly one characterized by a predominance of simple carbohydrates, may
lead to greater DNL in an attempt to control blood glucose, thus increasing hepatic
fat stores and consequently increasing the amount of TAGFA in circulation
[@Chehade2013a; @Verges2015a]. In fact, we found in our study that
higher proportions of these four DNL TAGFA also highly correlated with a higher
concentration of clinical TAG, reinforcing this pathway between carbohydrate
intake, DNL, and circulating TAG. The higher concentration of circulating 14 and
16 carbon fatty acids may then expose tissues to greater lipotoxicity, for
instance from palmitic acid (16:0), which is well-known to have harmful effects
on tissues [@Riserus2008a; @Iggman2010a].

The direction of association between TAGFA and insulin sensitivity is unclear 
from previous cross-sectional studies due to the physiological feedback mechanisms 
involved. For example, while greater DNL may promote muscle insulin resistance, 
the reverse may also be true [@Flannery2012a]. Higher insulin resistance may
encourage greater DNL to handle the higher blood glucose. To illustrate this, in
a weight loss intervention trial (n=19 with TAGFA data), participants who lost
weight over 33 weeks showed higher insulin sensitivity and lower TAGFA
composition indicative of lower DNL (less 14:0, 14:1, 16:0, 16:1, etc)
[@Schwab2008a]. Given the complex biological mechanisms and feedback loops
involved, disentangling whether insulin sensitivity influences TAGFA more
strongly than TAGFA influencing insulin sensitivity will require more
complicated research designs and analyses. While further research will need to
confirm this, the lack of a time interaction we observed in addition to the
timing of our measures likely suggests that it is the TAGFA predicting insulin
resistance.

<!--
{{ Not sure about this... Include this? }}
As with our previous analysis of the PL fatty acid fraction {{cite}}, we found
that cis-vaccenic acid strongly predicted higher insulin sensitivity and
beta-cell function. Likewise, in the TAGFA fraction we found a similar
association with cis-vaccenic acid. {{we can discuss to include more}}

{{ comment on comparing models with and without NEFA }} While
NEFA may be a mediating factor, we found that adjustment for it did not
{{confirm}} attenuate this association since TAGFA composition is also dictated
by other sources for DNL (e.g. recent meal, DNL from glucose stores, etc.).

While the NEFA fraction is much smaller than the TAG fraction, even though it
contributes substantially to the TAG fraction, the VLDL-TAG is not cleared
(lipolyzed) as much as chylomicron (which also contains a large amount of TAG)
and so has a higher half-life than NEFA, remaining in circulation longer {{hence
why there is bigger TAG pool vs NEFA}} {{cite?}}. 

- contributions to DNL (e.g. 70% 16:0 to newly synth) {{ confirm what this is }}

- Zulyniak2012a (only vaccenic acid in TAG) (n=20, TLC-GC, HOMA-IR/HOMA-B,
two groups: normogly and hypergly. results in general showed no diff between
groups, only cis-vaccenic was higher in hyper, 'trend' with palmitic, did
not present results on whole fraction for IR)
    
{{ comment on unadjusted vs adjusted }}

- Potential clinical application

- Or that post-prandial TAG is not suppressed in IR Sondergaard2012a
- {{ confirm }} greater flux of saturated fatty acids into tissues may encourage
lipotoxic environment {{ cite }}

{{ Concentration data is not }}

-->

### Limitations and strengths

Our study has potential limitations that need to be considered when interpreting
the results. Firstly, this is an observational cohort and as such there may be
some residual confounding we were not able to control for or were unaware of. 
However, we have taken extensive, empirically based precautions in identifying
potential confounders and mediators through the use of the DAG modeling, relying
on previous literature, and through QIC model fit comparison methods.

<!--
TAGFA can fluctuate substantially throughout the day and to control for this all
PROMISE participants came for their clinic visits in the morning in a fasted
state. Some of the inherent variability in TAGFA fluctuation can be managed by
standardizing the data collection time, though there may still be some
{{residual error? random error?}}.
[@Rhee2011a] fasting TAG status better as discriminating between DM case vs
control than 2hr-OGTT TAG
-->

TAGFA were only quantified at the baseline visit and as such we cannot
investigate whether there are concomitant changes in TAGFA and the metabolic
measures over time. However, to optimally use GEE to analyze the data and for
interpretation, we used the model to infer that a given value of TAGFA could
predict values of insulin sensitivity or beta-cell function over a 6 year
period. This in our view is a strength of our analysis, as it reduces the chance
of reverse causality given the tight integration of the glucose and fatty acid
metabolism pathways, as well as maximizes the specific usage of the GEE
modeling.

PLS is a well-established technique for constructing predictive models of high
dimensionality data structures (i.e. fatty acid composition), however a 
limitation is that the initial models analyzed through PLS and the final 
computed scores are not able to control for potential confounders and other 
effect modifiers. Likewise, PLS is not able to handle longitudinal data so only 
the baseline visit was used in the PLS analysis, although we analyzed the extracted
scores using the GEE modeling to overcome this limitation and observed
concordant results between the PLS and GEE analyses.

Our study has several notable strengths, including the longitudinal design and
the use of advanced statistical techniques for data analysis. These statistical
techniques take advantage of the longitudinal data to allow appropriate
investigation of temporal relationships and are able to handle the
multidimensional nature of the data. Lastly, our cohort contains highly detailed
and comprehensive variable measurements for the fatty acids and outcomes,
of which were collected at each visit.

### Final conclusion

In conclusion, we found that a TAGFA composition indicative of higher DNL
(containing higher 14:0, 14:1n-7, 16:0, and 16:1n-7) associated strongly with lower
insulin sensitivity and (more moderately) with lower beta-cell function. The fatty
acids that clustered together represent fatty acids created from DNL, which is
characteristic of higher simple carbohydrate (e.g. added sugar) intake. Our
results, which are congruent with current evidence, suggest that higher DNL,
likely due to greater intake of simple or refined carbohydrates, may increase the
risk of diabetes through worsening of insulin sensitivity.

## Supplemental Material

### Supplemental Methods: Statistical analysis

Generalized estimating equations (GEE) is a technique similar to mixed effects
modeling, except it calculates the marginal population estimates compared to the
subject-specific estimates in mixed effects models. GEE is well suited to and commonly
used in longitudinal cohort studies, especially given its capacity to handle
missed visits.

The working correlation matrix for the GEE analysis was chosen based on 
quasi-likelihood information criteria (QIC). The auto-regressive of order 1
(AR1) matrix was chosen for the GEE models as it had the best model fit accessed
using QIC, though other matrices (eg. exchangeable) had similar fit (data not
shown). 

For the confounders, they were chosen based on previous literature, from
directed acyclic graph (DAG) [@Greenland1999a] recommendations, and from
QIC. The DAG recommendations were obtained from using the DAGitty software
[@Textor2011a], http://dagitty.net/. See Supplemental Figure 6.2 and 
Supplemental Figure 6.3) for the DAG model. See Supplemental Table 6.1 for the
comparison of various models with different covariates using QIC. While the
final GEE model selected as best fitting differed between insulin sensitivity
and beta-cell function measures, the differences in the QIC values were less than 10
between many of the models, suggesting similar fit. As such, we selected the
model that had the fewest covariates and that had similar fit between the
outcome measures. Total NEFA was included as a confounder because it is used
substantially as a source of fatty acids in hepatic TAG production
[@Barrows2006a].

No imputation was conducted on missing values. Prevalent cases of diabetes at
baseline and incident cases at follow-up were excluded from the GEE analysis. 
The resulting GEE beta estimates were exponentiated to allow the interpretation
as stated in the main methods.

PLS is a technique used for multivariate, high dimensionality datasets where
there is a potential or likely underlying structure to the data. Because it uses
a response variable (i.e. the outcome or y variable) when identifying the 
underlying structure it is known as a supervised statistical method, which gives
it greater predictive power when using the model against updated or new data.
PLS generates a number of components based on the number of variables provided.
We used internal 10-fold cross-validation to determine which components to
extract from the PLS analysis. Based on the internal cross-validation, the first
two components gave the highest amount of explained variance (data not shown),
of which we decided to use these two components in the final results.

The predictive capability of the PLS models were tested using cross-validation. 
The data set was randomly split in half into a training set and a testing set. 
After specifying the model on the training set, results were compared using the 
testing set to determine whether how predictive the model was given a new
dataset. Final results shown in the figures use the full dataset (rather than a
training or testing set). No prevalent diabetes cases were included in the PLS 
analysis.

### Supplemental Tables and Figures

\small
\newpage\newpage

| Model           | QIC     | Delta   |
|:----------------|:--------|:--------|
| **log(ISI)**    | NA      | NA      |
| M7              | -1651.5 | 0.0     |
| M8              | -1649.9 | 1.6     |
| M9              | -1646.9 | 4.6     |
| M5              | -1646.5 | 5.0     |
| M6              | -1643.9 | 7.6     |
| M4              | -1641.8 | 9.7     |
| M3              | -1619.1 | 32.4    |
| M1              | -1261.7 | 389.8   |
| M0              | -1258.3 | 393.2   |
| M2              | -1250.4 | 401.1   |
| **log(ISSI-2)** | NA      | NA      |
| M8              | -2591.9 | 0.0     |
| M9              | -2590.4 | 1.5     |
| M6              | -2584.8 | 7.1     |
| M7              | -2583.7 | 8.2     |
| M4              | -2573.7 | 18.2    |
| M5              | -2571.2 | 20.7    |
| M3              | -2566.2 | 25.7    |
| M2              | -2353.9 | 238.1   |
| M1              | -2301.4 | 290.5   |
| M0              | -2300.3 | 291.6   |

Table: Supplemental Table 6.1: Comparison of GEE model fitness for variable selection using quasi-likelihood information criteria.

Given the number of possible combinations of outcome and predictor variables,
only ISI and ISSI-2 with total triacylglycerol fatty acids (nmol/mL) were used to
compare various GEE models and to select a final model.  Baseline age was used
as including both the original age and the time variable would result in
collinearity.  Column names: QIC is the quasi-likelihood information criteria
(smaller values, eg. larger negative values, indicate a better fit compared to
other models), Delta is the QIC minus the lowest QIC (models with delta <10 are 
considered equivalent).  Models were:

- M0: log(ISSI-2) or log(ISI) = total triacylglycerol fatty acids (nmol/mL) + years from baseline
- M1: M0 + fatty acid by time interaction
- M2: M0 + sex + ethnicity + baseline age
- M3: M2 + waist
- M4: M3 + ALT
- M5: M4 + physical activity (MET)
- M6: M5 + total NEFA
- M7: M6 + alcohol intake
- M8: M7 + family history of diabetes
- M9: M8 + smoking status

\newpage\newpage


| TAGFA   |  Concentrations (nmol/mL)  |  Proportion (mol%)  |
|:--------|:--------------------------:|:-------------------:|
| 18:3n-3 |        45.2 (31.1)         |      1.5 (0.6)      |
| 20:5n-3 |         9.9 (8.1)          |      0.4 (0.4)      |
| 22:5n-3 |         8.3 (5.7)          |      0.3 (0.2)      |
| 22:6n-3 |        16.7 (14.5)         |      0.6 (0.6)      |
| 18:2n-6 |       548.6 (298.7)        |     18.0 (4.2)      |
| 18:3n-6 |         15.1 (9.9)         |      0.5 (0.2)      |
| 20:2n-6 |         10.2 (4.7)         |      0.4 (0.1)      |
| 20:3n-6 |         10.2 (6.0)         |      0.3 (0.1)      |
| 20:4n-6 |        38.2 (19.1)         |      1.3 (0.5)      |
| 22:4n-6 |         4.6 (2.9)          |      0.1 (0.1)      |
| 14:1n-7 |         5.1 (6.1)          |      0.1 (0.1)      |
| 16:1n-7 |        126.1 (98.8)        |      3.8 (1.3)      |
| 18:1n-7 |        71.6 (34.8)         |      2.4 (0.4)      |
| 18:1n-9 |       1168.5 (592.2)       |     37.8 (3.7)      |
| 20:1n-9 |         8.5 (5.2)          |      0.3 (0.2)      |
| 22:1n-9 |         1.0 (0.6)          |      0.0 (0.0)      |
| 24:1n-9 |         2.2 (4.0)          |      0.1 (0.1)      |
| 14:0    |        62.4 (59.0)         |      1.8 (1.0)      |
| 16:0    |       868.0 (556.2)        |     26.6 (4.4)      |
| 18:0    |        113.6 (63.4)        |      3.7 (0.8)      |
| 20:0    |         1.9 (1.3)          |      0.1 (0.0)      |
| 22:0    |         1.5 (1.2)          |      0.1 (0.0)      |
| Total   |      3137.5 (1686.6)       |                     |

Table: Supplemental Table 6.2: Concentration (nmol/mL) and relative percent (mol%) values of triacylglycerol fatty acids in PROMISE participants at the baseline visit (2004-2006).

\footnotesize
\newpage\newpage

| Fatty acid   | log(HOMA2-%S)          | log(ISI)               | log(IGI/IR)            | log(ISSI-2)          |
|:-------------|:-----------------------|:-----------------------|:-----------------------|:---------------------|
| **Totals**   |                        |                        |                        |                      |
| BaseTAG      | -23.5 (-29.0, -17.7)\* | -24.0 (-29.4, -18.2)\* | -17.2 (-23.9, -9.8)\*  | -9.9 (-14.0, -5.6)\* |
| Total        | -22.4 (-27.8, -16.7)\* | -23.1 (-28.5, -17.2)\* | -15.8 (-22.2, -8.8)\*  | -9.2 (-13.2, -5.1)\* |
| **nmol/mL**  |                        |                        |                        |                      |
| 14:0         | -19.1 (-23.7, -14.2)\* | -19.6 (-24.2, -14.8)\* | -10.8 (-16.5, -4.7)\*  | -6.7 (-10.0, -3.2)\* |
| 16:0         | -23.2 (-28.5, -17.5)\* | -23.9 (-29.2, -18.2)\* | -16.4 (-22.9, -9.4)\*  | -9.6 (-13.5, -5.4)\* |
| 18:0         | -21.1 (-26.6, -15.2)\* | -21.6 (-27.3, -15.5)\* | -15.3 (-21.8, -8.2)\*  | -9.0 (-12.9, -4.9)\* |
| 20:0         | -13.1 (-17.1, -8.9)\*  | -13.0 (-17.6, -8.2)\*  | -8.4 (-13.7, -2.7)\*   | -5.7 (-8.6, -2.8)\*  |
| 22:0         | -12.6 (-17.1, -7.9)\*  | -13.1 (-17.8, -8.2)\*  | -2.2 (-7.9, 3.9)       | -1.9 (-4.7, 1.0)     |
| 18:1n-9      | -21.6 (-26.2, -16.8)\* | -22.1 (-26.8, -17.1)\* | -15.9 (-21.8, -9.5)\*  | -9.1 (-12.6, -5.4)\* |
| 20:1n-9      | -15.0 (-19.2, -10.7)\* | -14.9 (-19.0, -10.6)\* | -12.7 (-18.8, -6.2)\*  | -7.0 (-10.2, -3.6)\* |
| 22:1n-9      | -13.8 (-19.3, -7.8)\*  | -11.9 (-18.1, -5.3)\*  | -11.1 (-15.7, -6.4)\*  | -5.5 (-8.1, -2.9)\*  |
| 24:1n-9      | -4.5 (-8.6, -0.2)      | -6.4 (-10.7, -1.8)\*   | 4.0 (-1.6, 9.8)        | 2.0 (-0.7, 4.7)      |
| 14:1n-7      | -14.8 (-19.3, -10.1)\* | -15.2 (-19.7, -10.4)\* | -9.0 (-14.4, -3.2)\*   | -5.4 (-8.4, -2.3)\*  |
| 16:1n-7      | -19.3 (-24.4, -13.8)\* | -19.8 (-25.0, -14.4)\* | -14.2 (-20.7, -7.1)\*  | -8.1 (-12.0, -4.1)\* |
| 18:1n-7      | -22.3 (-26.2, -18.3)\* | -22.9 (-26.9, -18.7)\* | -16.0 (-21.3, -10.4)\* | -9.0 (-12.0, -5.9)\* |
| 18:2n-6      | -19.0 (-23.7, -14.0)\* | -19.5 (-24.3, -14.4)\* | -12.0 (-17.7, -5.8)\*  | -7.2 (-10.7, -3.6)\* |
| 18:3n-6      | -13.3 (-18.2, -8.1)\*  | -13.2 (-18.4, -7.8)\*  | -9.1 (-15.0, -2.8)\*   | -5.3 (-8.8, -1.7)\*  |
| 20:2n-6      | -15.8 (-20.9, -10.4)\* | -16.3 (-21.5, -10.9)\* | -12.8 (-19.3, -5.9)\*  | -6.8 (-10.7, -2.8)\* |
| 20:3n-6      | -17.7 (-22.8, -12.2)\* | -18.5 (-23.7, -13.0)\* | -12.5 (-18.6, -5.9)\*  | -7.3 (-10.9, -3.4)\* |
| 20:4n-6      | -17.2 (-24.6, -9.1)\*  | -18.3 (-25.9, -9.9)\*  | -13.3 (-21.6, -4.1)\*  | -7.5 (-12.7, -1.9)\* |
| 22:4n-6      | -18.1 (-25.8, -9.6)\*  | -18.7 (-26.4, -10.2)\* | -10.1 (-17.6, -1.9)\*  | -5.8 (-10.4, -0.9)\* |
| 18:3n-3      | -15.8 (-19.7, -11.7)\* | -16.8 (-20.7, -12.6)\* | -9.5 (-14.3, -4.5)\*   | -6.0 (-8.6, -3.3)\*  |
| 20:5n-3      | -5.5 (-9.3, -1.5)\*    | -8.4 (-12.2, -4.5)\*   | -3.7 (-8.5, 1.3)       | -3.2 (-5.8, -0.5)\*  |
| 22:5n-3      | -7.7 (-11.8, -3.3)\*   | -8.0 (-12.3, -3.5)\*   | -11.0 (-15.7, -5.9)\*  | -6.7 (-9.1, -4.2)\*  |
| 22:6n-3      | -7.3 (-11.1, -3.3)\*   | -10.1 (-14.0, -6.0)\*  | -4.2 (-8.6, 0.4)       | -3.4 (-5.9, -0.9)\*  |
| **mol%**     |                        |                        |                        |                      |
| 14:0         | -14.0 (-17.8, -10.0)\* | -13.8 (-17.8, -9.6)\*  | -4.8 (-10.5, 1.4)      | -3.6 (-6.6, -0.6)\*  |
| 16:0         | -20.0 (-23.6, -16.2)\* | -20.4 (-24.2, -16.5)\* | -13.5 (-18.9, -7.7)\*  | -8.0 (-10.9, -5.0)\* |
| 18:0         | 3.7 (-0.9, 8.6)        | 3.4 (-1.4, 8.3)        | 1.4 (-5.2, 8.5)        | 0.5 (-2.7, 3.8)      |
| 20:0         | 9.5 (2.5, 16.9)\*      | 9.6 (2.3, 17.4)\*      | 7.6 (0.8, 15.0)\*      | 3.0 (-0.6, 6.7)      |
| 22:0         | 4.2 (-0.7, 9.3)        | 4.2 (-0.4, 8.9)        | 11.0 (5.1, 17.2)\*     | 5.2 (2.6, 7.9)\*     |
| 18:1n-9      | 14.6 (9.1, 20.4)\*     | 15.5 (9.5, 22.0)\*     | 5.6 (-0.5, 12.0)       | 4.4 (1.4, 7.5)\*     |
| 20:1n-9      | 7.3 (1.9, 13.1)\*      | 8.2 (2.3, 14.4)\*      | 2.1 (-4.8, 9.6)        | 1.8 (-2.0, 5.7)      |
| 22:1n-9      | 10.3 (4.8, 16.1)\*     | 12.7 (7.1, 18.6)\*     | 6.6 (-0.3, 14.0)       | 4.3 (0.8, 7.8)\*     |
| 24:1n-9      | 3.2 (-1.5, 8.0)        | 1.4 (-3.2, 6.3)        | 10.5 (4.3, 17.0)\*     | 4.7 (1.8, 7.8)\*     |
| 14:1n-7      | -10.0 (-14.2, -5.6)\*  | -9.6 (-14.0, -5.0)\*   | -4.1 (-9.7, 2.0)       | -2.8 (-5.7, 0.3)     |
| 16:1n-7      | -10.9 (-15.2, -6.4)\*  | -10.7 (-15.2, -6.0)\*  | -8.5 (-14.2, -2.3)\*   | -5.0 (-7.9, -1.9)\*  |
| 18:1n-7      | 7.2 (1.7, 12.9)\*      | 7.9 (2.4, 13.8)\*      | 5.3 (-1.0, 12.0)       | 3.8 (0.7, 7.0)\*     |
| 18:2n-6      | 12.6 (6.7, 18.8)\*     | 13.1 (7.1, 19.6)\*     | 10.0 (3.4, 17.1)\*     | 5.1 (1.9, 8.5)\*     |
| 18:3n-6      | 8.4 (3.7, 13.2)\*      | 10.3 (5.5, 15.3)\*     | 7.3 (1.2, 13.8)\*      | 3.9 (1.0, 6.9)\*     |
| 20:2n-6      | 21.4 (16.2, 26.9)\*    | 21.9 (16.3, 27.7)\*    | 9.3 (2.9, 16.2)\*      | 6.6 (3.5, 9.8)\*     |
| 20:3n-6      | 4.3 (-0.1, 9.0)        | 3.6 (-1.0, 8.4)        | 1.6 (-3.2, 6.8)        | 0.7 (-1.7, 3.2)      |
| 20:4n-6      | 10.1 (5.0, 15.4)\*     | 8.9 (3.1, 15.0)\*      | 3.5 (-2.4, 9.8)        | 2.4 (-0.5, 5.4)      |
| 22:4n-6      | 1.6 (-3.1, 6.4)        | 1.6 (-3.0, 6.3)        | 6.1 (-0.5, 13.1)       | 3.3 (0.4, 6.4)\*     |
| 18:3n-3      | 5.7 (0.6, 11.1)\*      | 5.3 (-0.2, 11.2)       | 5.9 (-1.0, 13.3)       | 2.3 (-1.0, 5.7)      |
| 20:5n-3      | 8.2 (1.7, 15.0)\*      | 4.7 (-1.9, 11.6)       | 7.1 (1.6, 12.9)\*      | 2.6 (-0.1, 5.4)      |
| 22:5n-3      | 12.9 (7.4, 18.6)\*     | 12.1 (6.0, 18.5)\*     | 3.2 (-2.8, 9.5)        | 1.6 (-1.4, 4.6)      |
| 22:6n-3      | 5.4 (0.2, 10.8)        | 2.2 (-3.0, 7.7)        | 5.6 (0.6, 10.9)\*      | 2.0 (-0.6, 4.7)      |

Table: Supplemental Table 6.3: Raw estimates and confidence interval values for *time*-adjusted GEE models of the association of the triacylglycerol fatty acids (mol% and nmol/mL) and total clinically-measured TAG with insulin sensitivity and beta-cell function outcomes using the 6 year longitudinal data from the PROMISE cohort. Estimates represent a percent difference in the outcome per SD increase in the fatty acid. P-values were adjusted for the BH false discovery rate, with an asterisk (*) denoting a significant (p<0.05) association.

\footnotesize
\newpage\newpage

| Fatty acid   | log(HOMA2-%S)         | log(ISI)               | log(IGI/IR)          | log(ISSI-2)         |
|:-------------|:----------------------|:-----------------------|:---------------------|:--------------------|
| **Totals**   |                       |                        |                      |                     |
| BaseTAG      | -13.9 (-18.2, -9.4)\* | -14.7 (-19.1, -10.1)\* | -7.9 (-13.8, -1.7)\* | -4.0 (-7.2, -0.8)\* |
| Total        | -13.3 (-17.5, -9.0)\* | -14.3 (-18.7, -9.7)\*  | -6.8 (-12.3, -1.0)\* | -3.7 (-6.7, -0.6)\* |
| **nmol/mL**  |                       |                        |                      |                     |
| 14:0         | -11.0 (-14.5, -7.3)\* | -12.1 (-15.8, -8.2)\*  | -4.1 (-9.0, 1.0)     | -2.5 (-5.0, 0.1)    |
| 16:0         | -13.7 (-17.8, -9.4)\* | -14.7 (-19.0, -10.2)\* | -7.3 (-12.8, -1.5)\* | -3.9 (-6.9, -0.8)\* |
| 18:0         | -12.5 (-16.6, -8.1)\* | -13.2 (-17.7, -8.5)\*  | -7.4 (-13.0, -1.4)\* | -4.1 (-7.2, -1.0)\* |
| 20:0         | -7.2 (-10.5, -3.8)\*  | -7.7 (-11.6, -3.6)\*   | -4.5 (-9.6, 1.0)     | -3.4 (-5.9, -0.7)\* |
| 22:0         | -8.6 (-12.4, -4.7)\*  | -8.5 (-12.7, -4.2)\*   | -1.5 (-7.0, 4.2)     | -1.5 (-4.0, 1.0)    |
| 18:1n-9      | -12.7 (-16.5, -8.8)\* | -13.6 (-17.8, -9.3)\*  | -6.5 (-11.9, -0.7)   | -3.4 (-6.3, -0.4)   |
| 20:1n-9      | -7.7 (-10.7, -4.5)\*  | -7.9 (-11.3, -4.3)\*   | -5.3 (-10.7, 0.4)    | -2.4 (-5.1, 0.3)    |
| 22:1n-9      | -6.2 (-10.1, -2.2)\*  | -4.8 (-9.3, 0.0)       | -4.2 (-9.1, 0.9)     | -1.2 (-3.6, 1.2)    |
| 24:1n-9      | -3.6 (-6.5, -0.5)\*   | -4.4 (-7.8, -1.0)\*    | 1.6 (-3.3, 6.7)      | 0.8 (-1.5, 3.1)     |
| 14:1n-7      | -7.8 (-11.1, -4.4)\*  | -8.8 (-12.3, -5.2)\*   | -3.2 (-8.0, 1.8)     | -1.8 (-4.1, 0.6)    |
| 16:1n-7      | -10.1 (-13.8, -6.2)\* | -10.9 (-14.8, -6.8)\*  | -5.1 (-10.6, 0.7)    | -2.5 (-5.2, 0.4)    |
| 18:1n-7      | -12.3 (-16.1, -8.3)\* | -12.9 (-17.2, -8.5)\*  | -5.2 (-10.7, 0.8)    | -2.3 (-5.1, 0.6)    |
| 18:2n-6      | -12.1 (-15.9, -8.1)\* | -12.7 (-16.7, -8.5)\*  | -5.7 (-10.8, -0.2)   | -3.4 (-6.3, -0.5)\* |
| 18:3n-6      | -8.3 (-12.1, -4.3)\*  | -8.7 (-13.0, -4.2)\*   | -5.5 (-10.5, -0.2)   | -3.0 (-5.7, -0.2)   |
| 20:2n-6      | -8.2 (-11.9, -4.2)\*  | -8.8 (-12.9, -4.5)\*   | -5.0 (-10.1, 0.2)    | -2.1 (-4.8, 0.7)    |
| 20:3n-6      | -9.5 (-13.0, -5.8)\*  | -10.5 (-14.3, -6.5)\*  | -4.7 (-9.4, 0.3)     | -2.5 (-5.1, 0.1)    |
| 20:4n-6      | -9.3 (-14.0, -4.3)\*  | -10.5 (-15.6, -5.0)\*  | -6.3 (-12.2, -0.1)   | -3.2 (-6.5, 0.3)    |
| 22:4n-6      | -9.9 (-14.8, -4.7)\*  | -10.5 (-15.4, -5.2)\*  | -4.2 (-9.5, 1.4)     | -2.1 (-5.0, 1.0)    |
| 18:3n-3      | -10.2 (-13.4, -7.0)\* | -11.2 (-14.6, -7.8)\*  | -5.3 (-10.1, -0.3)   | -3.5 (-5.8, -1.1)\* |
| 20:5n-3      | -3.3 (-6.5, 0.0)      | -6.0 (-9.4, -2.4)\*    | -1.0 (-5.2, 3.5)     | -1.7 (-3.8, 0.5)    |
| 22:5n-3      | -2.8 (-6.3, 0.9)      | -3.9 (-7.9, 0.3)       | -2.8 (-7.7, 2.3)     | -2.2 (-4.5, 0.1)    |
| 22:6n-3      | -4.9 (-8.5, -1.2)\*   | -7.2 (-11.0, -3.3)\*   | -0.2 (-4.2, 3.9)     | -1.4 (-3.4, 0.7)    |
| **mol%**     |                       |                        |                      |                     |
| 14:0         | -9.0 (-12.0, -5.9)\*  | -9.7 (-13.1, -6.2)\*   | -2.1 (-7.1, 3.1)     | -1.9 (-4.3, 0.6)    |
| 16:0         | -11.6 (-14.7, -8.3)\* | -12.3 (-15.8, -8.7)\*  | -6.3 (-11.4, -1.0)\* | -3.4 (-5.9, -0.8)\* |
| 18:0         | 2.2 (-1.3, 5.9)       | 1.9 (-1.9, 5.9)        | -1.6 (-7.4, 4.6)     | -1.3 (-4.0, 1.4)    |
| 20:0         | 5.9 (1.6, 10.5)\*     | 5.9 (1.1, 10.9)\*      | 2.5 (-3.2, 8.5)      | 0.3 (-2.7, 3.3)     |
| 22:0         | 1.0 (-2.3, 4.4)       | 1.6 (-1.8, 5.1)        | 3.7 (-2.5, 10.4)     | 1.1 (-1.5, 3.8)     |
| 18:1n-9      | 8.7 (4.4, 13.1)\*     | 9.0 (4.1, 14.1)\*      | 4.4 (-1.1, 10.3)     | 3.5 (0.7, 6.3)\*    |
| 20:1n-9      | 5.4 (1.1, 9.8)\*      | 6.2 (1.1, 11.5)\*      | 1.9 (-4.4, 8.7)      | 1.6 (-2.0, 5.3)     |
| 22:1n-9      | 7.3 (3.4, 11.3)\*     | 8.9 (4.7, 13.3)\*      | 4.1 (-1.8, 10.4)     | 2.8 (0.0, 5.7)      |
| 24:1n-9      | 0.6 (-3.0, 4.3)       | 0.3 (-3.6, 4.4)        | 4.2 (-1.6, 10.3)     | 1.2 (-1.3, 3.9)     |
| 14:1n-7      | -6.0 (-9.2, -2.7)\*   | -6.6 (-10.1, -2.9)\*   | -1.3 (-6.2, 3.9)     | -0.9 (-3.3, 1.5)    |
| 16:1n-7      | -5.7 (-9.1, -2.2)\*   | -5.8 (-9.5, -1.8)\*    | -1.9 (-7.6, 4.3)     | -0.9 (-3.6, 2.0)    |
| 18:1n-7      | 7.8 (3.7, 12.2)\*     | 8.8 (4.3, 13.5)\*      | 8.3 (2.4, 14.5)\*    | 5.4 (2.7, 8.2)\*    |
| 18:2n-6      | 5.4 (1.2, 9.9)\*      | 6.8 (2.1, 11.8)\*      | 2.1 (-3.3, 7.7)      | 0.6 (-2.0, 3.3)     |
| 18:3n-6      | 2.9 (-1.1, 7.1)       | 4.3 (-0.2, 9.0)        | 0.3 (-5.2, 6.0)      | 0.1 (-2.5, 2.8)     |
| 20:2n-6      | 14.2 (10.2, 18.3)\*   | 15.2 (10.8, 19.9)\*    | 4.5 (-1.1, 10.5)     | 3.8 (1.1, 6.6)\*    |
| 20:3n-6      | 4.3 (0.7, 8.0)\*      | 4.1 (0.1, 8.2)         | 1.2 (-3.1, 5.7)      | 0.6 (-1.5, 2.8)     |
| 20:4n-6      | 6.5 (2.5, 10.7)\*     | 5.8 (1.0, 10.9)\*      | -0.4 (-5.4, 4.9)     | 0.3 (-2.1, 2.8)     |
| 22:4n-6      | 1.9 (-1.5, 5.5)       | 2.4 (-1.3, 6.2)        | 2.0 (-4.0, 8.3)      | 1.3 (-1.3, 3.9)     |
| 18:3n-3      | 1.1 (-2.5, 4.8)       | 1.3 (-2.7, 5.5)        | 0.4 (-5.2, 6.3)      | -0.9 (-3.5, 1.8)    |
| 20:5n-3      | 4.1 (-0.1, 8.5)       | 1.3 (-3.4, 6.2)        | 3.4 (-0.8, 7.8)      | 0.6 (-1.5, 2.7)     |
| 22:5n-3      | 8.6 (4.6, 12.7)\*     | 7.6 (2.9, 12.5)\*      | 3.0 (-1.8, 8.0)      | 1.2 (-1.2, 3.6)     |
| 22:6n-3      | 2.3 (-1.2, 5.9)       | -0.1 (-3.9, 4.0)       | 3.4 (-0.5, 7.5)      | 0.7 (-1.4, 2.7)     |

Table: Supplemental Table 6.4: Raw estimates and confidence interval values for *fully*-adjusted GEE models of the association of the triacylglycerol fatty acids (mol\% and nmol/mL) and total clinically-measured TAG with insulin sensitivity and beta-cell function outcomes using the 6 year longitudinal data from the PROMISE cohort. Variables controlled for were follow-up time, waist circumference, baseline age, ethnicity, sex, ALT, physical activity, and total NEFA. Estimates represent a percent difference in the outcome per SD increase in the fatty acid. P-values were adjusted for the BH false discovery rate, with an asterisk (*) denoting a significant (p<0.05) association.

\normalsize

\begin{figure}
\centering
\includegraphics[width=\textwidth]{../img/flowDiagramSample.pdf}
\caption{Supplemental Figure 6.1: CONSORT diagram of PROMISE participants over the 3 visits.}
\end{figure}


\begin{figure}
\centering
\includegraphics[width=\textwidth]{../img/dagitty-model-is.png}
\caption{Supplemental Figure 6.2: Directed acyclic graphic output from the DAGitty online software for insulin sensitivity.}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{../img/dagitty-model-bcf.png}
\caption{Supplemental Figure 6.3: Directed acyclic graphic output from the DAGitty online software for beta-cell function.}
\end{figure}


\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/fig_gee_adj_nowaist-1.pdf}
\caption[Supplemental Figure 6.4: Fully-adjusted (without waist) GEE models of TAGFA with outcomes.]{Supplemental Figure 6.4: Fully-adjusted (without waist size) GEE models of the association of the triacylglycerol fatty acids (mol\% and nmol/mL) and total clinically-measured TAG with insulin sensitivity and beta-cell function outcomes using the 6 year longitudinal data from the PROMISE cohort. Variables controlled for were follow-up time, baseline age, ethnicity, sex, ALT, physical activity, and total NEFA. X-axis values represent a percent difference in the outcome per SD increase in the fatty acid. P-values were adjusted for the BH false discovery rate, with the largest dot representing a significant (p<0.05) association.}
\end{figure}

