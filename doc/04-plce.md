
# Longitudinal associations of phospholipid and cholesteryl ester fatty acids with disorders underlying diabetes

\chaptermark{Phospholipid, cholesteryl esters, and diabetes}

This chapter has been published in the Journal of Clinical Endocrinology and Metabolism (see citation below). The formatting of this article have been slightly modified to fit the requirements of a PhD thesis for the University of Toronto.

Johnston, L. W.; Harris, S. B.; Retnakaran, R.; Zinman, B.; Giacca, A.; Liu, Z.; Bazinet, R. P. & Hanley, A. J. Longitudinal associations of phospholipid and cholesteryl ester fatty acids with disorders underlying diabetes. J Clin Endocrinol Metab, 2016, 101, 2536-2544. \newline DOI: https://doi.org/10.1210/jc.2015-4267

\begin{chapabstract}

\textbf{Context}: Specific serum fatty acid (FA) profiles predict the development of 
incident type 2 diabetes; however, limited longitudinal data exist exploring
their role in the progression of insulin sensitivity (IS) and beta-cell function.

\textbf{Objective}: To examine the longitudinal associations of the FA composition of
serum phospholipid (PL) and cholesteryl ester (CE) fractions with IS and
beta-cell function over 6-years.

\textbf{Design}: The Prospective Metabolism and Islet Cell Evaluation (PROMISE)
cohort is a longitudinal observation study, with clinic visits occurring every 3
years. Three visits have been completed, totaling 6-years of follow-up.

\textbf{Setting}: Individuals (n=477) at-risk for diabetes recruited from the general
population in London and Toronto, Canada.

\textbf{Main Outcome Measures}: Values from an oral glucose tolerance test were used
to compute 1/HOMA-IR and the Matsuda index (ISI) for IS, and the Insulinogenic
Index over HOMA-IR (IGI/IR) and the Insulin Secretion-Sensitivity Index-2
(ISSI-2) for beta-cell function. Thin-layer-chromatograph and gas-chromatograph
quantified FA. Generalized estimating equations (GEE) was used for the
analysis.

\textbf{Results}: IS and beta-cell function declined by 8.3-19.4\% over 6-years.  In 
fully adjusted GEE models, PL \textit{cis}-vaccenate (18:1n-7) was positively 
associated with all outcomes, while gamma-linolenate (GLA; 18:3n-6) and stearate
(18:0) were negatively associated with IS.  Tests for time interactions revealed
PL eicosadienoate (20:2n-6) and palmitate (16:0), and CE dihomo-gamma-linolenate
(20:3n-6), GLA, and palmitate had stronger associations with the outcomes after
longer follow-up.

\textbf{Conclusions}: In a Canadian population at-risk for diabetes, we found that
higher PL stearate and GLA, and lower \textit{cis}-vaccenic acid, predicted
consistently lower IS and beta-cell function over 6-years.

\end{chapabstract}

## Introduction

The serum composition of fatty acids and their role in the development
of type 2 diabetes mellitus is an area of active investigation.
Individual fatty acids, which are either consumed from the diet or are
synthesized *de novo*, have been shown to have differential
associations with the incidence of diabetes depending on carbon chain
length and the degree of unsaturation
[@Wang2003a; @Forouhi2014a; @Kroger2011a; @Hodge2007a].  However, the
pathogenesis of diabetes is complex and multifactorial, and
understanding the mechanisms underlying these associations is
imperative to the development of effective diabetes prevention
strategies.

Worsening insulin sensitivity and beta-cell function play central
roles in the pathophysiology of diabetes
[@Bergman2002; @Fu2013; @Surampudi2009].  Experimental research
suggests that certain fatty acids influence insulin sensitivity
and beta-cell function to a greater degree than others.  For example,
saturated fatty acids such as palmitic acid (16:0) and stearic acid
(18:0) exert a strong lipotoxic effect, impairing beta-cell function
and inhibiting insulin signaling, while some polyunsaturated fatty
acids may protect against lipotoxicity
[@Cnop2008; @Giacca2011a; @Martins2012a; @Xiao2006].  In humans, these
findings have been supported by a limited number of cross-sectional
studies that have used indices of insulin sensitivity and secretion
[@Ma2015a; @Mahendran2014a; @Patel2010a; @Zhou2009b].

Importantly, limited longitudinal data exist on associations of
fatty acid profiles with detailed indices of insulin sensitivity and,
to our knowledge, no study to date has examined the association of
serum fatty acid composition with the longitudinal decline in
beta-cell function.  In this context, our objective for this current
study was to investigate the longitudinal associations of fatty acids
in the serum phospholipid (PL) and cholesteryl ester (CE) fractions
with the progression of insulin sensitivity and beta-cell function, as
well as their interactions over time.

## Materials and Methods



The Prospective Metabolism and Islet cell Evaluation (PROMISE) cohort
[@Hanley2009;@Kayaniyil2010] is a longitudinal observational study of
participants with one or more risk factors for type 2 diabetes
mellitus, including obesity, hypertension, family history of diabetes,
and/or a history of gestational diabetes or birth of a macrosomic
infant.  Participants aged 30 years and older were recruited from the
general population into the PROMISE cohort (n=736) from Toronto and
London, Ontario, Canada in 2004-2006.  Annual telephone contact is
maintained and follow-up examinations occur every
three years; three examination visits have been completed to date
(2004-2006, 2007-2009, and 2010-2013).  The current study used data on
participants who did not have diabetes at baseline, who returned for
one or more follow-up examinations, and who had samples available for
fatty acid measurements (n=477).  A diagram of the sample size at each
visit is shown in Supplemental Figure 4.1.  At each examination,
participants undergo extensive metabolic characterization,
anthropometric measurements, and structured questionnaires on
lifestyle and sociodemographics.  Research ethics approval was
obtained from Mount Sinai Hospital and the University of Western
Ontario.  Data collection methods were standardized across the 2
centres and research nurses were centrally trained.  <!--Over the
6-yrs, {{n return each visit}} returned for the 6-yr clinic visit
({{pct loss to followup}}% loss to follow-up).  By the 6-yr visit, {{n
converted to IFG}} converted into IFG, {{n converted to IGT}}
converted into IGT, and {{n converted to DM}} into diabetes.-->

### Anthropometric and metabolic measurements

Height, weight, and waist circumference (WC) were measured at all
clinic examinations using standard procedures.  WC was measured at the
natural waist, defined as the narrowest part of the torso between the
umbilicus and the xiphoid process.  Height and weight were measured using standard procedures. 
BMI was calculated by dividing weight (kg) by height (m)
squared.

A fasting blood sample was drawn after an 8-12 hour fast at
each clinic examination.  A 75g oral glucose tolerance test (OGTT) was
conducted, with additional blood samples drawn at 30 minutes and 2
hours post-glucose load.  All blood samples were processed and frozen
at -70°C for the determination of blood biomarkers.  Alanine
aminotransferase (ALT) was measured using standard laboratory
procedures.  Cholesterol, HDL, and triacylglycerols (TAG) were
measured using Roche Modular's enzymatic colometric tests
(Mississauga, ON).  Both specific insulin and glucose were derived
from the OGTT at fasting, 30 minute, and 2 hour time points.  Specific
insulin was measured using the Elecsys 1010 (Roche Diagnostics, Basel,
Switzerland) immunoassay analyzer and electrochemiluminescence
immunoassay.  This assay shows 0.05% cross-reactivity to intact human
pro-insulin and the Des 31,32 circulating split form (Linco Res. Inc),
and has a CV of 9.3%.  Glucose was determined using an enzymatic
hexokinase (Roche Modular, Roche Diagnostics) with a detection range
of 0.11 (2 mg/dL) to 41.6 mmol/L.  The inter-assay %CV is <1.1% and
intra-assay %CV is < 1.9%.  All assays were performed at the Banting
and Best Diabetes Centre Core Lab at the University of Toronto.
Impaired fasting glucose (IFG), impaired glucose tolerance (IGT), and
diabetes were categorized using the 2006 WHO criteria [@WHO2006].

### Measurement of Serum Fatty Acids

Fatty acid PL and CE compositions were quantified using stored fasting
serum samples from the baseline visit that had been frozen at -70°C
for 4-6 years; these samples had not been exposed to any freeze-thaw
cycles.  Previous literature has documented that serum fatty acids are
stable at these temperatures for up to 10 years [@Matthan2010a]. A
known amount of 1,2-diheptadecanoyl-sn-glycero-3-phosphocholine and
cholesteryl heptadecanoate (17:0) was added to the serum, as internal
standards, prior to extracting total lipids according to the method of
Folch [@Folch1957a].  A portion of the total lipid extract was added
onto thin layer chromatography G-plates (Cat. #10011; Analtech,
Newark, DE)
to isolate each fraction. Lipid fraction
bands were 
converted
to fatty acid methyl esters and
were separated and quantified
using a Varian-430 gas chromatograph (Varian, Lake Forest, CA, USA)
equipped with a Varian Factor Four capillary column 
and a flame ionization
detector.  Samples were injected in splitless mode. Peaks were
identified by retention times of fatty acid methyl ester
standards (Nu-Chek Prep, Inc., Elysian, MN, USA). Fatty acid
concentrations (nmol/mL) were calculated by proportional comparison of
gas chromatography peak areas to that of the internal standards
[@Nishi2014a; @Chen2011a].  There were 22 fatty acids measured in
each of the PL and CE fractions, ranging from 14:0 to 24:1n-9. While
triacylglycerol and non-esterified fatty acid (NEFA) serum fractions were also
quantified, we decided *a priori* to describe the PL and CE fractions in this
paper and the other fractions in separate reports given their complexity and unique
biology.

### Variable calculations

Glucose and insulin values from the OGTT were used to compute insulin
sensitivity and beta-cell function indices.  Insulin sensitivity was
assessed using the homeostasis model assessment of insulin sensitivity
(1/HOMA-IR) [@Matthews1985] and the Insulin Sensitivity Index (ISI)
[@Matsuda1999].  
1/HOMA-IR largely
reflects hepatic insulin sensitivity, while ISI reflects whole-body
insulin sensitivity [@AbdulGhani2007].  Beta-cell function was
assessed using the Insulinogenic Index [@Wareham1995] over HOMA-IR
(IGI/IR) and the Insulin Secretion-Sensitivity Index-2 (ISSI-2)
[@Retnakaran2009].  IGI/IR is a measure of the first phase insulin
secretion while the more recently developed ISSI-2 is analogous to the
disposition index. These insulin sensitivity and beta-cell function
indices have been validated against gold standard measures
[@Matthews1985; @Matsuda1999; @Retnakaran2009].


### Statistical analysis

The outcome variables in the analyses were 1/HOMA-IR, ISI, IGI/IR, and
ISSI-2, while the primary predictor variables were the 22 fatty acids
in each of the PL or CE fractions using mole percent (mol%) of the
total fraction.  As a sensitivity analysis, we replaced the mol% fatty
acids in the fully-adjusted models with the concentration data and
analyzed these variables as primary predictors with the outcome
variables.  Outcome variables were highly skewed and therefore were
log-transformed prior to analysis.  Spearman correlation coefficients
were computed for the PL and CE fatty acids with other continuous
variables.

For the primary analysis, generalized estimating equation (GEE) models
[@Zeger1986a] were used to determine the associations between the
outcome variables and the predictor variables.  An auto-regressive of
order 1 working correlation matrix was specified for the GEE models
given the longitudinal design, though other correlation matrices
(eg. exchangeable) had similar model fits when evaluated (data not
shown).  GEE is well suited to longitudinal data from cohorts, as it
is flexible to missed visits.  The predictor variables were scaled
(mean-centered and standardized) to compare 
across tests.  Since PL and CE fatty acids were only
quantified at the baseline visit, they were set as *time-independent*
predictor variables (held constant over time).  The outcome variables
1/HOMA-IR, ISI, IGI/IR, and ISSI-2 were set as *time-dependent*
variables as they were measured at each examination visit.  No
imputation was conducted on missing values.

Covariates included in the GEE models were identified using the
previous literature, recommendations from directed acyclic graph
theory [@Greenland1999a] on causal pathways that were based on the
biological mechanisms underlying the hypothesized associations, and
using quasi-likelihood information criterion to minimize information
loss in potential models by balancing model fit with model complexity.
A comparison of models with different covariates and the resulting
quasi-likelihood information criterion calculations are shown in
Supplemental Table 4.1.  The final GEE model (M9) we selected had
time, sex, ethnicity, baseline age, waist circumference, total
NEFA, ALT, and family history of diabetes
as covariates.  Continuous covariates (excluding time) were scaled and
the resulting GEE beta coefficients were
exponentiated. <!--(exponentiated, minus 1, and multiplied by 100)-->
Scaling, log-transforming, and exponentiating allowed interpreting the
GEE beta coefficients as an expected percent difference in the outcome
variable for every SD increase in the predictor variable given the
covariates are held constant.  Interaction for time with each
predictor variables were included in the GEE model. Two additional sensitivity
analyses were performed. One analysis included baseline pre-diabetes in the GEE 
models. Another analysis was conducted on conversion to dysglycemia over the 6-years with the fatty acids in the two lipid fractions using logistic regression
for comparison with the primary analysis.

Significance was set as p<0.05 for all tests.
All computed p-values were
adjusted using the Benjamini-Hochberg False Discovery Rate [@Benjamini1995a] to correct
for multiple testing.  Analyses were conducted in R 3.4.0
statistical computing environment [@Rbase], using the following
packages: ggplot2 2.2.1 for the graphics
[@Wickham2009a] and geepack 1.2.1 for the GEE
modeling [@Hoejsgaard2006a].  The code to generate the results shown
in this manuscript can be found at:
http://dx.doi.org/10.6084/m9.figshare.1552011


## Results







### Basic characteristics

At the baseline visit, participants recruited into the PROMISE cohort
had a mean age of 50.2 years (9.8
SD), an average BMI of 31.1 kg/m^2^ (6.4
SD), an average WC of 98.5 cm (15.5 SD), and
65.7% (n=308) had a family member with
diabetes (Table 4.1).  Outcome
indices had a significant median decline of between
8.3% to 19.4% over the 6 years of follow-up.

\newpage\newpage

| Measure                      | Baseline visit      |
|:-----------------------------|:--------------------|
| n                            | 477                 |
| Female                       | 334 (70%)           |
| European                     | 56 (12%)            |
| Latino/a                     | 51 (11%)            |
| Other ethnicity              | 32 (7%)             |
| South Asian                  | 308 (65%)           |
| IFG                          | 10 (2%)             |
| IGT                          | 13 (3%)             |
| Family members with diabetes | 346 (73%)           |
| Age (yrs)                    | 50.2 (9.8)          |
| BMI (kg/m^2^)                | 29.3 (15.1)         |
| WC (cm)                      | 31.1 (6.4)          |
| TAG (mmol/L)                 | 5552.0 (1038.8)     |
| Chol (mmol/L)                | 5.2 (0.9)           |
| LDL (mmol/L)                 | 1.4 (0.4)           |
| HDL (mmol/L)                 | 3.1 (0.8)           |
| ALT (U/L)                    | 5307.2 (867.2)      |
| NEFA (nmol/mL)               | 1.5 (0.8)           |
| FA in PL (nmol/mL)           | 383.1 (116.2)       |
| FA in CE (nmol/mL)           | 98.5 (15.5)         |
| HOMA-IR                      | 13.1 (8.5-22)       |
| ISI                          | 7.1 (4.2-10.5)      |
| IGI/IR                       | 13.6 (8.7-21.7)     |
| ISSI-2                       | 727.2 (569.6-918.6) |

Table: Table 4.1: Basic characteristics of PROMISE participants at the baseline
examination (2004-2006).

All values shown are mean (SD), median (IQR), or n (%). FA = fatty
acids. Note: n=9-16 participants had missing data
in the outcome variables.

\newpage\newpage

Figure 4.1 shows the proportions of each fatty acid
in the PL and CE fractions.  The fatty acids that
contributed the largest portion to the total PL fraction were palmitic
acid (16:0) at 30.7%, linoleic acid (18:2n-6) at 21.8%, stearic acid (18:0) at 15.1%, and oleic
acid (18:1n-9) at 10.2%, totaling  77.9%.
In the CE fraction, the fatty acids that contributed the most to the total
fraction were linoleic acid (18:2n-6) at 52.3%, oleic
acid (18:1n-9) at 18.8%, and palmitic acid (16:0) at 12.5%, totaling  83.6%.  See Supplemental Table 4.2 for the raw concentration and mol% values.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/figDistFA-1.pdf}
\caption[Figure 4.1: Distribution of fatty acids in PL and CE fractions.]{Figure 4.1: Distribution of fatty acids in the phospholipid and cholesteryl ester fractions as a percentage of the total pool.  Each dot represents a data point and each box shows the median and interquartile range.}
\end{figure}


Over the 6 years, 42 (9%) participants within this subset
of the cohort developed diabetes while 96 (20%) developed
pre-diabetes (either IFG or IGT).

### Spearman correlations at the baseline visit

Figure 4.2 shows a heatmap of the Spearman
correlation coefficients between the PL and CE fatty acids with
several continuous covariates and the outcomes at the baseline visit.
PL fatty acids (mol%) had moderate (>0.30) correlations with multiple
variables, among which were: 16:0 with ALT (r~s~=0.36); 20:3n-6 with
waist (r~s~=0.37), 1/HOMA-IR (r~s~=-0.39), and ISI (r~s~=-0.39); 18:1n-7 with
1/HOMA-IR (r~s~=-0.3) and ISI (r~s~=0.31); and, 18:0 with ISI (r~s~=0.32) and
1/HOMA-IR (r~s~=-0.33).  For the CE fraction, only one fatty acid (mol%) had a
moderate correlation (>0.30) with the variables: 20:3n-6 with
waist (r~s~=0.39), 1/HOMA-IR (r~s~=-0.39), ISI (r~s~=-0.4), and ISSI-2 (r~s~=-0.3).  All other PL and CE
fatty acids were weakly correlated (<0.30) with the covariates and
outcomes.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/figHeatmap-1.pdf}
\caption[Figure 4.2: Correlation heatmap of PL and CE with basic characteristics.]{Figure 4.2: Spearman correlation coefficient heatmap of the phospholipid and cholesteryl ester fatty acids with covariates and outcomes of the PROMISE participants at the baseline visit (2004-2006).}
\end{figure}



### GEE analysis

#### Main effect associations

The main effect analyses, without interactions by time in the GEE
model, are shown in Figure 4.3.  The top portion of Figure 4.3 shows the results of the GEE modeling for the
individual PL fatty acids (mol%) as well as the total PL fraction
(mol).  For the insulin sensitivity indices, the PL fatty acids that
had positive associations with 1/HOMA-IR were the monounsaturated
fatty acids 18:1n-7 and 18:1n-9, while those with a
negative association were 18:3n-6 and 18:0.  Fatty
acids with a positive association with ISI were 18:1n-9 and
18:1n-7, and those with a negative association were 18:0
and 18:3n-6.  For the beta-cell
function indices, the only PL fatty acid that had a positive association with
IGI/IR and ISSI-2 was 18:1n-7.  Total PL was not associated with
any of the outcomes.  The bottom portion of Figure 4.3 shows the results of
the GEE analysis for the CE fraction.  Compared to the PL fraction,
there were substantially fewer associations seen in the CE fraction.
Only one fatty acid,
18:1n-7, was positively associated with both log(1/HOMA-IR) and log(ISI).
No CE fatty acid was associated with IGI/IR and ISSI-2, nor was total CE associated
with any of the outcomes.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/figForest-pct-1.pdf}
\caption[Figure 4.3: GEE models of PL and CE (mol\%).]{Figure 4.3: Longitudinal associations of individual phospholipid and cholesteryl ester fatty acids (mol\%) with insulin sensitivity and beta-cell function using generalized estimating equations over the 6 years in the PROMISE cohort.  Adjusted for time, sex, ethnicity, baseline age, waist circumference, family history of diabetes, total non-esterified fatty acids, and alanine aminotransferase.  Outcome variables were log-transformed, predictor variables were scaled, and x-axis values were exponentiated to represent percent difference per SD increase in the fatty acid.  P-values were adjusted for the Benjamini-Hochberg False Discovery Rate.}
\end{figure}



Sensitivity analyses using fatty acids as a concentration are shown in
Supplemental Figure 4.2.  Associations were similar in the PL
fraction in magnitude and direction, though there was some attenuation
of results, particularly for the beta-cell function measures.  In
the CE fraction, of the few associations seen in the mol% analysis,
all were attenuated.  Raw beta and CI values from the GEE analyses
for mole percent are presented in Supplemental Table 4.3
and for the mole concentration in Supplemental Table 4.4.
Sensitivity analyses of GEE models adjusting for baseline pre-diabetes were 
not different from the final model (data not shown). Sensitivity analyses
examining dysglycemia showed no association of any with the fatty acids with
conversion to dysglycemia, shown in Supplemental Figure 4.3.

#### Interactions by time

Figure 4.4 shows the results of the interactions by time
in the PL and CE fraction (mol%).  Two PL fatty acids had a
significant interaction by time, namely 20:2n-6 with ISI and ISSI-2,
and 16:0 with ISSI-2.  In addition, three CE fatty acid interactions
were significant: 20:3n-6 had a significant interaction by time for
all outcomes (except for ISI); 18:3n-6 with ISI; and, 16:0 with ISSI-2.  Sensitivity
analysis using fatty acids as a concentration had similar results in
the PL fraction (shown in Supplemental Figure 4.4).

\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/figManhattan-pct-1.pdf}
\caption[Figure 4.4: Interaction testing for PL and CE (mol\%).]{Figure 4.4: P-values from a test for a time interaction by phospholipid and cholesteryl ester fatty acids (mol\%) in the fully adjusted GEE model, including the interaction term.  P-values were adjusted for the Benjamini-Hochberg False Discovery Rate.}
\end{figure}

Significant interactions in the PL
fraction as a mol% were visualized in Figure 4.5, while
those in the CE fraction are in Supplemental Figure 4.5
and those in the PL fraction as a concentration are in Supplemental Figure 4.6.
In the PL fraction, lower
proportions of 20:2n-6 and higher proportions of 16:0 had stronger
negative associations with ISI (20:2n-6 only) and ISSI-2 (in
Figure 4.5A, B, and C) after a longer follow-up.  In the CE
fraction, low proportions of 20:3n-6 had a slightly stronger negative
association in all the outcomes (except ISI) after a longer
follow-up (Supplemental Figure 4.5A, B, and C).  Lower proportions
of CE 18:3n-6 had a stronger negative association with 1/HOMA-IR
and ISI (in Supplemental Figure 4.5D and E) after a longer
follow-up.  As with PL 16:0, high proportions of CE 16:0 had a
slightly stronger negative association with ISSI-2 (in Supplemental Figure 4.5F) after a longer follow-up.  Concentration
interaction plots can be seen in Supplemental Figure 4.5G and B
for CE, and in Supplemental Figure 4.6 for PL.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/figInt-pl-1.pdf}
\caption[Figure 4.5: Interaction plots of PL (mol\%) by time with outcomes.]{Figure 4.5: Calculated least-squares means (with 95\% CI) of the outcomes from fully adjusted GEE models based on phospholipid fatty acids (mol\%), split by median for the groups, that had a significant interaction by time.}
\end{figure}

## Discussion



In a Canadian cohort of predominately female and European-ancestry
individuals who are at-risk for diabetes, we found that higher levels
of specific fatty acids in the PL and CE fraction associated with
differences in insulin sensitivity and beta-cell function over the 6
years.  Specifically, *cis*-vaccenic acid (18:1n-7) and oleic acid
(18:1n-9) had positive associations, while gamma-linolenic acid
(GLA; 18:3 n-6) and stearic acid (18:0) had negative associations
consistently over 6 years.  A few fatty acids had stronger
associations as time progressed, where lower proportions of
eicosadienoic acid (20:2n-6), GLA, and dihomo-gamma-linolenic acid
(DGLA; 20:3n-6) and higher proportions of palmitic acid (16:0) had
stronger harmful associations after longer follow-up times.

In our results, *cis*-vaccenic acid (18:1n-7) had the strongest and
most consistent association with the outcomes, predicting an estimated
5.17 to 11.7% greater insulin
sensitivity and beta-cell function with higher *cis*-vaccenic acid.
One study [@Ma2015a] reported a higher proportion of
*cis*-vaccenic acid in the PL fraction associated with a lower chance
of incident diabetes.  Two recent studies from Finland found that PL
*cis*-vaccenic acid predicted higher insulin sensitivity and lower
glucose AUC [@Mahendran2014a; @Lankinen2015a], as well as higher
beta-cell function [@Lankinen2015a].  Our study confirms these
observations are consistent longitudinally, assessed at three time
points over 6 years, and that the potential benefit of *cis*-vaccenic
acid is consistently strong in each insulin sensitivity and beta-cell
function measure, relative to the other fatty acids. Our findings also
strengthen previous associations of *cis*-vaccenic acid on insulin
sensitivity, which was consistent between the PL and CE fraction.
Although the causal pathways involved are not fully understood as
limited mechanistic studies have been done to date on *cis*-vaccenic
acid, an experimental study [@Burns2012a] in bovine adipocytes
suggests a potential role in inhibiting *de novo* lipogenesis. Another
possibility may be that more palmitoleic acid (16:1n-7) is elongated and
reducing its concentration, as it has been associated with harmful metabolic
profiles [@Zong2012a].

We also observed a consistent negative influence of GLA (18:3n-6) in
the PL fraction on insulin sensitivity, with an estimated 7.2% lower insulin
sensitivity for higher GLA.
Previous findings on GLA have been mixed, with data from the
EPIC-Potsdam cohort showing that higher GLA increased the risk for
incident diabetes [@Kroger2011a], while results from the ARIC cohort
found no association with incident diabetes [@Wang2003a].  GLA in the
CE fraction was not associated with any of the outcomes, however a
time interaction test showed that individuals with lower proportions
of CE GLA had lower whole-body insulin sensitivity after a longer
follow-up time compared to individuals with higher proportions, a
contradiction to the PL findings.  These conflicting results for GLA
as potentially harmful in the PL fraction, but beneficial in the CE
fraction are not entirely clear, although the associations seen in the
CE fraction were weak and could be a statistical anomaly.  The
mechanisms underlying either of the observed associations have not
been elucidated and while there is some research suggesting GLA may
have anti-inflammatory properties [@Kapoor2006a], our findings
would suggest that either less GLA is being used in anti-inflammatory
pathways within our population or GLA itself may be harmful.

The saturated fatty acids are perhaps the most extensively studied fatty acids in
the various lipid fractions.  We found that in the PL fraction higher
proportions of stearic acid (18:0) predicted an estimated 7.4% lower insulin sensitivity.  Our findings
are consistent with previous studies showing a harmful association of
stearic acid on the risk for diabetes
[@Patel2010a; @Forouhi2014a; @Ma2015a; @Wang2003a] and insulin
resistance or hyperglycemia [@Zhou2009b; @Ma2015a; @Ebbesson2010a; @Lankinen2015a],
though some studies have shown mixed results
[@Hodge2007a; @Mahendran2014a; @Kurotani2012a].  This present study is
the first to our knowledge in humans to observe that individuals with
a higher proportion of palmitic acid in either the PL or CE fraction
had markedly lower beta-cell function as time progressed compared
to individuals with a lower proportion, extending previous experimental
research highlighting the role of lipotoxicity in reducing the
secretory capacity of the beta-cells, possibly through greater endoplasmic
reticulum stress or activation of inflammatory immune cells such as monocytes or
macrophages [@Giacca2011a; @Maris2013a; @Eguchi2012a].

There were distinct differences in our findings between the PL and CE
fractions, highlighting the importance of research exploring each
lipid fraction.  Our results were also slightly different between the
percent and the concentration data, particularly in the beta-cell
function measures.  Previous studies have to our knowledge only
utilized data on fatty acids as a percent and is a stated limitation
[@Mahendran2014a; @Forouhi2014a] given the interdependence between
fatty acids in the lipid fraction.  A strength of our study is in the
inclusion of the analysis of both percent and concentration 
data, of which we found some agreement in results within the PL
fraction.  The few fatty acids that had attenuated associations as a
concentration were primarily stearic acid and oleic acid.  This
suggests that when these fatty acids replace other fatty acids
(i.e. increase as a percent) they may exert a beneficial (e.g. oleic
acid) or a harmful (e.g. stearic acid) influence on metabolic
functioning compared to neutral associations when they increase as a
concentration.

There are a number of limitations with our study.  Firstly, fatty
acids were only quantified at the baseline visit.  However, in our
view, this limitation has the advantage of reducing the chance of
reverse causality, given the interconnected pathways involved in fatty
acid metabolism and insulin regulation (e.g. higher insulin resistance
may also disrupt fatty acid metabolism).  Secondly, as this is an
observational study there may be some residual confounding we haven't
considered.  Nonetheless, we have taken extensive precautions to
understand and minimize potential confounding.  Lastly, given the
study design targeting individuals at-risk for diabetes and the large
portion of females and those of European-ancestry within our
population, our results may not be applicable to the general
population.  Our study has many strengths, including the longitudinal
design and statistical techniques, which are better suited to
understanding temporal relationships.  Our cohort also contains highly
detailed and comprehensive variable measurements at each collection
visit, as well as the inclusion of fatty acids as both a concentration
and a percent.

To conclude, in our study of Canadian individuals at-risk for
diabetes, we found that higher saturated and n-6 fatty acids and lower
*cis*-vaccenic acid associated with lower insulin sensitivity and
beta-cell function consistently over time, of which most associations
were seen in the PL fraction.  Certain fatty acids, notably palmitic
acid, had stronger associations after longer follow-up times.

### Acknowledgements

The authors thank Jan Neuman, Paula Van Nostrand, Stella Kink, and Annette
Barnie of the Leadership Sinai Centre for Diabetes, Mount Sinai Hospital,
Toronto, Canada and Sheila Porter and Mauricio Marin of the Centre for Studies
in Family Medicine, University of Western Ontario, London, Canada for their 
expert technical assistance and dedication in their work for PROMISE. 
<!--The
authors had the following responsibility: LWJ conducted research, analyzed data,
and wrote the paper; RR, ZL, BZ, and SBH designed research, conducted research, and
provided essential materials (infrastructure and clinical resources); RR, BZ,
SBH, RPB, and AG provided intellectual feedback on the paper; RPB conducted
research, provided essential reagents and materials; AJH designed research,
assisted with interpretation, and provided intellectual feedback on all versions
of the paper; LWJ and AJH had primary responsibility for final content. All
authors read and approved the final manuscript.  The authors report no potential
conflicts of interest relevant to this study.
-->

<!--Include meds -->

<!--
### Heatmap ##
-->

<!--
### Forest Plot ##
-->


<!--
### Manhattan plot ##
-->


<!--
### Interaction Plots ##
-->


## Supplemental Materials

\begin{figure}
\centering
\includegraphics[width=\textwidth]{../img/flowDiagramSample.pdf}
\caption{Supplemental Figure 4.1: Flow diagram of sample size and loss to follow-up at each examination.}
\end{figure}

\newpage\newpage
\small

| Model   |   QIC |   Delta |
|:--------|------:|--------:|
| M9      | -2398 |       0 |
| M8      | -2391 |     7.1 |
| M7      | -2387 |      11 |
| M5      | -2379 |      19 |
| M6      | -2376 |      22 |
| M2      | -2371 |      28 |
| M4      | -2360 |      38 |
| M3      | -2345 |      53 |
| M1      | -2128 |     270 |
| M0      | -2073 |     326 |

Table: Supplemental Table 4.1: Comparing generalized estimating equation
models adjusted for different covariates using
Quasi-Likelihood Information Criterion.

Given the number of possible combinations of outcome and predictor
variables, only ISSI-2 with total phospholipid (nmol/mL) were used to
compare various GEE models and to select a final model.  Baseline age
was computed and used as including both original age and the time
variable would result in collinearity.  Column names: QIC is the
quasi-likelihood information criteria (smaller values, eg. larger
negative values, indicate a better fit compared to other models),
Delta is the QIC minus the lowest QIC (models with delta <10 are
considered equivalent).  Models were:  
M0: log(ISSI-2) = total phospholipid (nmol/mL) + time;  
M1: M0 + sex + ethnicity + baseline age;  
M2: M1 + waist;  
M3: M1 + BMI;  
M4: M2 + physical activity + alcohol intake + smoking status +
education + occupation + family history of diabetes;  
M5: M4 + total NEFA + ALT;  
M6: M0 + sex + ethnicity + waist + total NEFA;  
M7: M6 + ALT;  
M8: M7 + age;  
M9: M8 + family history of diabetes.

\newpage\newpage
\normalsize

\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/figForest-conc-1.pdf}
\caption[Supplemental Figure 4.2: GEE models of PL and CE (nmol/mL).]{Supplemental Figure 4.2: Longitudinal associations of individual phospholipid and cholesteryl ester fatty acids as a concentration (nmol/mL) with insulin sensitivity and beta-cell function using generalized estimating equations for the 6 years in the PROMISE cohort.  Adjusted for visit, sex, ethnicity, baseline age, waist circumference, family history of diabetes, total free fatty acids, and alanine aminotransferase.  Outcome variables were log-transformed and x-axis values were exponentiated to represent percent difference per SD increase in the fatty acids.  P-values were adjusted for the False Discovery Rate.}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/figLogReg-1.pdf}
\caption[Supplemental Figure 4.3: Logistic model for PL and CE (mol\%) with dysglycemia.]{Supplemental Figure 4.3: Odds ratios for the association of conversion to dysglycemia over the 6 years and fatty acids within the phospholipid and cholesteryl ester serum fractions (mol\%). Models were adjusted using the covariates from the primary GEE model, not including time. P-values were adjusted for the Benjamini-Hochberg False Discovery Rate.}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/figManhattan-conc-1.pdf}
\caption[Supplemental Figure 4.4: Interaction testing for PL and CE (nmol/mL).]{Supplemental Figure 4.4: P-values from a test for a time interaction by phospholipid and cholesteryl ester fatty acids (as a concentration, nmol/mL) in the fully adjusted GEE model, including the interaction term.  P-values were adjusted for the False Discovery Rate.}
\end{figure}

\newpage\newpage
\newpage\newpage

\footnotesize
\newpage\newpage

| Fatty acid   |  CE (nmol/mL)   |  CE (mol%)  |  PL (mol%)  |  PL (nmol/mL)  |
|:-------------|:---------------:|:-----------:|:-----------:|:--------------:|
| Total (mole) | 5552.0 (1038.8) |             |             | 5307.2 (867.2) |
| 24:1n-9      |    1.3 (1.9)    |  0.0 (0.0)  |  0.5 (0.3)  |  24.7 (16.9)   |
| 22:6n-3      |   21.9 (10.2)   |  0.4 (0.2)  |  2.8 (1.0)  |  146.9 (58.5)  |
| 22:5n-3      |    1.6 (1.2)    |  0.0 (0.0)  |  0.7 (0.3)  |  39.2 (15.1)   |
| 22:4n-6      |    0.8 (0.9)    |  0.0 (0.0)  |  0.3 (0.1)  |   14.3 (4.5)   |
| 22:1n-9      |    3.7 (4.0)    |  0.1 (0.1)  |  0.0 (0.0)  |   2.5 (0.9)    |
| 22:0         |    1.0 (1.4)    |  0.0 (0.0)  |  0.0 (0.0)  |   2.1 (1.2)    |
| 20:5n-3      |   50.5 (31.8)   |  0.9 (0.5)  |  1.1 (0.7)  |  61.0 (38.8)   |
| 20:4n-6      |  342.6 (97.5)   |  6.2 (1.5)  |  9.6 (1.9)  | 510.2 (124.5)  |
| 20:3n-6      |   39.2 (14.9)   |  0.7 (0.2)  |  3.1 (0.8)  |  164.3 (53.6)  |
| 20:2n-6      |    5.2 (2.3)    |  0.1 (0.0)  |  0.5 (0.1)  |   24.4 (6.6)   |
| 20:1n-9      |    3.4 (7.4)    |  0.1 (0.1)  |  0.2 (0.1)  |   8.6 (4.0)    |
| 20:0         |    1.8 (1.3)    |  0.0 (0.0)  |  0.3 (0.1)  |   17.6 (5.7)   |
| 18:3n-6      |   56.2 (25.1)   |  1.0 (0.4)  |  0.1 (0.1)  |   7.1 (3.9)    |
| 18:3n-3      |   37.1 (14.3)   |  0.7 (0.2)  |  0.3 (0.1)  |   13.7 (6.1)   |
| 18:2n-6      | 2905.5 (603.4)  | 52.3 (4.5)  | 21.8 (2.8)  | 1157.4 (230.5) |
| 18:1n-9      | 1043.7 (223.3)  | 18.8 (2.1)  | 10.2 (1.3)  | 545.2 (123.0)  |
| 18:1n-7      |   65.9 (16.1)   |  1.2 (0.3)  |  1.5 (0.3)  |  80.5 (15.6)   |
| 18:0         |   85.0 (31.0)   |  1.6 (0.6)  | 15.1 (1.3)  | 801.5 (142.6)  |
| 16:1n-7      |  160.5 (84.3)   |  2.8 (1.3)  |  0.6 (0.2)  |  32.9 (16.2)   |
| 16:0         |  691.5 (145.5)  | 12.5 (1.3)  | 30.7 (2.8)  | 1633.1 (322.6) |
| 14:1n-7      |    1.4 (1.1)    |  0.0 (0.0)  |  0.0 (0.0)  |   0.7 (0.8)    |
| 14:0         |   32.1 (17.1)   |  0.6 (0.3)  |  0.4 (0.1)  |   19.4 (8.5)   |

Table: Supplemental Table 4.2: Mean concentration and
percent of each fatty acid with SD
in the phospholipid (PL) and cholesteryl ester (CE) fraction in PROMISE
participants without diabetes at the baseline visit only (2004-2006).

\newpage\newpage

| Species         |      log(1/HOMA-IR) |            log(ISI) |        log(IGI/IR) |       log(ISSI-2) |
|:----------------|--------------------:|--------------------:|-------------------:|------------------:|
| PL 22:6n-3      |     1.7 (-2.7, 6.4) |     1.1 (-3.3, 5.6) |    6.1 (0.1, 12.6) |   1.6 (-1.4, 4.7) |
| PL 22:5n-3      |     2.7 (-1.4, 7.0) |     2.2 (-2.1, 6.6) |    7.4 (1.1, 14.1) |   2.6 (-0.4, 5.7) |
| PL 20:5n-3      |     1.6 (-2.8, 6.2) |     0.4 (-4.0, 5.0) |    4.0 (-1.5, 9.8) |   0.2 (-2.6, 3.1) |
| PL 18:3n-3      |     1.0 (-2.5, 4.7) |     0.5 (-3.3, 4.5) |    0.8 (-4.1, 6.0) |   0.0 (-2.6, 2.6) |
| PL 22:4n-6      |     2.3 (-2.2, 7.0) |     1.7 (-2.7, 6.3) |    2.4 (-3.9, 9.2) |   2.2 (-1.0, 5.4) |
| PL 20:4n-6      |     1.6 (-2.8, 6.2) |     0.6 (-3.9, 5.4) |    0.3 (-5.4, 6.3) |   0.1 (-2.8, 3.1) |
| PL 20:3n-6      |  -6.6 (-11.6, -1.2) |  -7.1 (-11.9, -1.9) |   -1.1 (-7.8, 6.0) |   0.1 (-3.2, 3.6) |
| PL 20:2n-6      |     4.4 (-0.1, 9.1) |     4.1 (-0.7, 9.1) |   4.1 (-2.0, 10.6) |    3.4 (0.5, 6.3) |
| PL 18:3n-6      | -7.2 (-10.9, -3.3)* | -7.2 (-11.3, -2.9)* | -7.1 (-12.1, -1.8) | -3.5 (-6.0, -0.9) |
| PL 18:2n-6      |     3.5 (-1.0, 8.1) |      4.7 (0.2, 9.5) |   4.1 (-1.8, 10.3) |   1.5 (-1.3, 4.5) |
| PL 18:1n-7      |   11.7 (7.1, 16.5)* |   11.5 (7.0, 16.3)* |  10.8 (4.4, 17.7)* |   5.2 (2.3, 8.1)* |
| PL 16:1n-7      |   -4.9 (-8.6, -0.9) |   -5.1 (-9.1, -0.9) | -7.9 (-13.6, -1.9) | -3.0 (-5.7, -0.3) |
| PL 14:1n-7      |    -1.5 (-5.2, 2.2) |    -0.7 (-4.4, 3.2) |   -1.0 (-6.0, 4.3) |  -1.1 (-3.9, 1.8) |
| PL 24:1n-9      |     0.9 (-3.1, 5.1) |     1.5 (-2.8, 5.9) |   -0.6 (-6.2, 5.5) |  -0.1 (-3.1, 2.9) |
| PL 22:1n-9      |    -0.2 (-4.4, 4.3) |     0.5 (-4.0, 5.2) |    1.3 (-5.1, 8.0) |   0.4 (-2.6, 3.4) |
| PL 20:1n-9      |      4.2 (0.4, 8.2) |     4.1 (-0.1, 8.5) |    8.0 (2.1, 14.4) |    3.8 (0.7, 6.9) |
| PL 18:1n-9      |    7.4 (2.9, 12.0)* |    7.0 (2.4, 11.9)* |    1.8 (-4.3, 8.3) |   2.5 (-0.7, 5.7) |
| PL 22:0         |     2.8 (-1.1, 6.9) |      3.8 (0.3, 7.5) |   -2.2 (-8.1, 4.0) |  -1.7 (-4.5, 1.2) |
| PL 20:0         |    -0.4 (-4.2, 3.5) |    -0.8 (-4.6, 3.2) |   -1.9 (-7.3, 3.8) |  -1.6 (-4.5, 1.4) |
| PL 18:0         | -7.3 (-11.0, -3.3)* | -7.5 (-11.5, -3.4)* |   -1.6 (-7.0, 4.2) |  -0.9 (-3.5, 1.8) |
| PL 16:0         |   -4.9 (-8.7, -0.8) |   -4.4 (-8.5, -0.1) | -8.1 (-13.9, -1.9) | -3.7 (-6.5, -0.8) |
| PL 14:0         |   -4.8 (-8.7, -0.6) |   -5.0 (-9.3, -0.5) |   -4.0 (-9.4, 1.7) |  -1.5 (-4.3, 1.3) |
| PL Total (mole) |    -2.5 (-6.4, 1.6) |    -3.5 (-7.5, 0.7) |   -4.4 (-9.8, 1.3) |  -2.2 (-5.0, 0.6) |
| CE 22:6n-3      |    -0.5 (-4.8, 3.9) |    -0.5 (-4.9, 4.1) |    1.9 (-3.5, 7.6) |   0.0 (-2.8, 2.9) |
| CE 22:5n-3      |     2.7 (-1.2, 6.8) |      4.6 (0.4, 8.9) |    0.8 (-4.3, 6.2) |   1.0 (-1.7, 3.8) |
| CE 20:5n-3      |     1.3 (-3.0, 5.9) |     0.6 (-3.6, 5.0) |    2.3 (-3.2, 8.2) |   0.1 (-3.0, 3.2) |
| CE 18:3n-3      |     0.5 (-2.6, 3.7) |    -1.1 (-4.3, 2.1) |    1.6 (-3.0, 6.5) |   0.1 (-2.2, 2.6) |
| CE 22:4n-6      |    -2.4 (-6.9, 2.2) |    -2.2 (-7.1, 2.9) |    0.6 (-5.8, 7.4) |  -0.1 (-2.9, 2.8) |
| CE 20:4n-6      |    -2.1 (-7.1, 3.2) |    -2.5 (-7.6, 2.9) |   -3.4 (-8.9, 2.5) |  -1.6 (-4.3, 1.2) |
| CE 20:3n-6      |    1.0 (-9.7, 12.9) |   0.4 (-10.1, 12.2) |    0.3 (-5.5, 6.4) |   0.9 (-1.9, 3.8) |
| CE 20:2n-6      |     2.1 (-2.8, 7.3) |     1.7 (-3.5, 7.1) |    1.0 (-4.3, 6.7) |   1.1 (-1.8, 4.2) |
| CE 18:3n-6      |    -2.2 (-6.0, 1.8) |    -2.5 (-6.5, 1.8) |   -4.6 (-9.5, 0.5) |  -1.2 (-3.7, 1.3) |
| CE 18:2n-6      |     1.3 (-3.1, 5.9) |     2.1 (-2.2, 6.6) |    6.7 (0.5, 13.4) |   2.4 (-0.7, 5.6) |
| CE 18:1n-7      |    7.1 (2.8, 11.6)* |    7.5 (3.2, 12.0)* |   5.1 (-0.6, 11.1) |   2.9 (-0.1, 5.9) |
| CE 16:1n-7      |   -5.0 (-8.9, -0.9) |   -4.9 (-9.0, -0.6) | -8.2 (-13.8, -2.3) | -3.3 (-6.0, -0.6) |
| CE 14:1n-7      |   -4.4 (-7.6, -1.1) |   -3.6 (-7.0, -0.2) |   -4.5 (-9.8, 1.2) |  -1.5 (-3.8, 0.8) |
| CE 24:1n-9      |     2.8 (-2.0, 7.8) |     1.9 (-2.9, 7.0) |   6.5 (-0.2, 13.5) |   3.0 (-0.1, 6.3) |
| CE 22:1n-9      |     0.8 (-1.7, 3.4) |     0.4 (-2.2, 3.1) |    3.2 (-1.5, 8.0) |   0.7 (-1.1, 2.4) |
| CE 20:1n-9      |     2.1 (-3.1, 7.5) |     3.0 (-2.3, 8.5) |    1.3 (-4.2, 7.1) |   1.1 (-2.4, 4.8) |
| CE 18:1n-9      |     2.5 (-1.8, 6.9) |     1.4 (-2.9, 5.8) |   -3.8 (-9.5, 2.3) |  -0.6 (-3.5, 2.5) |
| CE 22:0         |     0.2 (-3.5, 4.1) |     0.8 (-2.9, 4.7) |    2.2 (-3.6, 8.5) |   0.6 (-1.8, 3.0) |
| CE 20:0         |    -1.8 (-7.6, 4.4) |     0.0 (-5.3, 5.6) |    1.7 (-3.3, 6.9) |   1.2 (-1.7, 4.2) |
| CE 18:0         |    -1.6 (-6.6, 3.8) |    -1.6 (-7.0, 4.1) |   -0.2 (-5.4, 5.3) |   0.0 (-3.0, 3.1) |
| CE 16:0         |    -2.2 (-6.2, 1.9) |    -2.3 (-6.4, 1.9) |  -4.6 (-10.2, 1.4) |  -2.7 (-5.6, 0.3) |
| CE 14:0         |    -1.4 (-5.3, 2.5) |    -1.1 (-4.9, 2.9) |   -2.2 (-7.7, 3.8) |  -1.0 (-3.6, 1.7) |
| CE Total (mole) |    -3.0 (-6.7, 0.9) |    -4.0 (-7.7, 0.0) |   -3.4 (-8.6, 2.1) |  -2.1 (-4.6, 0.5) |

Table: Supplemental Table 4.3: Exponentiated GEE beta coefficients and 95% confidence intervals for the
associations of phospholipid and cholesteryl ester fatty acids (mol%) with
the outcome variables. Calculated 95% CI were *not* adjusted for multiple
testing using the False Discovery Rate (FDR).  Astericks indicates p<0.05
adjusted by the FDR.

\newpage\newpage

| Species    |      log(1/HOMA-IR) |            log(ISI) |        log(IGI/IR) |       log(ISSI-2) |
|:-----------|--------------------:|--------------------:|-------------------:|------------------:|
| PL 22:6n-3 |    -0.2 (-4.3, 4.1) |    -1.1 (-5.3, 3.3) |   3.7 (-2.6, 10.4) |   0.2 (-2.9, 3.5) |
| PL 22:5n-3 |     1.5 (-2.3, 5.4) |     0.7 (-3.3, 4.8) |   4.4 (-1.8, 10.9) |   1.2 (-1.8, 4.2) |
| PL 20:5n-3 |     0.6 (-3.9, 5.3) |    -0.7 (-5.2, 4.0) |    2.0 (-3.8, 8.1) |  -0.8 (-3.7, 2.2) |
| PL 18:3n-3 |     0.1 (-3.5, 3.8) |    -0.8 (-4.7, 3.3) |   -0.9 (-5.6, 4.0) |  -0.8 (-3.2, 1.7) |
| PL 22:4n-6 |     0.4 (-3.9, 4.7) |    -0.8 (-4.9, 3.5) |    0.0 (-5.8, 6.1) |   0.8 (-2.2, 3.8) |
| PL 20:4n-6 |    -0.5 (-4.5, 3.7) |    -2.1 (-6.1, 2.2) |   -1.8 (-7.5, 4.2) |  -1.1 (-4.1, 2.0) |
| PL 20:3n-6 |  -6.4 (-10.6, -2.1) | -7.2 (-11.4, -2.9)* |   -3.5 (-9.4, 2.8) |  -1.1 (-4.3, 2.1) |
| PL 20:2n-6 |     1.6 (-2.6, 5.9) |     0.8 (-3.8, 5.5) |   -0.2 (-5.7, 5.6) |   0.8 (-2.0, 3.7) |
| PL 18:3n-6 | -6.5 (-10.3, -2.6)* | -6.9 (-10.9, -2.7)* | -7.3 (-12.2, -2.0) | -3.5 (-6.1, -0.9) |
| PL 18:2n-6 |     0.1 (-4.3, 4.7) |     0.0 (-4.6, 4.7) |   -1.4 (-6.9, 4.4) |  -1.0 (-3.7, 1.8) |
| PL 18:1n-7 |    8.0 (3.5, 12.7)* |     6.9 (2.1, 12.0) |   6.0 (-0.3, 12.7) |   2.7 (-0.3, 5.8) |
| PL 16:1n-7 |   -4.3 (-8.1, -0.4) |   -4.8 (-8.7, -0.7) | -7.7 (-13.4, -1.7) | -3.0 (-5.7, -0.3) |
| PL 14:1n-7 |    -2.1 (-5.8, 1.8) |    -1.3 (-5.2, 2.8) |   -1.6 (-6.9, 4.0) |  -1.4 (-4.2, 1.6) |
| PL 24:1n-9 |     0.5 (-3.4, 4.6) |     0.8 (-3.4, 5.3) |   -1.6 (-7.1, 4.1) |  -0.6 (-3.5, 2.3) |
| PL 22:1n-9 |    -1.3 (-5.4, 3.0) |    -0.9 (-5.3, 3.7) |   -0.9 (-7.1, 5.8) |  -0.6 (-3.4, 2.3) |
| PL 20:1n-9 |     2.9 (-1.1, 7.0) |     2.4 (-1.8, 6.8) |   5.5 (-1.9, 13.5) |   2.3 (-1.6, 6.5) |
| PL 18:1n-9 |     2.2 (-1.9, 6.6) |     1.5 (-2.9, 6.1) |   -3.0 (-8.5, 2.9) |  -0.6 (-3.5, 2.4) |
| PL 22:0    |     2.3 (-1.7, 6.4) |     2.8 (-1.0, 6.8) |  -4.2 (-10.2, 2.2) |  -2.6 (-5.5, 0.4) |
| PL 20:0    |    -1.5 (-5.3, 2.5) |    -2.1 (-6.1, 2.0) |   -4.2 (-9.2, 1.1) |  -2.6 (-5.2, 0.1) |
| PL 18:0    |   -5.7 (-9.4, -1.8) | -6.7 (-10.5, -2.7)* |   -4.7 (-9.7, 0.6) |  -2.5 (-5.1, 0.2) |
| PL 16:0    |   -4.1 (-7.9, -0.1) |   -4.7 (-8.6, -0.6) | -6.9 (-12.4, -1.0) | -3.3 (-6.0, -0.4) |
| PL 14:0    |   -4.4 (-8.2, -0.4) |   -5.3 (-9.2, -1.2) |   -4.6 (-9.8, 1.0) |  -1.9 (-4.5, 0.8) |
| CE 22:6n-3 |    -1.5 (-5.5, 2.7) |    -1.8 (-5.9, 2.6) |    1.3 (-3.8, 6.8) |  -0.5 (-3.2, 2.2) |
| CE 22:5n-3 |     1.7 (-2.3, 5.9) |     3.3 (-1.1, 7.9) |    0.8 (-4.4, 6.2) |   0.7 (-1.9, 3.4) |
| CE 20:5n-3 |     1.0 (-3.3, 5.5) |     0.3 (-3.9, 4.7) |    1.7 (-4.3, 7.9) |  -0.4 (-3.8, 3.2) |
| CE 18:3n-3 |    -1.2 (-4.5, 2.3) |    -3.1 (-6.5, 0.5) |   -0.3 (-5.0, 4.6) |  -0.8 (-3.1, 1.6) |
| CE 22:4n-6 |    -3.4 (-7.7, 1.2) |    -3.5 (-8.2, 1.5) |    0.2 (-5.7, 6.4) |  -0.4 (-3.1, 2.3) |
| CE 20:4n-6 |    -3.3 (-7.7, 1.4) |    -4.1 (-8.7, 0.6) |   -3.7 (-8.9, 1.8) |  -2.3 (-4.9, 0.4) |
| CE 20:3n-6 |  -1.4 (-12.0, 10.5) |   -2.2 (-12.7, 9.6) |   -1.9 (-8.6, 5.3) |  -0.4 (-4.0, 3.3) |
| CE 20:2n-6 |     0.2 (-4.6, 5.2) |    -0.7 (-5.6, 4.4) |   -0.2 (-5.5, 5.4) |   0.2 (-2.7, 3.1) |
| CE 18:3n-6 |    -3.3 (-7.0, 0.5) |    -3.7 (-7.5, 0.2) | -5.5 (-10.5, -0.1) |  -2.0 (-4.6, 0.7) |
| CE 18:2n-6 |    -2.1 (-5.9, 1.8) |    -2.8 (-6.7, 1.2) |   -0.7 (-6.4, 5.2) |  -1.0 (-3.7, 1.8) |
| CE 18:1n-7 |     3.7 (-0.3, 7.8) |     3.4 (-0.7, 7.6) |    2.3 (-2.6, 7.5) |   1.1 (-1.6, 3.8) |
| CE 16:1n-7 |   -5.5 (-9.5, -1.3) |   -5.5 (-9.6, -1.2) | -8.5 (-14.0, -2.6) | -3.6 (-6.2, -0.9) |
| CE 14:1n-7 |   -4.5 (-7.7, -1.1) |   -4.0 (-7.3, -0.5) |  -4.9 (-10.3, 0.8) |  -1.7 (-4.0, 0.7) |
| CE 24:1n-9 |     1.9 (-3.3, 7.4) |     0.9 (-4.2, 6.2) |   6.2 (-1.0, 13.8) |   2.8 (-0.7, 6.4) |
| CE 22:1n-9 |     0.4 (-2.0, 3.0) |    -0.2 (-3.0, 2.7) |    2.1 (-1.4, 5.6) |   0.2 (-1.1, 1.5) |
| CE 20:1n-9 |     1.6 (-3.4, 6.9) |     2.4 (-2.8, 7.9) |    1.0 (-4.5, 6.8) |   0.9 (-2.6, 4.5) |
| CE 18:1n-9 |    -1.6 (-5.3, 2.2) |    -2.8 (-6.5, 1.1) |  -5.3 (-10.5, 0.3) |  -2.2 (-4.9, 0.5) |
| CE 22:0    |    -0.3 (-3.5, 3.0) |     0.1 (-3.0, 3.3) |    2.2 (-2.7, 7.5) |   0.5 (-1.5, 2.6) |
| CE 20:0    |    -2.6 (-8.1, 3.3) |    -1.7 (-6.8, 3.8) |    1.0 (-3.6, 5.8) |   0.6 (-2.0, 3.4) |
| CE 18:0    |    -3.5 (-8.3, 1.5) |    -4.0 (-9.1, 1.3) |   -1.8 (-6.4, 3.0) |  -1.0 (-3.7, 1.6) |
| CE 16:0    |    -3.7 (-7.5, 0.3) |   -4.5 (-8.3, -0.6) |  -5.0 (-10.4, 0.7) | -2.9 (-5.5, -0.3) |
| CE 14:0    |    -2.2 (-5.9, 1.6) |    -2.2 (-5.8, 1.5) |   -3.2 (-8.7, 2.6) |  -1.4 (-3.9, 1.2) |

Table: Supplemental Table 4.4: Exponentiated GEE beta coefficients and 95% confidence intervals for the
associations of phospholipid and cholesteryl ester fatty acids (nmol/mL) with
the outcome variables.  Calculated 95% CI were *not* adjusted for multiple
testing using the False Discovery Rate (FDR).  Astericks indicates p<0.05
adjusted by the FDR.

\newpage\newpage
\normalsize

\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/figInt-ce-1.pdf}
\caption[Supplemental Figure 4.5: Interaction plots of CE (mol\% and nmol/mL) by time with outcomes.]{Supplemental Figure 4.5: Calculated least-squares means (with 95\% CI) of the outcomes from fully adjusted GEE models based on cholesteryl ester fatty acids (mol\% for A-F and nmol/mL for G-H), split by tertile for the groups, that had a significant interaction by time.}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/figInt-pl-conc-1.pdf}
\caption[Supplemental Figure 4.6: Interaction plot of PL (nmol/mL) by time with outcomes.]{Supplemental Figure 4.6: Calculated least-squares means (with 95\% CI) of the outcomes from fully adjusted GEE models based on phospholipid fatty acids (nmol/mL), split by median for the groups, that had a significant interaction by time.}
\end{figure}

\normalsize
