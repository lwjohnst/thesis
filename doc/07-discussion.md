
# Discussion

## Overall summary

The research contained within this dissertation addressed the overall objective of
the thesis project, which was to
determine the longitudinal associations of fatty acids in four lipid fractions
on insulin sensitivity and beta-cell function. In Chapter 4, both the
phospholipid and cholesteryl ester fatty acid fractions were analyzed for their
association with the outcomes. I found that within the phospholipid fraction, a
lower proportion (mol%) of *cis*-vaccenic acid (18:1n-7) while higher 
gamma-linolenic acid (18:3n-6) and stearic acid (18:0) associated with lower 
insulin sensitivity. There were a small number of fatty acids that showed
longitudinal interactions with time. Specifically, lower proportions of
eicosadienoic acid (20:2n-6) and higher proportions of palmitic acid (16:0)
associated with declines in beta-cell function over the 6 years of follow-up.
There are few data available regarding the biological roles of *cis*-vaccenic
acid or of eicosadienoic acid on glucose metabolism. However, there is strong
evidence supporting the harmful role of the long chain SFA palmitic and stearic
acid on risk of diabetes and associations with the underlying disorders 
[@Forouhi2014a;@Hodge2007a; @Kato2008; @Dixon2004]. Few associations were seen
in the cholesteryl ester fraction (either when modeled as a mol% or as a
concentration) and when modeling the phospholipid fatty acids as a
concentration.

In Chapter 5, I found that higher total NEFA associated with lower beta-cell 
function, but that no individual fatty acid strongly associated with the 
outcomes. While I did see negative associations with palmitic acid (16:0), oleic
acid (18:1n-9), and linoleic acid (18:2n-6) in the concentration (nmol/mL)
modeling, this was likely due to the positive correlation with the total NEFA
concentration, as there were no associations for individual fatty acids when
modeled as mol%. This observation was confirmed using a multivariate clustering
technique. To my knowledge, no other study has examined the specific NEFA
composition on the pathophysiology of T2DM. However, there is a broad literature
on the role of total NEFA on glucose metabolism which supports my findings 
[@Rebelos2015a;@Arner2015a;@Cnop2008a;@Carpentier2003a;@Pankow2004a].

In Chapter 6, several TAG fatty acids, as well as specific clusters of fatty
acids, were strongly associated with insulin sensitivity and moderately
associated with beta-cell function. In particular, higher myristic acid (14:0),
7-tetradecenoic acid (14:1n-7), palmitic acid (16:0), and palmitoleic acid
(16:1n-7) all strongly associated with lower insulin sensitivity. All but two
fatty acids had null associations with beta-cell function. Only palmitic acid
and *cis*-vaccenic acid (18:1n-7) were associated negatively and positively, 
respectively, with beta-cell function. Four fatty acids (14:0, 14:1n-7, 16:0, 
16:1n-7) tightly clustered together and higher proportions of the individual
fatty acids that make up this cluster as well as a higher cluster score
(extracted from PLS) strongly associated
with lower insulin sensitivity. These four fatty acids have been shown to be
involved in the de novo lipogenesis (DNL) of refined and simple carbohydrates 
[@Hodson2008a;@Kawano2013a], with several studies supporting the link between 
higher DNL and a greater risk for T2DM 
[@Ma2015a;@Zong2013a;@Lankinen2015a;@Kroger2011a].

With regard to the specific a priori hypotheses, for hypothesis 1 regarding SFA,
while I did see that in nearly all the fractions palmitic acid strongly
associated with metabolic dysfunction, stearic acid did not have nearly as
strong an association. Previous studies have reported similar findings
[@Forouhi2014a;@Hodge2007a;@Ma2015a;@Wang2003a]. There is evidence
[@Forouhi2014a] that the risk attributable to SFA is primarily
due to palmitic acid, with risk decreasing as SFA carbon length
increases or decreases. I only found that palmitic acid in the phospholipid
fraction associated with declines in beta-cell function over the 6 years.

Regarding hypothesis 2, while there is strong evidence that omega-3 fatty acids such as eicosapentaenoic
acid and docosahexaenoic acid are heavily involved in inflammation pathways
[@Calder2009], which may in turn influence the risk for T2DM, 
I saw no strong evidence in the results for associations of these
fatty acids with the outcomes. I also found that associations of oleic acid with the outcomes were not consistent across the different fractions. 

For hypothesis 3 regarding the varying proportions of SFA compared to other
fatty acid classes, there was no strong evidence showing that a greater
proportion of specific fatty acid classes (SFA, MUFA, PUFA) associated with the
outcomes. I did find that specific fatty acids (e.g. *cis*-vaccenic acid) or
specific clusters (the TAGFA DNL cluster) associated with the outcomes,
suggesting that the complex interrelationship between lipid and glucose
metabolism is not captured by a simple grouping by fatty acid class.

Lastly, hypothesis 4 proposed stronger associations with the TAG and NEFA
fractions. I did find that the TAG fraction had the strongest and most varied
associations with the outcomes compared to the other fractions, although I did
not see the same magnitude of association with the NEFA fraction aside from that
of the total fraction. Aside from specific phospholipid fatty acids, no other
fraction had longitudinal interactions with the outcomes over time.

Taken together, the findings from my PhD research highlight
that the role of fatty acids in influencing diabetes pathogenesis depends on the fatty acid
type and on the specific lipid fraction. The fatty acids within the TAG
fraction had some of the strongest associations seen with insulin sensitivity
compared to all the other fractions. *Cis*-vaccenic acid also showed strong
associations with the outcomes and was one of the few individual fatty acids
with positive associations in all but the NEFA fraction. And in
only the TAG fraction did fatty acids cluster together (i.e. the DNL fatty
acids) and strongly associate with insulin sensitivity. This comprehensive
analysis of a broad spectrum of fatty acids and fractions within serum provides
an integrated overview of the importance of the fractions and the fatty acid
type on metabolism.

## Limitations

There are several important limitations to my PhD project that must be
considered when interpreting the results. Because PROMISE is a longitudinal
observational cohort, there may be some residual confounding due to lack of
adjustment for confounders or suboptimal measurement of covariates included in models. It is also possible that
confounding pathways exist which have not yet been described. However, as best
as possible, I took extensive precautions in controlling for confounding and
reducing possible sources of bias, by using empirically based methods such as
relying on the literature, mapping the underlying biology analytically (directed
acyclic graphs), and using analytical techniques (information criterion model
selection). 

The fatty acids from the four lipid fractions were only measured at the baseline
visit while the metabolic outcome variables were measured at each time point.
This limits how the results can be interpreted and what types of research
questions can be answered from the data. For instance, fatty acid and glucose
metabolism are tightly integrated and over time can differentially influence one
another, and with the available PROMISE data it is not possible to investigate
this phenomenon. However, analysis and interpretation of the data from this
thesis is simpler as the results can be interpreted as the association of a 
given fatty acid baseline value with 6 year trends in glucose metabolism, thus 
reducing the potential for reverse causality.

A single measure also does not reflect fluctuations over time, which fatty
acids are known to have. Most lipid fractions reflect the composition of the diet,
and changes in diet can produce changes in the fatty acid composition of the
lipid fractions [@Louheranta2002a;@Hodson2008a;@Lopes1991a;@Arab2003a]. Certain
fractions are more variable over time than others, specifically, available
literature suggests that the TAG fraction is the most variable
while the cholesteryl ester is the least [@Jacobs1982a;@Mjoes1979a].
This is consistent with current understanding of lipid biology, as the TAG
fraction is the first to be
modified following dietary changes while the cholesteryl ester composition is
enzyme dependent. However, even with these inherent natural fluctuations in
fatty acid composition and even after adjusting for the false discovery rate,
strong associations were still seen in the most variable of fractions (TAG),
suggesting that these significant associations are likely biologically meaningful.

Fatty acids were only measured during fasting. There is some evidence 
[@Sondergaard2012a] suggesting that the post-prandial response may provide
additional information independent of fasting measures and would be a valuable
and interesting avenue of inquiry for future studies.

Lastly, the PROMISE cohort was designed to study individuals at risk for
T2DM to more closely examine factors that influence the transition into T2DM.
The majority of participants were also female and were mainly of European
ancestry. Therefore, the results from my PhD research as such may not be
generalizable to other populations.

## Strengths

The longitudinal design of the PROMISE cohort 
allows for the examination of temporal associations between exposures and outcomes
and the comprehensive fatty acid 
composition data allow for a broader, overarching view of their associations 
with glucose metabolism. The cohort also contained highly detailed insulin 
sensitivity and beta-cell function indices calculated from insulin and glucose
at multiple time points, in addition
to other questionnaire, demographic, metabolic, and anthropometric variables.
Given the complex data available in PROMISE (longitudinal and multivariate
predictors), a challenge was to analyze the data such that this complexity and
dimensionality was optimized. Therefore, I used advanced statistical techniques in
my PhD project that were specifically suited to handle the longitudinal data
(generalized estimating equations), to handle the multivariate nature of the
fatty acid composition with the outcomes (partial least squares), and to best
build and select statistical models (directed acyclic graphs, information
criterion methods).

The characterization and description of multiple lipid fractions within a single
research project is an important strength. Only one group 
[@Lankinen2015a] to my knowledge has published work that included more than two 
fractions in their analysis. As my PhD research 
demonstrates, depending on the specific lipid fraction and fatty acid type there
are differential associations with glucose metabolism that would not be 
elucidated had all lipid fractions not been characterized. Likewise,
given the inherent interdependence between fatty acids when analyzed
them as a mol% (by definition, if one fatty acid increases, one or more other
fatty acids must decrease in value), this PhD project a priori included both
mol% and concentration in the analyses to better understand the 
impact of the fatty acid composition on glucose metabolism. Including
both concentration and mol% allowed for more comprehensive interpretations of the
results; for instance, I found that higher total NEFA, but not any
individual fatty acid within the fraction, associated with lower beta-cell
function.

Lastly, the graphical visualizations of the results and the public
availability of the code used in the analyses are important strengths from the
perspective of data presentation and analytic transparency.
The specific visualizations I developed and used allow the complex
quantity of results to be relatively easily and quickly understood and interpreted.
Similarly, the code is publicly available for other researchers to use and to
confirm the
results. Increasingly, there are calls for the code used in analyses to be
published along with the manuscript to confirm the findings in a reproducible
manner and is a component of open science 
[@Watson2015a;@Groves2012a;@Marszalek2016a;@Sandve2013a]. This provides a level 
of rigor, transparency, and strength to the scientific findings.

## Implications

While my PhD research is largely discovery-based science and may not lead to any
immediate clinical or policy applications, there are several important implications of
my work. Firstly, this is the first published cohort with
data on a broad spectrum of fatty acids and lipid fractions in serum. Previous
studies in general only examined one or two individual lipid fractions in a
given cohort, often only analyzing specific fatty acids (e.g. only saturated
fatty acids) within the fraction. This limitation prevents broader patterns and 
interrelationships from 
being identified (e.g. my findings on the DNL fatty acids within the TAG fraction). 
My research has extended the
literature in this area by providing these comprehensive data.

In addition to the comprehensive data and results being presented, the implications
of my research also provide greater understanding on the underlying biological 
processes. For instance, while total NEFA
concentration has been well-documented to associate with risk for diabetes and
other metabolic diseases, no data are available on the role of the specific
composition of NEFA. My work showing no association of any individual NEFA on
the pathogenesis of diabetes suggests the fatty acid composition of this
specific lipid fraction may not be an important
factor in the development of diabetes. Measuring the composition of NEFA is a
complicated and intensive process, while measuring total NEFA is relatively
quick and inexpensive. My results, if confirmed by future studies, would encourage
researchers to continue using the enzymatic assay for total NEFA, which is much more cost
and time efficient compared to the approach used in this thesis.
Other implications include the identification of the DNL fatty acid cluster in 
the TAG fraction and the strong positive associations seen with *cis*-vaccenic acid. 
Research extending these findings could yield potential biomarkers for 
identifying at-risk individuals or for therapeutic targets of clinical interventions.

Further, the graphical visualizations, as well as the
computational and statistical approaches used in this thesis are, I believe,
important contributions that will be of value to other researchers who are using
large volumes of highly complex data. The 'revised
forest plots' that represent the main findings within each chapter are a unique,
convenient, and relatively simple way to present these types of results and it is
hoped that these approaches will be adopted by other researchers.
The plots help to visually emphasize significant associations and their
magnitudes; these features allow easy and quick comparisons across a large number of
predictors and outcomes, all in a single figure. Similarly, because of the
volume and complexity of the data, in order to efficiently and meaningfully
analyze and present the results, I had to develop computer programs to complete
these tasks. These programs I released online (three of which were published in
CRAN, the official R software package repository), are publicly accessible, and
usable by anyone as they are licensed under the highly permissive MIT copyright
license. These programs I developed used standard software development practices
(e.g. unit tests, continuous integration, cross-platform compatibility checks,
version control), which ensure that my programs perform accurately and provide
scientific rigor and reliability. Other researchers can use these programs in
their own work to speed their own analyses.
Likewise, the code I used to analyze my own results from each chapter are
publicly accessible, such that other researchers can reproduce my findings or use
my analytic and computational process in their own research.

## Future directions

There are several promising directions for future research. First, there are
limited data available that have described the interrelationship of individual
fatty acids across all serum lipid fractions, nor has any study explored
correlations of fatty acids along DNL pathways (e.g. 14:0 elongated to 16:0) in
large cohort settings. The data available within this cohort would allow for the
exploration of this knowledge gap by characterizing the consistency and
correlation between fatty acids across all four lipid fractions. This analysis
would likely involve some simple correlations and hierarchical cluster analysis
of the correlations to uncover how each fatty acid associates with the fatty
acid composition within and across fractions. Given that fatty acid DNL
follows a biological pathway, e.g. palmitic acid (16:0) is elongated to stearic
acid (18:0) or desaturated to palmitoleic acid (16:1n-7), more advanced,
confirmatory statistical techniques such as structural equation modeling could
be used to describe differences in DNL correlations between fractions. Using
such a big picture approach may uncover novel differences between fatty acids
and fractions that could help guide future research questions in this area.
The current analyses used in my PhD could also be repeated by using
Metabolic Syndrome [@Alberti2009] components and other metabolic biomarkers as
outcomes. 

There are other novel research questions that could be addressed in future
research. The fatty acid measures were quantified from fasting blood samples,
however, the post-prandial lipid response may differentially influence glucose
metabolism compared to fasting. The fatty acid composition during the
post-prandial response may change and specific patterns of change may have 
unique influences on glucose metabolism. As
a result of my finding regarding the DNL fatty acid cluster within the TAG fraction,
Drs. Bazinet and Hanley are collaborating on a project to measure the ratio
of the stable isotopes carbon-12 to carbon-13 in these DNL fatty acids
(specifically 16:1n-7) using a unique measurement technique. The ratio of
carbon-12 to carbon-13 is an indicator of the source of plants from which carbon is
obtained, such that the specific ratio could be used as a marker of whether
the diet is high in certain foods (e.g. higher in corn-derived carbohydrates).
Lastly, the finding of the strong positive correlation of the TAG fraction DNL
cluster with clinical TAG as well as the strong consistent associations seen
with *cis*-vaccenic acid warrant further investigation.

## Conclusion

In a Canadian cohort of individuals at risk for T2DM, who were mainly female and
of primarily European ancestry, I found that fatty acids had differential
associations with insulin sensitivity and beta-cell function that were dependent
on the specific lipid fraction and the specific species of fatty acid. In
particular (and briefly), I found that:

- Cholesteryl ester fatty acids generally had null associations with the
outcomes, as described in Chapter 4.
- Phospholipid gamma-linolenic acid (18:3n-6) and stearic acid (18:0) had
negative associations while oleic acid (18:0) had positive associations with
insulin sensitivity (all as mol%, no associations with concentration data). 
*Cis*-vaccenic acid (18:1n-7) had strong positive associations with both insulin
sensitivity and beta-cell function. Higher palmitic acid (16:0) and lower 
eicosadienoic acid (20:2n-6) as mol% associated with a decline in beta-cell 
function over 6 years. These findings are described in Chapter 4.
- Higher total NEFA concentration associated with lower beta-cell
function, independent of the specific fatty acid composition of the NEFA
fraction, as described in Chapter 5.
- As concentrations, nearly all TAG fatty acids had very strong negative 
associations with insulin sensitivity, and to a lesser extent beta-cell
function. As a mol%, four fatty acids (14:0, 16:0, 14:1n-7, and 16:1n-7) had
negative while most others (e.g. 18:1n-9, 22:1n-9, 20:2n-6, 22:5n-3) had strong
positive associations with insulin sensitivity. *Cis*-vaccenic acid had very
strong positive associations with insulin sensitivity and beta-cell function.
Four fatty acids (14:0, 16:0, 14:1n-7, 16:1n-7) clustered together in predicting
lower insulin sensitivity; these fatty acids are the products of the DNL of
carbohydrates. These findings are described in Chapter 6.

The results of this PhD research project emphasize that fatty acids have complex
and differential associations on glucose metabolism. In particular, strong
harmful associations were seen for DNL fatty acid products, likely derived from
simple carbohydrate consumption, on glucose metabolism that may impact risk for
T2DM. Findings from this PhD provide potential future avenues of research that
could influence prevention strategies
for T2DM through mechanisms that influence fatty acid metabolism and lipogenesis.
