
pandoc 00-contributions.md -o 00-contributions.tex --chapters --natbib --wrap=none
pandoc 01-introduction.md -o 01-introduction.tex --chapters --natbib --wrap=none
Rscript -e "rmarkdown::render('02-lit-review.Rmd')"
pandoc 02-lit-review.md -o 02-lit-review.tex --chapters --natbib --wrap=none
pandoc 03-objectives.md -o 03-objectives.tex --chapters --natbib --wrap=none
pandoc 04-plce.md -o 04-plce.tex --chapters --natbib --wrap=none
pandoc 05-nefa.md -o 05-nefa.tex --chapters --natbib --wrap=none
pandoc 06-tag.md -o 06-tag.tex --chapters --natbib --wrap=none
pandoc 07-discussion.md -o 07-discussion.tex --chapters --natbib --wrap=none

# sed -i -e "s/begin{longtable}\[\]/begin{tabulary}{\\\textwidth}/" \
#     -e "s/end{longtable}/end{tabulary}/" \
#     02-lit-review.tex
#
# sed -i -e "s/begin{longtable}\[\]/begin{tabulary}{\\\textwidth}/" \
#     -e "s/end{longtable}/end{tabulary}/" \
#     -e ""
#     06-tag.tex
