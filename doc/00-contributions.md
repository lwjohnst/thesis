 \chapter*{Statement of Contributions}

During my PhD, my work included a variety of tasks, many of which are not
commonly done in the Department of Nutritional Sciences and not immediately
obvious from my thesis. In addition to identifying knowledge gaps, refining
the research questions, conducting the statistical analysis, writing the
manuscripts, and presenting at conferences (oral/poster), I've also
contributed these products/tasks below:  

- Assisted the research nurse in Mount Sinai clinic with calling
participant to schedule clinic visits and by helping with data collection.
- Managed the PROMISE datasets. Created several R software packages
to assist with management, curation, and integrating new data. These
packages are found at:
    - <https://github.com/lwjohnst86/PROMISE.methods>
    - <https://github.com/lwjohnst86/PROMISE.scrub>
- Created several R software packages to run my statistical analysis
and for visual presentation of the main results for the modeling. Found
at:
    - <https://github.com/lwjohnst86/seer>
    - <https://cran.r-project.org/package=mason> (available as an official CRAN
    package)
- Contributed to an existing CRAN package (`broom`) by adding a method to the
package to incorporate the statistical modeling used in my PhD research.
- Created other packages to simplify and automate many aspects of my
(and others) research: 
    - `prodigenr` (<https://cran.r-project.org/package=prodigenr>): Create
    standard folders and files to adhere to reproducible analyses (available as
    an official CRAN package)
    - `carpenter` (<https://cran.r-project.org/package=carpenter>): Automate the
    creation of tables to improve reproducibility of the manuscript (available
    as an official CRAN package).
