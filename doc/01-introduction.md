
# Introduction

Type 2 diabetes mellitus (T2DM) is a major global public health concern. There 
were an estimated 2.4 to 3.4 million Canadians living with diabetes [@IDF2015a] 
in 2015, and this number is projected to increase to 5 million by 2025. T2DM is
a complex chronic disease with multiple risk factors and a protracted etiology.
Although T2DM is characterized and diagnosed by elevated glucose concentrations,
fatty acid and glucose metabolism are tightly integrated, and thus the role of fatty
acids in the pathogenesis of diabetes has been an area of active research for
many years, particularly in experimental studies involving non-human animals,
cell lines, or short term human infusion trials. 
Investigating the contribution of fatty acids on T2DM risk is paramount to
understanding prevention and management. There has been
growing appreciation for the role that specific fatty acids have on the risk for
T2DM, and in this context, a number of previous studies have examined individual
fatty acids (e.g. palmitic acid) in short term controlled experiments. However,
individual fatty acids and the various lipid fractions in which they are found
are highly interdependent within a complex biological network, the complexity of
which is crucial to understanding the role that fatty acids play in T2DM. There
is thus a need for high quality, long term prospective studies that examine the broad
spectrum of fatty acids in multiple lipid fractions, to better understand the
interaction between fatty acids and the pathogenesis of T2DM. A more detailed
understanding of the influence of fatty acids on glucose metabolism could lead
to improved strategies to prevent or manage T2DM.

In this context, the aim of this thesis was to investigate the associations
of fatty acids from multiple lipid fractions within serum with the pathogenesis 
of T2DM (with a specific focus on their role in modulating insulin sensitivity and beta-cell
function). Chapter 2 will describe the natural history of T2DM, how fatty acids
are involved in health, and specifically how fatty acids influence glucose metabolism. In
Chapters 4, 5, and 6, the results from analyses of the different lipid fractions
are presented. Lastly, in Chapter 7, the results of each data
chapter will be discussed as an integrated whole. No study to date has presented
such a comprehensive assessment of fatty acids in four serum lipid fractions and
their impact on glucose metabolism measures in a longitudinal setting. We anticipate
the results of this research will extend the current knowledge base on this
topic.
