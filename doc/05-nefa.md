
# Total non-esterified fatty acids, but not individual fatty acids, predict progression in beta-cell dysfunction: The Prospective Metabolism and Islet Cell Evaluation (PROMISE) cohort

\chaptermark{NEFA and T2DM}

This chapter is undergoing a revise and resubmit with the journal Diabetologia.

\begin{chapabstract}

\textbf{Aim}: Our aim was to determine the longitudinal associations of individual
non-esterified fatty acids (NEFA) on changes in insulin sensitivity (IS) and
beta-cell function over 6 years in a cohort of individuals who are at-risk for
diabetes.

\textbf{Methods}: In the PROMISE cohort, 477 participants had serum
NEFA measured at the baseline visit and completed an OGTT at 3 time points over
6 years. Outcome variables were calculated using the OGTT values. IS was
assessed using 1/HOMA-IR and the Matsuda index, while beta-cell function was
assessed using the Insulinogenic index over HOMA-IR (IGI/IR) and the Insulin
Secretion-Sensitivity Index-2 (ISSI-2). Generalized estimating equations were
used, adjusting for time, waist, sex, ethnicity, baseline age, alanine
aminotransferase, and family history of diabetes. NEFA were analyzed as both
concentrations (nmol/mL) and proportions (mol%) of the total fraction.

\textbf{Results}: IS and beta-cell function declined by 14-27\% while BMI did not
significantly change over 6 years of follow-up. Total NEFA, 16:0, 18:1n-9, and
18:2n-6 as concentrations had significant negative associations with IGI/IR and
ISSI-2, predicting between 4-8\% lower IGI/IR and ISSI-2 over the 6 years.
However, individual NEFA modeled as mol\% showed no associations with these
outcomes. Total and individual NEFA were not associated with IS measures.

\textbf{Conclusions}: Total NEFA concentration was a strong predictor of lower beta-cell 
function over 6 years. Our results suggest that the association with beta-cell 
function is due to the absolute size of the serum NEFA fraction, rather than the
specific fatty acid composition.

\end{chapabstract}







## Introduction

Total non-esterified fatty acids (NEFA) have been well documented to influence 
the pathogenesis of type 2 diabetes mellitus. Experimental work has shown that 
exposure to high concentrations of NEFA can induce insulin resistance in 
insulin-sensitive tissues such as muscle and liver, and in addition can impair 
pancreatic beta-cell production of insulin [@Rachek2014a;@Cnop2008a].
Observational and clinical studies have reported concordant findings, showing
in particular that elevated total plasma NEFA associates with an increased risk
for incident type 2 diabetes [@Paolisso1995a;@Salgin2012a].

Much of the previous experimental work on the role of NEFA in type 2 diabetes
utilized individual fatty acids such as palmitic acid (16:0) or oleic acid
(18:1n-9), or alternatively used specific oils such as soybean oil, which is
high in the polyunsaturated fatty acid (PUFA) linoleic acid (18:2n-6), as the
exposure to characterize the metabolic impact of total NEFA. However, fatty acids comprise
multiple molecules with diverse physiological functions, and few 
studies have analyzed the effects of a broader spectrum of fatty acids. Notably,
one study suggested that eicosapentaenoic acid (20:5n-3) can protect against the
lipotoxic effect of palmitic acid in beta-cells [@Kato2008].

Despite a sizable literature studying the role of total NEFA concentration in 
diabetes, there are important gaps in this research. The majority of previous 
studies have used animal models or cell lines [@Maris2013a;@Deguil2011a], 
have been short term human trials 
[@Szendroedi2012a;@Daniele2014a;@Liang2013a;@Kehlenbrink2012a], or have
been epidemiological studies that only looked at total NEFA and not individual
fatty acids [@Miller2012a;@Johns2014a]. To date, there have
been no longitudinal studies examining the role of the composition of the serum NEFA
fraction on the pathogenesis of diabetes, which is critical given the protracted natural
history of diabetes and the growing appreciation of the divergent effects of
individual fatty acids. Therefore, our objective was to examine the 
association of serum NEFA composition on changes over time in insulin
sensitivity and beta-cell function in a longitudinal cohort. We hypothesized
that higher palmitic acid and lower PUFA such as
eicosapentaenoic acid in the NEFA fraction would associate with declining insulin sensitivity and
beta-cell function over 6 years.

## Methods

Participants from London and Toronto, Canada, were recruited into the 
Prospective Metabolism and Islet cell Evaluation (PROMISE) cohort. Eligibility 
for recruitment into PROMISE required having one or more risk factors for type 2
diabetes mellitus, including obesity, hypertension, family history of diabetes, 
and/or a history of gestational diabetes or birth of a macrosomic infant. 
Participants aged 30 years and older (n=736) attended the baseline visit
between 2004-2006. Follow-up examinations in this cohort occur every three years, with three
examination visits completed to date (2004-2006, 2007-2009, and 2010-2013). 
Participants are contacted annually by telephone. The
current study sample used data on participants who did not have diabetes at baseline,
who returned for one or more of the follow-up examinations, and who had samples
available for fatty acid measurements (n=477). A diagram of the sample size at
each visit is shown in Supplemental Figure 5.1. At each examination,
participants undergo metabolic characterization, anthropometric measurements,
and questionnaires on lifestyle and sociodemographics. Research ethics approval
was obtained from Mount Sinai Hospital and the University of Western Ontario,
and all participants provided written informed consent.
Data collection methods were standardized across the 2 centres and research
nurses were centrally trained. 

### Blood measure assessments

At each examination, an 8-12 hour fasting blood sample was drawn from each
participant, followed by a 75g oral glucose tolerance test (OGTT) with a 30
minute and 2 hour blood draw. All blood samples were processed and frozen at
-70°C. Alanine aminotransferase (ALT) was measured using standard laboratory
procedures. Cholesterol, HDL, and triacylglycerides (TAG) were measured using Roche
Modular's enzymatic colorimetric tests (Mississauga, ON).  Both insulin
and glucose were derived from the OGTT at fasting, 30 minute, and 2 hour time
points. Specific insulin was measured using the Elecsys 1010 (Roche Diagnostics,
Basel, Switzerland) immunoassay analyzer and electrochemiluminescence
immunoassay. This assay shows 0.05% cross-reactivity to intact human pro-insulin
and the Des 31,32 circulating split form (Linco Res. Inc), and has a coefficient of variation (CV) of 9.3%.
Glucose was determined using an enzymatic hexokinase (Roche Modular, Roche
Diagnostics) with a detection range of 0.11 (2 mg/dL) to 41.6 mmol/L.  The
inter-assay %CV is <1.1% and intra-assay %CV is < 1.9%.  All assays were
performed at the Banting and Best Diabetes Centre Core Lab at Mt Sinai Hospital.
Impaired fasting glucose (IFG), impaired glucose tolerance (IGT), and diabetes
were categorized using the 2006 WHO criteria [@WHO2006].

NEFA composition was quantified using stored fasting serum samples from the 
baseline visit, which had been frozen at -70°C for 4-6 years and had not been 
exposed to any freeze-thaw cycles. Serum fatty acids have been documented to be
stable at these temperatures for up to 10 years [@Matthan2010a]. A known amount 
of heptadecanoic acid (17:0) was added to the serum as an internal 
standard prior to extracting total lipids according to the method of Folch 
[@Folch1957a]. Each serum lipid fraction (NEFA, cholesteryl ester, phospholipid,
and TAG) was isolated using thin layer chromatography; each fraction was
visualized under UV light after lightly spraying with 8-anilino-1-naphthalene
sulfonic acid (0.1% wt/vol) and then converted to fatty acid methyl esters with
14% boron trifluoride in methanol at 100°C for 1 h. Fatty acid methyl esters
were separated and quantified using a Varian-430 gas chromatograph (Varian, Lake
Forest, CA, USA) equipped with a Varian Factor Four capillary column and a flame
ionization detector, which were injected in splitless mode. Fatty acid
concentrations (nmol/ml) were calculated by proportional comparison of gas
chromatography peak areas to that of the internal standards [@Nishi2014a]. 
There were 22 fatty acids measured in the NEFA fraction. Given their
diverse biology, as well as the complexity of the analyses, findings for other lipid
fractions in this cohort are reported separately (see ref @Johnston2016a for the
analysis of the phospholipid and cholesteryl ester fractions).

### Outcome variables

Insulin sensitivity and beta-cell function indices were computed using the OGTT 
glucose and insulin data. Insulin sensitivity was assessed using 1/HOMA-IR (1
divided by HOMA-IR) [@Matthews1985] and the Insulin Sensitivity Index (ISI)
[@Matsuda1999]. HOMA-IR largely reflects hepatic insulin resistance, while ISI
reflects whole-body insulin sensitivity [@AbdulGhani2007].  Beta-cell function
was assessed using the Insulinogenic Index [@Wareham1995] over HOMA-IR (IGI/IR)
and the Insulin Secretion-Sensitivity Index-2 (ISSI-2) [@Retnakaran2009]. IGI/IR
is a measure of the first phase of insulin secretion while ISSI-2 is analogous to
the disposition index (but using OGTT values). Each index has been validated
against gold standard measures [@Matthews1985; @Matsuda1999; @Retnakaran2009].

### Anthropometrics and sociodemographics

Height, weight, and waist circumference (WC) were measured at all clinic 
examinations using standard procedures.  WC was measured at the natural waist, 
defined as the narrowest part of the torso between the umbilicus and the xiphoid
process. BMI was calculated by dividing weight (kg) by height (m) squared.
Sociodemographic information, including age, sex, and ethnicity, were determined
using questionnaires administered at each examination. In the lifestyle
questionnaire, physical activity was determined using a version of the
Modifiable Activity Questionnaire (MAQ) [@Kriska1990].  The MAQ collects 
information on leisure and occupational activity, including intensity, 
frequency, and duration, over the past year.  Each reported activity from the 
MAQ was weighted by its metabolic intensity allowing for the estimation of 
MET-hours per week.

### Statistical analysis

The primary outcome variables for this analysis were 
1/HOMA-IR, ISI, IGI/IR, and ISSI-2; outcome variables were log-transformed for
the statistical modeling. The primary predictor variables for this analysis were
22 individual NEFA as mole percent (mol%) of the total fraction and as
concentration (nmol/mL). Pearson correlation coefficients were computed to
assess the relationships of individual NEFA with other continuous variables.

For the primary analysis, generalized estimating equation (GEE) models 
[@Zeger1986a] were used to determine the longitudinal associations between the 
outcome variables and the predictor variables. Given the longitudinal design 
an auto-regressive of order 1 (AR1) correlation matrix was chosen for the GEE models, 
though other matrices (eg. exchangeable) had similar fit (data not shown). GEE
is well suited to longitudinal cohort studies given it's capacity to handle
missed visits. The predictor variables and continuous covariates were scaled
(mean centered and standardized). The NEFA variables were classified as
*time-independent* (held constant) as they were measured only at the baseline
visit, while the outcome variables and covariates were set as *time-dependent*.
No imputation was conducted on missing values.

Covariate selection was based on previous literature, directed acyclic graph 
[@Greenland1999a] recommendations, and quasi-likelihood information criterion 
(QIC). Supplemental Table 5.1 shows the covariates compared using QIC. While the
final GEE model selected as best fitting differed between insulin sensitivity
and beta-cell function measures, the differences in QIC values were less than
10 between the models, suggesting similar fit. As such, we selected the model
that had the fewest covariates and that had similar fit between the outcome
measures (M8). The final GEE model was adjusted for time, sex, ethnicity,
baseline age, WC, ALT, and family history of diabetes. After scaling,
log-transforming, and exponentiating, the GEE estimates are
interpreted as an expected percent difference in the outcome variable for every
standard deviation (SD) increase in the predictor variable given the covariates are held constant.
Since TAG is a risk factor for diabetes and since NEFA contribute to TAG
production, TAG may act as a mediator between NEFA and the outcomes. To
determine the role of TAG in the association between NEFA and the outcomes, TAG
was included in the GEE model as a sensitivity analysis. Lastly for the GEE
models, we tested for an interaction with sex, ethnicity, or time by the
predictor term for each outcome variable.

While GEE accounts for the longitudinal design of the data, this approach is
limited in that it cannot analyze the inherent multivariate nature of the
composition of the NEFA fraction. Therefore, to confirm the results of the GEE
analyses in a multivariate environment (all NEFA variables in a single model),
partial least squares discriminant analysis (PLS-DA) was used to identify the
patterns of NEFA composition against beta-cell function (using ISSI-2, which had
the majority of the associations in the GEE models) and for those
who converted to or maintained dysglycemia status (either IFG, IGT, or DM) over
the 6 years. Latent class mixed models (LCMM) were used to extract the underlying
trajectories of beta-cell function (ISSI-2) over the 6 years. For more detailed
explanation of GEE, PLS-DA, and LCMM, please see the ESM Methods.

All analyses were performed using R 3.4.0 [@Rbase], along with the R
packages geepack 1.2.1 for GEE [@Hoejsgaard2006a], caret
6.0.76 for PLS-DA, and lcmm 1.7.8 for
LCMM. The R code for this manuscript is available at 
https://dx.doi.org/10.6084/m9.figshare.3472433
. Results were considered statistically significance at p<0.05,
after adjusting for multiple testing using the Benjamini-Hochberg False
Discovery Rate [@Benjamini1995a]. STROBE was used as a guideline for reporting [@Vandenbroucke2007b].

## Results









### Basic characteristics of the PROMISE cohort

For this study, there were 
349 (73%) females and 337 (70.5%) who had European-ancestry.
The average age of the participants was 50.2 years 
(9.8 SD) and the average BMI was
31.1 (6.4 SD). Most of the 
participants, 308 (64.8%), had a family history of diabetes.
Insulin sensitivity and beta-cell function measures showed a significant median
decline between
14% to 27% (p<0.001) over the 6
years in this analysis of the PROMISE cohort.
Consistent with this decline, there were 42 (9%)
participants who developed diabetes while 96 (20%) participants either
converted to or maintained IFG or IGT status over the 6 years.
<!-- maintain vs convert -->

\newpage\newpage

| Measure        |      Baseline       |        3-yr         |        6-yr         |
|:---------------|:-------------------:|:-------------------:|:-------------------:|
| HOMA-IR        |   13.1 (8.5-22.1)   |  16.3 (10.0-27.1)   |  16.6 (10.9-26.1)   |
| ISI            |   13.6 (8.7-21.8)   |   11.6 (6.9-19.1)   |   11.6 (7.5-17.5)   |
| IGI/IR         |   7.1 (4.2-10.6)    |    5.6 (3.6-9.8)    |    5.6 (3.5-9.0)    |
| ISSI-2         | 727.5 (570.0-922.5) | 613.4 (493.9-836.7) | 622.5 (472.5-810.3) |
| BMI (kg/m^2^)  |     31.1 (6.4)      |     31.4 (6.5)      |     31.1 (6.6)      |
| WC (cm)        |     98.5 (15.5)     |     99.3 (15.7)     |    100.4 (15.7)     |
| Age (yrs)      |     50.2 (9.8)      |     53.2 (9.7)      |     56.3 (9.5)      |
| MET            |     44.9 (59.6)     |     48.4 (60.5)     |     44.0 (57.1)     |
| ALT (U/L)      |     29.6 (16.0)     |     28.4 (19.5)     |     25.9 (16.9)     |
| TAG (mmol/L)   |      1.5 (0.8)      |      1.4 (0.8)      |      1.4 (0.7)      |
| Chol (mmol/L)  |      5.2 (0.9)      |      5.1 (1.0)      |      5.1 (0.9)      |
| HDL (mmol/L)   |      1.4 (0.4)      |      1.3 (0.4)      |      1.4 (0.4)      |
| NEFA (nmol/mL) |    383.4 (116.4)    |                     |                     |
| Ethnicity      |                     |                     |                     |
| - European     |      337 (71%)      |                     |                     |
| - Latino/a     |      58 (12%)       |                     |                     |
| - Other        |      51 (11%)       |                     |                     |
| - South Asian  |       32 (7%)       |                     |                     |
| Sex            |                     |                     |                     |
| - Female       |      349 (73%)      |                     |                     |
| - Male         |      129 (27%)      |                     |                     |

Table: Table 5.1: Basic characteristics of the PROMISE participants at each of the 3 clinic visits.

Note: Values are in median (IQR), mean (SD), and n (%). The proportion of ethnic
and sex groups did not change over the 6 years. Outcome variables sample size
ranges from 367 to 470 over the 3 visits.

\newpage\newpage

Figure 5.1 shows the compositional distribution of NEFA in the study
participants. Four individual NEFA made up the vast majority 
(89.3%) of the total NEFA fraction. The largest contributors
were 18:1n-9 (36.6%), 16:0 (23.5%), 18:0 (15.2%), and 
18:2n-6 (14%). Raw concentration values are shown in Supplemental Table 5.2. 
All individual NEFA as well as the total fraction had
correlations that ranged from weak to null (r<0.3) with participant
characteristics (see Supplemental Figure 5.2).

\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/figure1-1.pdf}
\caption{Figure 5.1: Concentrations (nmol/mL) of each NEFA in PROMISE participants at the baseline visit (2004-2006).}
\end{figure}


### GEE modeling

A number of associations were seen in the unadjusted GEE models 
(see Supplemental Figure 5.3), particularly for NEFA modeled as a concentration.
While full model adjustment (Figure 5.2) attenuated most of these
associations, total NEFA, 16:0, 18:1n-9, and 18:2n-6 (all as nmol/mL)
continued to show negative associations with IGI/IR and ISSI-2. Sensitivity
analyses revealed that adjustment for WC was responsible for attenuating the
unadjusted results. The magnitude of association for each of these variables was
fairly consistent for each beta-cell function measure. For every one SD increase
in any of these three NEFA variables, there was an average predicted
8.4% lower IGI/IR and 4.1% lower ISSI-2 at each
clinic visit. However, in contrast to these results using NEFA modeled as
concentrations, none of the NEFA variables modeled as mol% were associated with
the outcomes. Adjusting for TAG attenuated all associations with IGI/IR and
ISSI-2 (data not shown). Sensitivity analyses to examine the association with
the sum of fatty acid classes (saturated vs unsaturated) revealed similar
findings as seen with the adjusted GEE model for total NEFA (data not shown).
There were no significant interaction effects between time, sex, or ethnicity
and the individual NEFA on any of the outcome measures, except for a single
marginally significant (p=0.04) interaction between sex, 20:3n-6 (mol%), and
ISI, which showed that males had a 12% higher ISI for every SD increase in
20:3n-6 compared to females. Raw values from the GEE models are shown in Supplemental Table 5.3 for
fully adjusted models and Supplemental Table 5.4 for unadjusted models.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/figure2-1.pdf}
\caption[Figure 5.2: GEE models of NEFA with outcomes.]{Figure 5.2: Longitudinal associations of individual NEFA (mol\% and nmol/mL) with insulin sensitivity and beta-cell function over the 6 years in the PROMISE cohort. Fully adjusted generalized estimating equation models. X-axis values represent a percent difference in the outcome per SD increase in the fatty acid. P-values were adjusted for the BH false discovery rate, with the largest dot representing a significant (p<0.05) association.}
\end{figure}

### Latent trajectories and differences in NEFA composition

Three latent classes were extracted from LCMM, with 118 
participants in the high, 269 in the middle, and 
86
in the low beta-cell function groups. The trajectories of these groups are
shown in Supplemental Figure 5.4. These groups were used as the beta-cell function
response variables in the PLS-DA analysis, which was used to confirm the results
of the GEE modeling, but in a multivariate environment (all NEFA in a single
model). The PLS-DA results show a very poor discriminatory ability of the
NEFA composition in classifying participants into the correct beta-cell function
latent class. The percent of participants correctly predicted to belong to
their group (i.e. those predicted in the low group and actually belonging to the
low group) was 1.2% in the low group,
97.8% in the middle group, and 
11.9% in the high group (the distribution of the
groupings based on the components from PLS-DA are shown in Supplemental Figure 5.5). 
The loadings plot of the NEFA composition against the extracted PLS-DA
components is shown in Supplemental Figure 5.6 and is used to identify
fatty acids that contribute the most explained variance to the beta-cell
function groupings. However, because of the high rate of misclassification, no
meaningful predictive information can be extracted even if any NEFA does
contribute to the explained variance (e.g. 16:0). The PLS-DA analysis using
conversion to or maintenance of dysglycemia status over the 6 years as the
response variable showed similar poor discriminatory ability, though better than
the beta-cell function trajectory results. While 
99.1%
of participants were correctly predicted to not have dysglycemia, only 
4.4% were correctly predicted to have
dysglycemia (see Supplemental Figure 5.7 for the distribution of 
dysglycemia status based on the components from the PLS-DA and 
Supplemental Figure 5.8 for the loadings plot of the NEFA composition
by extracted PLS-DA components). The results from the PLS-DA analysis are consistent
with the associations seen in the GEE models.

## Conclusions

In a Canadian population of adults who were at-risk for diabetes, we found that 
higher total NEFA concentrations independently predicted lower beta-cell
function after 6 years. While we found negative associations with palmitic acid
(16:0), oleic acid (18:1n-9), and linoleic acid (18:2n-6) for the concentration
(nmol/mL) modeling, no associations were seen for these fatty
acids as a proportion, an observation that was further confirmed by multivariate
cluster techniques, which found no ability of these
individual NEFA to predict beta-cell function or dysglycemia. These observations suggest
that the absolute size of the total NEFA fraction, rather than its specific
composition, likely influences the pathogenesis of diabetes, at least within a
population at-risk for diabetes.

There are a few important limitations to our study. NEFA were only quantified at
the baseline visit and as such we cannot investigate whether there were 
concomitant changes in NEFA and the metabolic measures over time. However, we believe this
is a strength for our specific objective, as the chance of reverse causality is 
reduced given that fatty acid and glucose metabolism pathways are tightly 
integrated. This is also an observational cohort, and there may be some residual
confounding we have not considered or that could not be measured. Nonetheless,
potential covariates were empirically analyzed prior to inclusion into the
GEE models to best understand and minimize potential
confounding. Finally, our cohort consists of individuals at-risk for diabetes, 
who are primarily female and European-ancestry and as such our results may not be
generalizable to other populations. However, given these limitations, our study
also has several strengths, including the longitudinal design and the rigorous
statistical techniques and methods applied, which are specifically suited to
investigating temporal relationships and to handling the multivariate nature of
the data. Lastly, our cohort contains highly detailed and comprehensive
variable measurements at each collection visit, and has both concentration and
mol% data for the fatty acids.

The role of total NEFA in the etiology of diabetes is well-documented.
Epidemiological studies have shown that higher NEFA associate with lower insulin
secretion and a higher risk for developing diabetes 
[@Paolisso1995a;@Salgin2012a;@Pankow2004a]. In a cross-sectional analysis of the
RISCK cohort, total NEFA had a negative association with insulin sensitivity and
a particularly strong negative association with beta-cell function
[@Johns2014a]. Experimentally, several potential mechanisms have been elucidated
for the role of NEFA on beta-cell function, particularly for palmitic acid.
Prolonged exposure to elevated NEFA can induce apoptosis in the beta-cells,
possibly through endoplasmic reticulum stress, formation of ceramides, and
generation of nitric oxide, as well as impairment of proinsulin production and
mitochondrial function [@Cnop2008a; @Maris2013a; @Giacca2011a; @Xiao2009a]. The
present analysis is the first study to our knowledge to examine the longitudinal
association of a broad spectrum of individual NEFA on beta-cell function in a
large cohort. We found that there was a strong signal of higher total NEFA,
palmitic acid, oleic acid, and linoleic acid modeled as concentrations with
lower beta-cell function. However, in modeling these fatty acids as a mol% and
using novel clustering analysis approaches, no specific fatty acids in the NEFA
fraction strongly predicted lower beta-cell function, insulin sensitivity, or
dysglycemia status. Taken together, these results suggest that it is the
absolute size of the circulating NEFA fraction, irrespective of any specific
composition of fatty acids, that is responsible for the hypothesized lipotoxic
effects of NEFA on the beta-cells.

Biologically, in free-living populations chronic elevation of NEFA may be 
mediating its association with metabolic outcomes through TAG. In normal 
metabolism, NEFA enters the liver and assists in the production of TAG that is
to be processed into very-low density lipoproteins (VLDL) [@Nielsen2012a]. As
such, higher NEFA may contribute to hypertriglyceridemia, which is a known risk
factor for diabetes. In the sensitivity analysis adjusting for TAG, all
associations with beta-cell function were attenuated, suggesting that NEFA may
in fact be mediating its association with beta-cell dysfunction through higher
TAG.

There is substantial experimental evidence highlighting the role of increased 
NEFA and the subsequent increase in insulin resistance via impairment of insulin 
signaling cascades, as reviewed in previously published articles 
[@Martins2012a;@Ebbert2013a;@Capurso2012a]. However, in this longitudinal
analysis, we saw no association of any individual or total NEFA with hepatic
(1/HOMA-IR) or whole-body insulin sensitivity (ISI). We identified
WC as having a powerful attenuating effect on the associations between
unadjusted and adjusted models. There are some possible explanations for our
null findings for the insulin sensitivity measures and the influence of WC on
the results. First, WC may be a strong causal link in previously reported
associations between NEFA and insulin sensitivity, given the role adipocytes
play in metabolism (e.g. leptin, adiponectin). Second, there may be differences
in physiology between fasting and postprandial NEFA kinetics that we are not
able to investigate but that may explain our null findings for fasting NEFA and
insulin sensitivity. For instance, some experimental studies using clamp
protocols found that fasting NEFA was a weak predictor of insulin sensitivity
compared to postprandial concentrations of NEFA
[@Magkos2012a;@Kehlenbrink2012a]. Inefficiencies in NEFA uptake into the adipose
tissue (e.g. as seen in obesity and larger WC) following postprandial TAG
lipolysis via lipoprotein lipase may result in NEFA spillover into the blood and
a subsequent increase in circulating NEFA [@Almandoz2013a], which may be more
metabolically active given postprandial activity. Third, the null findings seen
may be due to the high risk population examined in PROMISE, as the majority of subjects had a 
high WC and BMI. It may be that in this population, IR has become well
established and NEFA may not contribute to IR at this somewhat more advanced
stage in the pathogenesis of diabetes.

Few studies have examined the composition of NEFA on metabolic functioning. One 
recent, well-analyzed study used a variety of advanced fatty acid measurement 
and statistical techniques to explore the multivariate relationship between the 
NEFA composition and components of the metabolic syndrome (MetS) [@Dai2015a]. 
Specifically, the authors identified NEFA 16:1n-9, 20:1n-9, and 22:4n-6 to
correlate with components of the MetS. Another similar study examining diabetes
found that 16:0, 18:0, 18:1, 18:2, 18:3 may be useful biomarkers for identifying
healthy compared to individuals with diabetes [@Liu2010a]. However, both studies were
limited by smaller sample sizes (approximately 100 subjects) and the
cross-sectional design.

In conclusion, we found that total NEFA was a strong predictor for lower
beta-cell function over 6 years, irrespective of the specific composition of the
NEFA fraction, suggesting that efforts at lowering circulating total NEFA
through medication and/or lifestyle strategies to reduce the risk of diabetes
are well warranted. While future
studies are needed to confirm these findings, our results reinforce the
importance of continuing to investigate the role of circulating NEFA
concentration on the natural history of diabetes.

## Supplemental Material

### Supplemental Methods

Generalized estimating equations (GEE) is analogous to mixed effects models, 
except it emphasizes population-level trends over individual-level trends that 
mixed models use. Because it can handle autocorrelations in the outcome (as 
would be seen in variables measured over time), GEE modeling is well suited to
longitudinal data. Latent class mixed models (LCMM) is a form of mixed effects
modeling that tries to categorize individuals into groups that have similar
clusters; in the case of the present analysis are the trajectories in beta-cell
function over time. Because mixed models use the participant as the random
effect, LCMM can identify individuals who similarly change over time.

Partial least squares (PLS) regression is a technique similar to principal
component analysis (PCA) that is designed to extract meaningful information from
multivariate, high dimensionality data (e.g. as in metabolomic and other -omic type 
analyses). These types of methods try to extract as much of the variance in the 
data into a smaller number of components or factors. The difference between PCA 
and PLS is that PLS is a supervised method, while PCA is not. This means that
PLS uses a response variable(s), i.e. an outcome or **Y**, to describe the
variation in the predictors (**X**) while PCA only describes the variation 
inherent in **X**. Because of this, PLS can be better at predicting the 
contributions of predictor variables against an outcome variable. Partial least
squares discriminant analysis (PLS-DA) is a form of PLS, except the **Y** is 
a discrete (i.e. categorical) variable and not a continuous one. Therefore, 
PLS-DA is able to predict who will belong to which group based on the values of
the predictors.

The two plots generated from the PLS-DA analysis show the results from both
the response (i.e. LCMM beta-cell function groups and dyslgycemia status) and
the predictors (all 22 NEFA). The response result plots as seen in 
Supplemental Figure 5.5 and Supplemental Figure 5.7 show the
poor discriminatory ability of the PLS-DA model, with Supplemental Figure 5.9 
showing a hypothetical example of good discriminatory ability. When the predictors represent an
underlying pattern that may exist between groups, the plots will show greater
separation between the groups in addition to a low misclassification rate. The
predictor result plots as seen in Supplemental Figure 5.6 and
Supplemental Figure 5.8 show which of the predictor variables
contribute the most to the explained variance. Variables between the solid and 
dashed lines explain between 50-100% of the explained variance. Whether the 
variables have positive or negative values on either of the components axes 
determines how they contribute to each group. For instance, if those individuals
with dysglycemia have a positive value for component 1 and the fatty acid 16:0 also
has a positive value for component 1, this indicates that 16:0 is predictive of
having dysglycemia.

### Supplemental Tables and Figures

\begin{figure}
\centering
\includegraphics[width=\textwidth]{../img/flowDiagramSample.pdf}
\caption{Supplemental Figure 5.1: CONSORT diagram of sample size at each examination visit.}
\end{figure}

\newpage\newpage
\small

| Model           |  QIC  |  Delta  |
|:----------------|:-----:|:-------:|
| **log(ISI)**    |       |         |
| M4              | -1583 |    0    |
| M5              | -1583 |   0.2   |
| M6              | -1582 |   0.8   |
| M3              | -1576 |   6.9   |
| M8              | -1574 |   9.5   |
| M2              | -1574 |   9.7   |
| M9              | -1573 |   10    |
| M7              | -1572 |  11.2   |
| M1              | -1539 |  44.1   |
| M0              | -1051 |  532.1  |
| **log(ISSI-2)** |       |         |
| M9              | -2478 |    0    |
| M8              | -2477 |   1.1   |
| M7              | -2477 |   1.1   |
| M5              | -2474 |   4.2   |
| M6              | -2473 |   5.1   |
| M2              | -2472 |   6.5   |
| M3              | -2470 |   8.5   |
| M4              | -2468 |   9.8   |
| M1              | -2458 |  20.5   |
| M0              | -2139 |  339.2  |

Table: Supplemental Table 5.1: Comparing generalized estimating equation models adjusting for different covariates using Quasi-Likelihood Information Criterion.

Given the number of possible combinations of outcome and predictor variables,
only ISI and ISSI-2 with total non-esterified fatty acids (nmol/mL) were used to
compare various GEE models and to select a final model.  Baseline age was used
as including both the original age and the time variable would result in
collinearity.  Column names are: QIC is the quasi-likelihood information criteria
(smaller values, eg. larger negative values, indicate a better fit compared to
other models), Delta is the QIC minus the lowest QIC (models with delta <10 are 
considered equivalent).  Models were:

- M0: log(ISSI-2) or log(ISI) = total non-esterified fatty acids (nmol/mL) + time
- M1: M0 + waist + sex + ethnicity + baseline age
- M2: M1 + ALT
- M3: M2 + physical activity
- M4: M3 + alcohol intake
- M5: M4 + family history of diabetes
- M6: M5 + smoking status
- M7: M2 + ALT + family history of diabetes + smoking status
- M8: M2 + ALT + family history of diabetes
- M9: M8 + time by NEFA interaction

\newpage\newpage

| NEFA    |  Concentrations (nmol/mL)  |
|:--------|:--------------------------:|
| 22:6n-3 |         1.7 (1.2)          |
| 22:5n-3 |         0.7 (0.4)          |
| 20:5n-3 |         0.7 (0.6)          |
| 18:3n-3 |         4.7 (2.1)          |
| 22:4n-6 |         0.4 (0.3)          |
| 20:4n-6 |         3.1 (1.4)          |
| 20:3n-6 |         1.5 (1.4)          |
| 20:2n-6 |         0.9 (0.4)          |
| 18:3n-6 |         0.7 (0.6)          |
| 18:2n-6 |        54.1 (19.4)         |
| 18:1n-7 |         10.1 (4.0)         |
| 16:1n-7 |         10.3 (6.9)         |
| 14:1n-7 |         0.3 (0.3)          |
| 24:1n-9 |         0.6 (0.8)          |
| 22:1n-9 |         0.3 (0.3)          |
| 20:1n-9 |         1.7 (1.5)          |
| 18:1n-9 |        142.5 (52.5)        |
| 22:0    |         0.2 (0.2)          |
| 20:0    |         0.7 (0.3)          |
| 18:0    |        55.2 (14.1)         |
| 16:0    |        90.0 (33.8)         |
| 14:0    |         3.2 (2.9)          |
| Total   |       383.4 (116.4)        |

Table: Supplemental Table 5.2: Raw concentration values (nmol/mL) of each non-esterified fatty acid in the PROMISE cohort at the baseline visit (2004-2006). Values are presented as mean (SD).

\normalsize

\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/ESM_Fig2-1.pdf}
\caption[Supplemental Figure 5.2: Correlation heatmap of NEFA with basic characteristics.]{Supplemental Figure 5.2: Pearson correlation heatmap of non-esterified fatty acids (nmol/mL) and basic PROMISE participant characteristics for the baseline visit (2004-2006). Darkness of the colour indicates the magnitude of the correlation, with orange indicating positive and blue indicating negative correlations.}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/ESM_Fig3-1.pdf}
\caption[Supplemental Figure 5.3: Unadjusted GEE models of NEFA with outcomes.]{Supplemental Figure 5.3: Unadjusted generalized estimating equation modeling of the longitudinal association of individual non-esterified fatty acids (mol\% and nmol/mL) with insulin sensitivity and beta-cell function over 6 years in the PROMISE cohort. GEE models are only adjusted for time. Outcome variables were log-transformed, predictor variables were scaled, and x-axis values were exponentiated to represent percent difference per SD increase in the fatty acid. P-values were adjusted for the BH false discovery rate, with the largest dot representing a significant (p<0.05) association (*).}
\end{figure}

\newpage\newpage
\footnotesize

| Fatty acid   | log(1/HOMA-IR)   | log(ISI)          | log(IGI/IR)          | log(ISSI-2)         |
|:-------------|:-----------------|:------------------|:---------------------|:--------------------|
| **mol%**     |                  |                   |                      |                     |
| 14:0         | 3.1 (-1.2, 7.6)  | 4.7 (0.2, 9.5)    | -1.8 (-7.3, 4.1)     | -0.5 (-3.3, 2.4)    |
| 16:0         | -0.9 (-4.5, 2.8) | -0.7 (-4.7, 3.4)  | -1.6 (-6.9, 4.0)     | -0.6 (-3.2, 2.1)    |
| 18:0         | 0.3 (-3.8, 4.6)  | 1.7 (-2.6, 6.3)   | 5.1 (-0.9, 11.5)     | 2.4 (-0.6, 5.5)     |
| 20:0         | 1.5 (-3.3, 6.4)  | 2.2 (-2.8, 7.5)   | 2.6 (-3.2, 8.9)      | 1.0 (-2.0, 4.1)     |
| 22:0         | -1.3 (-5.3, 2.8) | -1.3 (-5.3, 2.8)  | 1.8 (-2.8, 6.6)      | -0.2 (-2.6, 2.3)    |
| 18:1n-9      | 0.2 (-3.7, 4.2)  | -1.2 (-5.3, 3.1)  | -1.2 (-6.3, 4.1)     | -0.6 (-3.2, 2.1)    |
| 20:1n-9      | 2.5 (-1.8, 6.9)  | 2.8 (-1.6, 7.5)   | 0.6 (-6.3, 8.0)      | 0.6 (-3.2, 4.6)     |
| 22:1n-9      | -2.3 (-5.7, 1.3) | -1.0 (-4.5, 2.7)  | 4.6 (-0.6, 10.2)     | 2.1 (-1.0, 5.2)     |
| 24:1n-9      | 3.6 (-0.3, 7.6)  | 3.3 (-1.0, 7.7)   | 4.3 (-1.8, 10.8)     | 1.4 (-1.7, 4.6)     |
| 14:1n-7      | 3.3 (-1.7, 8.5)  | 4.3 (-0.6, 9.4)   | -0.8 (-7.5, 6.3)     | 0.0 (-3.0, 3.2)     |
| 16:1n-7      | 4.2 (-0.3, 8.9)  | 4.1 (-0.4, 8.8)   | -2.1 (-7.9, 4.2)     | -0.8 (-3.7, 2.2)    |
| 18:1n-7      | 1.1 (-3.4, 5.7)  | 0.9 (-3.8, 5.9)   | 0.1 (-5.7, 6.3)      | 0.8 (-2.3, 3.9)     |
| 18:2n-6      | -2.4 (-6.9, 2.2) | -2.3 (-6.9, 2.6)  | -3.5 (-9.4, 2.7)     | -2.1 (-5.1, 1.0)    |
| 18:3n-6      | 1.8 (-2.0, 5.7)  | 2.0 (-1.9, 6.0)   | 3.4 (-1.7, 8.8)      | 0.9 (-2.0, 3.9)     |
| 20:2n-6      | -0.7 (-4.7, 3.4) | -1.5 (-5.8, 3.1)  | -0.4 (-6.7, 6.2)     | -0.3 (-3.6, 3.0)    |
| 20:3n-6      | 0.5 (-3.2, 4.3)  | 0.2 (-3.9, 4.4)   | 3.5 (-2.1, 9.3)      | 1.2 (-1.7, 4.2)     |
| 20:4n-6      | 0.1 (-4.1, 4.4)  | -0.7 (-5.0, 3.8)  | 4.6 (-2.2, 11.8)     | 2.5 (-1.2, 6.3)     |
| 22:4n-6      | -2.9 (-7.0, 1.4) | -3.0 (-7.1, 1.2)  | 6.4 (-0.1, 13.3)     | 2.0 (-1.1, 5.2)     |
| 18:3n-3      | 0.3 (-3.4, 4.1)  | -0.5 (-4.5, 3.7)  | -0.8 (-6.1, 4.8)     | -0.5 (-3.2, 2.4)    |
| 20:5n-3      | 7.4 (2.2, 12.9)  | 5.9 (0.5, 11.6)   | 3.4 (-3.3, 10.6)     | 0.8 (-2.4, 4.2)     |
| 22:5n-3      | -2.0 (-5.5, 1.6) | -1.5 (-5.8, 3.0)  | 0.2 (-5.9, 6.7)      | 0.0 (-3.5, 3.5)     |
| 22:6n-3      | -2.8 (-6.0, 0.5) | -3.9 (-7.9, 0.3)  | 1.4 (-4.2, 7.2)      | -0.3 (-3.2, 2.7)    |
| **nmol/mL**  |                  |                   |                      |                     |
| Total        | -1.1 (-5.0, 2.9) | -2.9 (-6.8, 1.2)  | -8.4 (-13.4, -3.1)\* | -4.3 (-6.9, -1.7)\* |
| 14:0         | 1.7 (-2.7, 6.3)  | 2.8 (-1.8, 7.6)   | -3.9 (-10.1, 2.7)    | -1.5 (-4.5, 1.5)    |
| 16:0         | -1.5 (-5.5, 2.6) | -2.6 (-6.5, 1.5)  | -8.9 (-14.1, -3.3)\* | -4.3 (-6.9, -1.7)\* |
| 18:0         | -0.7 (-4.5, 3.3) | -2.0 (-5.8, 2.0)  | -4.3 (-9.7, 1.5)     | -2.4 (-5.3, 0.6)    |
| 20:0         | 0.7 (-4.0, 5.6)  | 0.1 (-4.7, 5.1)   | -1.7 (-8.5, 5.5)     | -1.2 (-4.8, 2.5)    |
| 22:0         | -1.2 (-5.6, 3.4) | -2.2 (-6.5, 2.4)  | 0.6 (-4.1, 5.6)      | -1.0 (-3.4, 1.5)    |
| 18:1n-9      | -0.9 (-4.8, 3.1) | -2.8 (-6.7, 1.4)  | -7.0 (-11.9, -1.7)   | -3.6 (-6.1, -1.0)\* |
| 20:1n-9      | 2.1 (-2.1, 6.4)  | 2.0 (-2.2, 6.3)   | -1.9 (-9.2, 6.0)     | -0.6 (-4.4, 3.3)    |
| 22:1n-9      | -3.5 (-6.8, 0.0) | -3.0 (-6.5, 0.7)  | 1.6 (-3.6, 7.1)      | 0.3 (-2.5, 3.3)     |
| 24:1n-9      | 2.4 (-1.0, 5.9)  | 1.6 (-2.2, 5.5)   | 2.1 (-3.3, 7.9)      | 0.3 (-2.4, 3.1)     |
| 14:1n-7      | 2.0 (-2.8, 7.1)  | 2.9 (-1.8, 7.7)   | -2.8 (-10.2, 5.3)    | -0.8 (-3.9, 2.4)    |
| 16:1n-7      | 1.9 (-2.5, 6.6)  | 1.7 (-2.6, 6.3)   | -6.4 (-12.3, 0.0)    | -2.9 (-5.8, 0.1)    |
| 18:1n-7      | -0.5 (-4.6, 3.8) | -2.1 (-6.4, 2.3)  | -7.1 (-12.7, -1.1)   | -3.4 (-6.3, -0.4)   |
| 18:2n-6      | -1.9 (-5.9, 2.2) | -3.5 (-7.5, 0.7)  | -7.9 (-12.9, -2.7)\* | -4.3 (-6.9, -1.6)\* |
| 18:3n-6      | 0.9 (-3.3, 5.2)  | 0.6 (-3.8, 5.2)   | -0.6 (-5.6, 4.7)     | -1.3 (-3.8, 1.4)    |
| 20:2n-6      | -0.8 (-4.6, 3.0) | -2.9 (-6.8, 1.2)  | -5.1 (-11.2, 1.4)    | -2.8 (-5.9, 0.4)    |
| 20:3n-6      | -1.2 (-4.5, 2.2) | -2.3 (-6.0, 1.6)  | 0.3 (-4.8, 5.7)      | -0.4 (-3.0, 2.2)    |
| 20:4n-6      | -0.6 (-4.5, 3.4) | -3.0 (-6.9, 1.1)  | -1.0 (-8.6, 7.1)     | -0.6 (-4.6, 3.7)    |
| 22:4n-6      | -3.2 (-7.4, 1.2) | -4.7 (-9.0, -0.1) | -0.3 (-6.5, 6.4)     | -1.1 (-4.3, 2.1)    |
| 18:3n-3      | -0.5 (-4.4, 3.5) | -2.5 (-6.4, 1.6)  | -6.0 (-10.9, -0.9)   | -3.4 (-6.1, -0.5)   |
| 20:5n-3      | 6.5 (1.6, 11.6)  | 4.2 (-1.0, 9.6)   | 0.9 (-5.3, 7.4)      | -0.5 (-3.3, 2.4)    |
| 22:5n-3      | -2.3 (-6.3, 1.9) | -3.3 (-7.5, 1.1)  | -2.7 (-8.8, 3.7)     | -1.8 (-5.3, 1.8)    |
| 22:6n-3      | -2.9 (-6.2, 0.5) | -5.2 (-9.0, -1.1) | -1.8 (-6.5, 3.2)     | -1.9 (-4.4, 0.6)    |

Table: Supplemental Table 5.3: Longitudinal associations of individual non-esterified fatty acids (mol% and nmol/mL) with insulin sensitivity and beta-cell function over the 6 years in the PROMISE cohort. GEE models were adjusted for time, sex, ethnicity, baseline age, WC,
ALT, and family history of diabetes. Outcome variables were log-transformed, predictor variables were scaled, and x-axis values were exponentiated to represent percent difference per SD increase in the fatty acid.  P-values were adjusted for the BH false discovery rate, with significant (p<0.05) associations indicated by asterisk (*).

\newpage\newpage

| Fatty acid   | log(1/HOMA-IR)       | log(ISI)             | log(IGI/IR)           | log(ISSI-2)         |
|:-------------|:---------------------|:---------------------|:----------------------|:--------------------|
| **mol%**     |                      |                      |                       |                     |
| 14:0         | 7.8 (2.0, 14.0)      | 7.5 (1.8, 13.4)      | 4.9 (-1.7, 11.9)      | 3.1 (-0.3, 6.5)     |
| 16:0         | -2.2 (-7.2, 3.2)     | -2.6 (-7.6, 2.6)     | 0.9 (-5.0, 7.2)       | 0.7 (-2.3, 3.8)     |
| 18:0         | 2.8 (-2.6, 8.4)      | 4.9 (-0.5, 10.6)     | 5.4 (-1.1, 12.3)      | 3.2 (-0.1, 6.6)     |
| 20:0         | 4.9 (-0.7, 10.7)     | 6.1 (0.5, 12.1)      | 4.1 (-2.6, 11.2)      | 2.3 (-1.1, 5.8)     |
| 22:0         | 0.7 (-3.9, 5.5)      | 0.6 (-3.8, 5.1)      | 4.8 (-0.6, 10.4)      | 1.7 (-1.1, 4.7)     |
| 18:1n-9      | -1.6 (-6.7, 3.7)     | -2.2 (-7.1, 2.9)     | -5.4 (-11.1, 0.6)     | -3.1 (-6.2, 0.1)    |
| 20:1n-9      | 0.3 (-4.5, 5.4)      | 1.2 (-3.8, 6.5)      | -2.5 (-9.0, 4.5)      | -1.1 (-4.9, 2.8)    |
| 22:1n-9      | -3.4 (-8.2, 1.7)     | -1.0 (-5.6, 3.7)     | 2.5 (-3.5, 8.8)       | 0.9 (-2.3, 4.2)     |
| 24:1n-9      | 3.4 (-1.5, 8.6)      | 3.0 (-2.0, 8.4)      | 4.2 (-1.9, 10.6)      | 1.3 (-1.7, 4.4)     |
| 14:1n-7      | 7.2 (0.6, 14.3)      | 6.6 (0.5, 13.2)      | 4.8 (-2.6, 12.8)      | 2.8 (-0.8, 6.4)     |
| 16:1n-7      | 1.3 (-4.3, 7.2)      | 0.8 (-4.5, 6.4)      | -0.7 (-7.3, 6.3)      | -0.7 (-4.0, 2.7)    |
| 18:1n-7      | -9.1 (-14.0, -3.8)\* | -7.3 (-12.5, -1.8)   | -8.6 (-14.1, -2.7)    | -4.7 (-7.8, -1.4)   |
| 18:2n-6      | 1.0 (-5.1, 7.5)      | 0.1 (-5.9, 6.4)      | -0.5 (-6.9, 6.4)      | -0.6 (-4.1, 3.0)    |
| 18:3n-6      | 2.4 (-3.2, 8.3)      | 2.7 (-2.7, 8.3)      | 4.3 (-1.8, 10.7)      | 1.6 (-1.8, 5.1)     |
| 20:2n-6      | -1.5 (-6.4, 3.5)     | -1.5 (-6.6, 3.8)     | -3.9 (-9.6, 2.3)      | -2.2 (-5.3, 1.1)    |
| 20:3n-6      | 0.9 (-3.8, 5.9)      | 0.6 (-4.3, 5.8)      | 1.9 (-3.6, 7.7)       | 0.4 (-2.5, 3.3)     |
| 20:4n-6      | -2.0 (-7.1, 3.4)     | -1.6 (-6.7, 3.8)     | 0.0 (-6.7, 7.2)       | 0.1 (-3.6, 3.9)     |
| 22:4n-6      | -5.3 (-9.9, -0.6)    | -5.1 (-9.5, -0.5)    | 8.2 (2.5, 14.3)       | 3.6 (0.7, 6.6)      |
| 18:3n-3      | 2.7 (-3.1, 8.8)      | 1.0 (-4.6, 7.0)      | 0.8 (-5.5, 7.5)       | 0.2 (-3.1, 3.7)     |
| 20:5n-3      | 12.9 (5.6, 20.8)\*   | 10.3 (3.3, 17.8)     | 6.1 (-0.8, 13.5)      | 2.5 (-1.0, 6.1)     |
| 22:5n-3      | -1.4 (-6.4, 4.0)     | -0.4 (-6.3, 5.8)     | -1.9 (-7.8, 4.4)      | -1.1 (-4.6, 2.6)    |
| 22:6n-3      | 0.7 (-5.0, 6.8)      | -1.0 (-7.0, 5.5)     | 1.9 (-4.3, 8.5)       | 0.3 (-3.2, 3.9)     |
| **nmol/mL**  |                      |                      |                       |                     |
| Total        | -3.3 (-8.9, 2.7)     | -5.2 (-10.4, 0.3)    | -8.4 (-13.9, -2.6)\*  | -4.8 (-7.8, -1.7)\* |
| 14:0         | 5.3 (-0.9, 11.8)     | 4.6 (-1.2, 10.7)     | 1.8 (-5.1, 9.2)       | 1.5 (-1.9, 4.9)     |
| 16:0         | -4.4 (-9.7, 1.3)     | -5.9 (-10.9, -0.6)   | -7.5 (-13.4, -1.3)    | -4.0 (-7.0, -0.9)   |
| 18:0         | -1.9 (-7.0, 3.5)     | -2.7 (-7.6, 2.4)     | -4.9 (-10.5, 1.1)     | -2.6 (-5.6, 0.5)    |
| 20:0         | 1.9 (-3.8, 8.0)      | 2.0 (-3.6, 7.9)      | -1.4 (-8.3, 5.9)      | -0.8 (-4.4, 3.0)    |
| 22:0         | 1.0 (-4.3, 6.6)      | -0.3 (-5.0, 4.7)     | 4.6 (-1.2, 10.7)      | 1.4 (-1.8, 4.8)     |
| 18:1n-9      | -2.8 (-8.3, 3.1)     | -4.6 (-9.8, 0.9)     | -8.2 (-13.7, -2.5)\*  | -4.7 (-7.7, -1.6)\* |
| 20:1n-9      | -0.7 (-5.9, 4.8)     | -0.5 (-5.6, 4.9)     | -4.6 (-11.3, 2.6)     | -2.4 (-6.0, 1.4)    |
| 22:1n-9      | -6.8 (-11.3, -2.0)\* | -5.1 (-9.5, -0.5)    | -1.9 (-8.0, 4.6)      | -1.8 (-4.8, 1.4)    |
| 24:1n-9      | 1.9 (-2.6, 6.7)      | 1.1 (-3.6, 6.0)      | 2.2 (-3.2, 7.9)       | 0.2 (-2.4, 3.0)     |
| 14:1n-7      | 4.9 (-1.5, 11.8)     | 4.3 (-1.5, 10.5)     | 2.2 (-5.6, 10.7)      | 1.5 (-2.0, 5.0)     |
| 16:1n-7      | -1.1 (-6.9, 5.0)     | -1.7 (-7.0, 3.9)     | -5.4 (-11.8, 1.5)     | -3.0 (-6.3, 0.4)    |
| 18:1n-7      | -7.8 (-12.9, -2.4)\* | -8.5 (-13.5, -3.3)\* | -11.3 (-16.8, -5.5)\* | -6.5 (-9.5, -3.3)\* |
| 18:2n-6      | -1.5 (-7.4, 4.9)     | -3.8 (-9.3, 2.0)     | -6.3 (-11.9, -0.3)    | -3.7 (-6.9, -0.5)   |
| 18:3n-6      | -0.1 (-5.9, 6.1)     | -0.3 (-5.8, 5.6)     | -0.1 (-6.4, 6.6)      | -1.0 (-4.3, 2.5)    |
| 20:2n-6      | -2.7 (-7.8, 2.8)     | -4.4 (-9.4, 0.9)     | -7.3 (-13.1, -1.1)    | -4.3 (-7.4, -1.0)   |
| 20:3n-6      | -1.7 (-6.0, 2.9)     | -2.6 (-7.1, 2.1)     | -1.4 (-6.4, 3.9)      | -1.5 (-4.1, 1.1)    |
| 20:4n-6      | -4.8 (-9.5, 0.1)     | -6.1 (-10.6, -1.3)   | -6.0 (-12.9, 1.5)     | -3.6 (-7.4, 0.5)    |
| 22:4n-6      | -7.3 (-11.9, -2.5)\* | -8.0 (-12.6, -3.3)\* | 2.7 (-3.8, 9.6)       | 0.7 (-2.7, 4.2)     |
| 18:3n-3      | 0.7 (-5.4, 7.1)      | -2.1 (-7.8, 4.0)     | -4.2 (-9.9, 1.8)      | -2.7 (-5.9, 0.6)    |
| 20:5n-3      | 12.0 (5.1, 19.3)\*   | 8.4 (1.9, 15.2)      | 3.9 (-2.5, 10.6)      | 1.2 (-1.9, 4.4)     |
| 22:5n-3      | -3.8 (-8.7, 1.4)     | -4.4 (-9.3, 0.8)     | -5.6 (-11.5, 0.7)     | -3.6 (-7.0, -0.1)   |
| 22:6n-3      | -0.4 (-5.1, 4.6)     | -3.3 (-8.2, 1.8)     | -1.2 (-6.4, 4.3)      | -1.5 (-4.4, 1.4)    |

Table: Supplemental Table 5.4: Unadjusted generalized estimating equation models of the longitudinal associations of individual non-esterified fatty acids (mol% and nmol/mL) with insulin sensitivity and beta-cell function over the 6 years in the PROMISE cohort.  GEE models are only adjusted for time. Outcome variables were log-transformed, predictor variables were scaled, and x-axis values were exponentiated to represent percent difference per SD increase in the fatty acid.  P-values were adjusted for the BH false discovery rate, with significant (p<0.05) associations indicated by asterisk.

\normalsize

\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/ESM_Fig4-1.pdf}
\caption[Supplemental Figure 5.4: LCMM trajectories of beta-cell function over time.]{Supplemental Figure 5.4: Latent class mixed model (LCMM) analysis to identify individual classes of trajectories for log(ISSI-2) over the 6 years in the PROMISE cohort. LCMM is a technique that identifies groups of participants that share a similar underlying trajectory in beta-cell function over the 6 years (e.g. no change compared to declines in ISSI-2). Red lines indicate individuals with a high beta-cell function who stayed high, green represents those in the middle, and blue represents those who had the lowest beta-cell function.}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/ESM_Fig5-1.pdf}
\caption[Supplemental Figure 5.5: PLS-DA components with classes of LCMM trajectories.]{Supplemental Figure 5.5: Clustering of extracted components from the partial least squares discriminant analysis (PLS-DA) on the classes extracted from the latent class mixed model (LCMM) in 463 participants from the baseline PROMISE visit (2004-2006). See the ESM Methods for a description of PLS-DA and interpreting this plot. The percent explained variance of each component is shown in brackets on each axis. Red, green, and blue lines indicate participants classified as high, middle, and low for beta-cell function, respectively, from the LCMM analysis. See Supplemental Figure 5.9 for a hypothetical example plot showing good discriminatory ability between groups.}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/ESM_Fig6-1.pdf}
\caption[Supplemental Figure 5.6: PLS-DA component loadings of individual NEFA (based on classes of LCMM).]{Supplemental Figure 5.6: Pattern loadings from partial least squares discriminant analysis (PLS-DA) to identify potential clusters of NEFA composition within the classes extracted from the latent class mixed model in 463 participants from the baseline PROMISE visit (2004-2006). The percent explained variance of each component is shown in brackets on each axis. The solid line represents an explained variance of 100\% while the dashed line represents an explained variance of 50\%. See the ESM Methods for a description of PLS-DA and an explanation of interpreting this plot.}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/ESM_Fig7-1.pdf}
\caption[Supplemental Figure 5.7: PLS-DA components with dysglycemia.]{Supplemental Figure 5.7: Clustering of extracted components from the partial least squares discriminant analysis for dysglycemia (IFG, IGT, DM) conversion status over the 6-years in the participants from the baseline PROMISE visit (2004-2006).  See the ESM Methods for a description of PLS-DA and interpreting this plot. The percent explained variance of each component is shown in brackets on each axis. Blue lines indicate dysglycemia conversion or maintanence and red lines indicate no dysglycemia status. See Supplemental Figure 5.9 for a hypothetical example plot showing good discriminatory ability between groups.}
\end{figure}


\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/ESM_Fig8-1.pdf}
\caption[Supplemental Figure 5.8: PLS-DA component loadings of individual NEFA (based on dysglycemia).]{Supplemental Figure 5.8: Pattern loadings from partial least squares discriminant analysis (PLS-DA) to identify potential clusters of NEFA composition for dysglycemia (IFG, IGT, DM) conversion status over the 6-years in the participants from the baseline PROMISE visit (2004-2006). See the ESM Methods for a description of PLS-DA and an explanation of interpreting this plot. The percent explained variance of each component is shown in brackets on each axis. The solid line represents an explained variance of 100\% while the dashed line represents an explained variance of 50\%.}
\end{figure}


\begin{figure}
\centering
\includegraphics[width=\textwidth]{manuscript_files/figure-docx/ESM_Fig9-1.pdf}
\caption[Supplemental Figure 5.9: Example of high accuracy of classification using PLS-DA.]{Supplemental Figure 5.9: Example results of a high discriminatory ability to classify groups accurately when using partial least squares discriminatory analysis (PLS-DA). Discriminatory ability is evident from the amount of separation between the two groups. In this case, PLS-DA was 82\% accurate at correctly classifying groups. See the ESM Methods for a description of PLS-DA and for interpreting this plot.}
\end{figure}

\normalsize
