
# Rationale and Objectives

Limited longitudinal data exist regarding associations of serum fatty acid composition
with detailed indices of dynamic glucose regulation that underlie T2DM
pathogenesis, specifically insulin sensitivity and beta-cell function. The
literature is also sparse on the importance of fatty acid composition within the
four main serum lipid fractions. Finally, many individual fatty acids have not
received extensive study, even in experimental settings. In light of these
important knowledge gaps in the literature, the overarching aim of my PhD
project was to explore the impact of the composition of serum fatty acids on the
pathogenesis of T2DM. There is one overall objective and three specific
sub-objectives:

1. To identify the longitudinal associations of individual fatty acids, and their 
underlying inter-correlation, in the four serum lipid fractions with insulin
sensitivity and beta-cell function in a population at-risk for T2DM. This
over-arching objective is addressed within separate serum fractions and the
results of each of these analyses are presented in separate chapters of the
thesis, specifically:
    - Chapter 3: Phospholipids and cholesteryl esters
    - Chapter 4: Non-esterified fatty acids
    - Chapter 5: Triacylglycerols

Specific hypotheses based on the previous literature:

1. Higher amounts of SFA such as palmitic acid (16:0) and 
stearic acid (18:0) would predict lower insulin sensitivity and lower beta-cell
function over time, irrespective of the individual serum lipid fraction.
2. Higher amounts of MUFA such as oleic acid (18:1n-9) and
PUFA such as eicosapentaenoic acid (20:5n-3) and
docosahexaenoic acid (22:6n-3) would predict higher insulin sensitivity and
higher beta-cell function over time, irrespective of serum lipid fraction.
3. Fatty acid compositions with an overall greater proportion of SFA
compared to MUFA or PUFA fatty acids would predict lower insulin
sensitivity and lower beta-cell function over time, irrespective of the serum lipid
fraction.
4. Triacylglyceride and non-esterified fatty acid fractions would have stronger
associations with insulin sensitivity and beta-cell function over time compared
to the phospholipid and cholesteryl ester fractions.
